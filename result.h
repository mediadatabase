/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: result.h,v 1.1 2004/05/16 18:58:50 nedko Exp $
 *
 * DESCRIPTION:
 *  libdb return values
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef RESULT_H__BE5C6D9E_732C_4B21_ABC5_B231B0DAF76A__INCLUDED
#define RESULT_H__BE5C6D9E_732C_4B21_ABC5_B231B0DAF76A__INCLUDED

typedef int mediadb_result;

#define MEDIADB_IS_ERROR(result) ((result) < 0)
#define MEDIADB_IS_SUCCESS(result) ((result) >= 0)

#define MEDIADB_ERROR(result) (-(result))
#define MEDIADB_SUCCESS(result) (result)

#define MEDIADB_OK         MEDIADB_SUCCESS(0) /* Generic success */
#define MEDIADB_FAIL       MEDIADB_ERROR(1) /* Generic fail */
#define MEDIADB_NOT_IMPL   MEDIADB_ERROR(2) /* Not implemented */
#define MEDIADB_MEM        MEDIADB_ERROR(3) /* Out of memory */
#define MEDIADB_INVAL_ARG  MEDIADB_ERROR(4) /* Invalid argument to function */
#define MEDIADB_UNEXP      MEDIADB_ERROR(5) /* Unexpected behaviour */
#define MEDIADB_NO_HOME    MEDIADB_ERROR(0) /* HOME environment variable not set */

#endif /* #ifndef RESULT_H__BE5C6D9E_732C_4B21_ABC5_B231B0DAF76A__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: result.h,v $
 *   Revision 1.1  2004/05/16 18:58:50  nedko
 *   mediadb_result has became more general, it is not specific to libdb
 *
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
