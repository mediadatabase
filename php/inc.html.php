<?php

function XHTML_Text($text, $class = null)
{
  if ($class != null)
  {
    echo "<div class=\"".$class."\">";
    echo $text;
    echo "</div>";
  }
  else
  {
    echo $text;
  }
}

define("XHTML_FormMethodGET", "GET");
define("XHTML_FormMethodPOST", "POST");

function XHTML_Form_Begin($action, $method)
{
  echo "<form action=\"".$action."\" method=\"".$method."\">\n";
}

function XHTML_Form_GET_Begin($action)
{
  XHTML_Form_Begin($action, constant("XHTML_FormMethodGET"));
}

function XHTML_Form_POST_Begin($action)
{
  XHTML_Form_Begin($action, constant("XHTML_FormMethodPOST"));
}

function XHTML_Form_End()
{
  echo "</form>\n";
}

function XHTML_Form_Input_Text($name, $size = null)
{
  echo "<input type=\"text\" class=\"input-text\" name=\"".$name."\"";
  if ($size != null)
  {
    echo " size=\"".$size."\"";
  }
  echo " />\n";
}

function XHTML_Form_Input_Checkbox($name)
{
  echo "<input type=\"checkbox\" class=\"input-checkbox\" name=\"".$name."\" />\n";
}

function XHTML_Form_Input_Choice($name, $value, $checked = false)
{
  if ($checked)
  {
    echo "<input type=\"radio\" class=\"input-radio\" name=\"".$name."\" value=\"".$value."\" checked />\n";
  }
  else
  {
    echo "<input type=\"radio\" class=\"input-radio\" name=\"".$name."\" value=\"".$value."\" />\n";
  }
}

function XHTML_Form_Input_Submit($value)
{
  echo "<input type=\"submit\" value=\"".$value."\" class=\"input-button\">";
}

function XHTML_Form_Select_Begin($name)
{
  echo "<select name=\"".$name."\">\n";
}

function XHTML_Form_Select_End()
{
  echo "</select>\n";
}

function XHTML_Form_Option($description, $value, $selected=false)
{
  if ($selected)
  {
    echo "<option value=\"".$value."\" selected>".$description."</option>\n";
  }
  else
  {
    echo "<option value=\"".$value."\">".$description."</option>\n";
  }
}

function XHTML_Table_Begin()
{
  echo "<table>\n";
}

function XHTML_Table_End()
{
  echo "</table>\n";
}

function XHTML_Table_Row_Begin()
{
  echo "</tr>\n";
}

function XHTML_Table_Row_End()
{
  echo "<tr>\n";
}

function XHTML_Table_Cell_Begin()
{
  echo "<td>\n";
}

function XHTML_Table_Cell_End()
{
  echo "</td>\n";
}

function XHTML_Span_Begin($class)
{
  echo "<span class=\"".$class."\">\n";
}

function XHTML_Span_End()
{
  echo "</span>\n";
}

function XHTML_LineBreak()
{
  echo "<br />\n";
}


?>
