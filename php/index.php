<?php
require('inc.header.php');

function ShowChangeLocationForm($media_id)
{
  $query  = "SELECT l.id,l.description,lt.description FROM ".
    $GLOBALS['config']['tbl_locations']." AS l, ".
    $GLOBALS['config']['tbl_location_types']." AS lt ".
    "WHERE l.type = lt.id AND l.id != 0 ORDER BY lt.description DESC, l.description";
  if (!$res = mysql_query($query))
  {
    echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
    return;
  }

  if (!mysql_num_rows($res))
  {
    echo "No locations found.\n";
    return;
  }

  echo "<form action=\"".$GLOBALS['self']."?media=".$media_id."\" method=\"post\">\n";
  echo "Change location to\n";
  echo "  <select name=\"new_location\" class=\"input-select\">\n";

  $first = TRUE;

  while ($row = mysql_fetch_array($res))
  {
    echo "    <option value=\"".$row[0]."\"";
    if ($first)
    {
      echo " selected=\"selected\"";
      $first = FALSE;
    }
    echo ">".$row[1]." (".$row[2].")"."</option>\n";
  }
  echo "  </select>\n";
  echo "  <input type=\"submit\" class=\"input-button\" value=\"Change\">\n";
  echo "</form>\n";
}

function ChangeMediaLocation($media_id, $new_location_id)
{
  if ($GLOBALS['config']['readonly_mode'])
  {
    echo "<p>Cannot modify database in read-only mode.</p>\n";
  }
  else
  {
    $query = "UPDATE ".$GLOBALS['config']['tbl_media'].
      " SET location = '".$new_location_id."'".
      " WHERE mediaid = '".$media_id."'";
//  echo $query;

    if (!$res = mysql_query($query))
    {
      echo "<p><b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n</p>\n";
    }
    else if (mysql_affected_rows() != 0)
    {
      echo "<p>Location changed.</p>\n";
      return;
    }
  }

  echo "<p>Could not change location.</p>\n";
}

if (isset($_GET['media']))
{
  if (isset($_POST['new_location']))
  {
    ChangeMediaLocation($_GET['media'], $_POST['new_location']);
  }

	echo "<p>\n";
	$query = "SELECT m.*,l.description,l.id FROM ".
    $config['tbl_media']." AS m, ".
    $config['tbl_locations']." AS l ".
    "WHERE mediaid = '".addslashes($_GET['media'])."' AND ".
    "m.location = l.id ".
    "ORDER BY name";

	if (!$res = mysql_query($query))
  {
		echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
	}
  else
  {
		if (!mysql_num_rows($res))
    {
			echo "Media not found.\n";
		}
    else
    {
			$row = mysql_fetch_assoc($res);

      echo "<table class=\"ObjectParametersTable\">\n";
      echo "  <tr>\n";
      echo "    <td>Media ID</td>\n";
      echo "    <td>".$row['mediaid']."</td>";
      echo "  </tr>\n";
      echo "  <tr>\n";
      echo "    <td>Media description</td>\n";
      echo "    <td>".$row["name"]."</td>";
      echo "  </tr>\n";
      echo "  <tr>\n";
      echo "    <td>Media type</td>\n";
      echo "    <td>".mb_typetext($row['type'])."</td>";
      echo "  </tr>\n";
      echo "  <tr>\n";
      echo "    <td>Current location</td>\n";
      echo "    <td>";
      if ($row["description"])
      {
        echo "<a href=locations.php?id=".$row['id'].">".$row['description']."</a>";
      }
      else
      {
        echo "Unknown.";
      }
      echo "    </td>\n";
      echo "  <tr>\n";
      echo "</table>\n";
      echo "<p>\n";
      ShowChangeLocationForm($_GET['media']);
      echo "</p>\n";
      echo "<p>\n";

			if ($row['type'] == MB_T_AUDIO)
      {
				$query = "SELECT * FROM ".$config['tbl_tracks']." WHERE mediaid = ".$row['mediaid']." ORDER BY trackid";
				if (!$res = mysql_query($query))
        {
					echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
				}
        else
        {
					if (!mysql_num_rows($res))
          {
						echo "No tracks found in database.\n\n";
					}
          else
          {
						mb_table_start('#','Name','Length');
						if (isset($_GET['cat']))
            {
							$link = "\"$self?cat=".$_GET['cat'].'"';
						}
            else
            {
							$link = "\"$self\"";
						}
						mb_table_widecol("<a href=$link>Back to index</a>");
						while ($row = mysql_fetch_assoc($res))
            {
							mb_table_col($row['trackid']);
							mb_table_col($row['name']);
							mb_table_col(mb_lengthtext($row['length']));
						}
						mb_table_end();
					}
				}
			}
      elseif ($row['type'] == MB_T_DATA)
      {
				$dir = isset($_GET['dir']) ? $_GET['dir'] : '/';
				$parentdir = mb_getparentdir($dir);

        echo "<p>".mb_icon('__folder').$dir."<p>";

				$query = "SELECT * FROM ".$config['tbl_files'].
          " WHERE path = '".addslashes($dir)."' AND ".
          "mediaid = '".addslashes($_GET['media'])."' ".
          "ORDER BY name";

				if (!$res = mysql_query($query))
        {
					echo "<b>MySQL error at ".basename(__FILE__).", line ".__LINE__."<br>";
					echo "<b>Error:</b> ".mysql_error()."<br>";
					echo "<b>Error description :</b> " . mysql_errno() . "<br>";
					echo "<b>SQL request:</b> ".$query."\n\n";
				}
        else
        {
					if (!mysql_num_rows($res))
          {
						echo "No files found.\n\n";
					}
          else
          {
						$folders = array();
						$files = array();
						while ($row = mysql_fetch_assoc($res))
            {
							if (substr($row['name'],-1) == '/')
              {
								$folders[] = $row;
							}
              else
              {
								$files[] = $row;
							}
						}
						$selflink  = "$self?media=".$_GET['media'].(isset($_GET['cat'])?'&cat='.$_GET['cat']:'');
						mb_table_start('Filename','Size','Timestamp');
						if ($parentdir)
            {
							mb_table_widecol(mb_icon('__folder')."<a href=\"$selflink&dir=".urlencode($parentdir)."\">Parent directory</a>");
						}
            else
            {
							if (isset($_GET['cat']))
              {
								$link = "\"$self?cat=".$_GET['cat'].'"';
							}
              else
              {
								$link = "\"$self\"";
							}
							mb_table_col("<a href=$link>Back to index</a>");
							mb_table_col('&nbsp;');
							mb_table_col('&nbsp;');
						}
						foreach ($folders as $folder)
            {
							$link = "<a href=\"$selflink&dir=".urlencode($dir.$folder['name']).'">'.$folder['name']."</a>";
							mb_table_col(mb_icon('__folder').$link);
							mb_table_col('&nbsp;');
							mb_table_col(mb_datetext($folder['time']));
						}
						foreach ($files as $file)
            {
							echo "	<tr>\n";
							mb_table_col(mb_icon('__unknown').$file['name']);
							mb_table_col(mb_sizetext($file['size']));
							mb_table_col(mb_datetext($file['time']));
						}
						mb_table_end();
					}
				}
			}
      else
      {
				echo "<b>Error:</b> Media must be either data or audio.\n";
			}
		}
	}
}
elseif (!$config['use_categories'] || isset($_GET['cat']))
{
	if ($config['use_categories'])
  {
		$query = "SELECT m.*,c.name as category FROM ".
      $config['tbl_media']." AS m, ".
      $config['tbl_categories']." AS c ".
      "WHERE m.catid = c.catid AND c.catid = ".addslashes($_GET['cat'])." ORDER BY m.name";
	}
  else
  {
		$query = "SELECT l.description,l.id,m.* FROM ".
      $config['tbl_media']." AS m, ".
      $config['tbl_locations']." AS l ".
      "WHERE m.location = l.id ".
      "ORDER BY m.mediaid DESC";
	}

	if (!$res = mysql_query($query))
  {
		echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
	}
  else
  {
		if (!mysql_num_rows($res))
    {
			echo "No media found".($config['use_categories']?' in this category':'').".\n";
		}
    else
    {
      mb_table_start('Media ID',
                     'Name',
                     'Date Added',
                     'Location');

			if ($config['use_categories'])
      {
				mb_table_widecol("<a href=\"$self\">Back to category index</a>");
			}

			while ($row = mysql_fetch_assoc($res))
      {
				$extra = ($config['use_categories']?'&cat='.$row['catid']:'');
				$link = (($row['type'] == MB_T_AUDIO) || ($row['type'] == MB_T_DATA));
				mb_table_col($row['mediaid']);
				mb_table_col(mb_iconbytype($row['type']).
                     ($link?"<a href=\"$self?media=".
                      $row['mediaid'].
                      $extra.
                      '">':'').
                     truncate_ellipsis($row['name'], $config['media_title_ellipsis_limit']).
                     ($link?'</a>':''));
				mb_table_col(mb_datetext($row['added']));
				mb_table_col("<a href=locations.php?id=".$row['id'].">".$row['description']."</a>");
			}

			mb_table_end();
		}
	}
}
else
{
	echo "<p>\n";
	$query  = "SELECT c.catid,c.name,count(m.mediaid) AS count FROM ".$config['tbl_categories']." AS c LEFT JOIN ";
	$query .= $config['tbl_media']." AS m ON m.catid = c.catid GROUP BY c.catid ORDER BY c.name";
	if (!$res = mysql_query($query))
  {
		echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
	}
  else
  {
		if (!mysql_num_rows($res))
    {
			echo "No categories found.\n";
		}
    else
    {
			mb_table_start('Category','Media');
			while ($row = mysql_fetch_assoc($res))
      {
				mb_table_col(mb_icon('__category').
                     "<a href=\"$self?cat=".
                     $row['catid'].'">'.
                     $row['name'].
                     "</a>");
				mb_table_col($row['count']);
			}
			mb_table_end();
		}
	}
}

require('inc.footer.php');
?>
