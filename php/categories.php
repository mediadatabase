<?php

require('inc.header.php');

if (isset($_POST['cat_add']) && strlen($_POST['cat_add']) > 0) {
	if (!$res = mysql_query("INSERT INTO ".$config['tbl_categories']." VALUES (NULL,'".addslashes($_POST['cat_add'])."')")) {
		echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n<p>\n";
	} else {
		if (!mysql_affected_rows()) {
			echo "Could not add category.\n<p>\n";
		} else {
			echo "Category added.\n<p>\n";
		}
	}
}


if (!$res = mysql_query("SELECT * FROM ".$config['tbl_categories']." ORDER BY name")) {
	echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
} else {
	if (!mysql_num_rows($res)) {
		echo "No categories!\n";
	} else {
		echo "<b>Categories</b>\n";
		echo "<ul>\n";
		while (list($id,$name) = mysql_fetch_row($res)) {
			echo "	<li>$name\n";
		}
		echo "</ul>\n";
	}
}

echo "<form action=\"$self\" method=\"post\">\n";
echo "	Add category <input type=\"text\" name=\"cat_add\">\n";
echo "	<input type=\"submit\" value=\"Add\">\n";
echo "</form>\n";

require('inc.footer.php');

?>
