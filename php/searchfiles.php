<?php

require('inc.header.php');
require('inc.html.php');


function ShowDataSearchForm()
{
  XHTML_Form_GET_Begin($GLOBALS['self']);
  {
    XHTML_Text("File search");
    XHTML_Table_Begin(); 
    {
      XHTML_Table_Row_Begin();
      {
        XHTML_Table_Cell_Begin();
        XHTML_Text("Filename");
        XHTML_Table_Cell_End();

        XHTML_Table_Cell_Begin();
        XHTML_Form_Input_Text("search_filename");
        XHTML_Text("Exact");
        XHTML_Form_Input_Checkbox("search_exact");
        XHTML_Text("case sensitive");
        XHTML_Form_Input_Checkbox("case");
        XHTML_Table_Cell_End();
      }
      XHTML_Table_Row_End();

      XHTML_Table_Row_Begin();
      {
        XHTML_Table_Cell_Begin();
        XHTML_Text("Min size");
        XHTML_Table_Cell_End();

        XHTML_Table_Cell_Begin();
        {
          XHTML_Form_Input_Text("search_minsize");
          XHTML_Form_Select_Begin("min_size_unit");
          XHTML_Form_Option("Bytes", "b");
          XHTML_Form_Option("KBytes", "k", true);
          XHTML_Form_Option("MBytes", "m");
          XHTML_Form_Option("GBytes", "g");
          XHTML_Form_Select_End();
        }
        XHTML_Table_Cell_End();
      }
      XHTML_Table_Row_End();

      XHTML_Table_Row_Begin();
      {
        XHTML_Table_Cell_Begin();
        XHTML_Text("Max size");
        XHTML_Table_Cell_End();

        XHTML_Table_Cell_Begin();
        {
          XHTML_Form_Input_Text("search_maxsize");
          XHTML_Form_Select_Begin("max_size_unit");
          XHTML_Form_Option("Bytes", "b");
          XHTML_Form_Option("KBytes", "k", true);
          XHTML_Form_Option("MBytes", "m");
          XHTML_Form_Option("GBytes", "g");
          XHTML_Form_Select_End();
        }
        XHTML_Table_Cell_End();
      }
      XHTML_Table_Row_End();

      XHTML_Table_Row_Begin();
      {
        XHTML_Table_Cell_Begin();
        XHTML_Text("Path");
        XHTML_Table_Cell_End();

        XHTML_Table_Cell_Begin();
        XHTML_Form_Input_Text("search_path");
        XHTML_Text("Exact");
        XHTML_Form_Input_Checkbox("search_pathexact");
        XHTML_Text("case sensitive");
        XHTML_Form_Input_Checkbox("pathcase");
        XHTML_Table_Cell_End();
      }
      XHTML_Table_Row_End();

      if ($GLOBALS['config']['use_categories'])
      {
        XHTML_Table_Row_Begin();
        {
          XHTML_Table_Cell_Begin();
          XHTML_Text("Category");
          XHTML_Table_Cell_End();

          XHTML_Table_Cell_Begin();
          XHTML_Form_Input_Select_Begin("search_category");
          {
            XHTML_Form_Option("Any category", "any");
            XHTML_Form_Option("--", "--");

            mb_fetch_select_categories(4);
          }
          XHTML_Form_Input_Select_End();
          XHTML_Table_Cell_End();
        }
      }
    }
    XHTML_Table_End(); 

    XHTML_Form_Input_Submit("Search");
  }
  XHTML_Form_End();
}

// SQL Query builder
$where = array();
if (isset($_GET['search_filename']) && strlen($_GET['search_filename']) > 0)
{
	if (isset($_GET['case']))
  {
    $binary = "BINARY ";
  }
  else
  {
    $binary = "";
  }

	if (isset($_GET['search_exact']))
  {
		$where[] = $binary."f.name = '".addslashes($_GET['search_filename'])."'";
	}
  else
  {
		$where[] = "f.name REGEXP ".$binary."'".addslashes($_GET['search_filename'])."'";
	}
}

if (isset($_GET['search_minsize']) && is_numeric($_GET['search_minsize']))
{
  $size = $_GET['search_minsize'];
  switch ($_GET['min_size_unit'])
  {
  case "g":
    $size *= 1024;
  case "m":
    $size *= 1024;
  case "k":
    $size *= 1024;
  }
	$where[] = "f.size >= ".$size;
}

if (isset($_GET['search_maxsize']) && is_numeric($_GET['search_maxsize']))
{
  $size = $_GET['search_maxsize'];
  switch ($_GET['max_size_unit'])
  {
  case "g":
    $size *= 1024;
  case "m":
    $size *= 1024;
  case "k":
    $size *= 1024;
  }
	$where[] = "f.size <= ".$size;
}

if (isset($_GET['search_path']) && strlen($_GET['search_path']) > 0)
{
	if (isset($_GET['pathcase']))
  {
    $binary = "BINARY ";
  }
  else
  {
    $binary = "";
  }

	if (isset($_GET['search_pathexact']))
  {
		$where[] = $binary."f.path = '".addslashes($_GET['search_path'])."'";
	}
  else
  {
		$where[] = "f.path REGEXP ".$binary."'".addslashes($_GET['search_path'])."'";
	}
}

if (count($where) > 0)
{
	// Database query {{{
		$tables = $config['tbl_files']." AS f, ".$config['tbl_media']." AS m";
		$where[] = 'f.mediaid = m.mediaid';
		$cols = 'f.name, f.path, f.size, f.time, m.mediaid, m.name as medianame';
		if ($config['use_categories'])
    {
			$tables .= ", ".$config['tbl_categories']." AS c";
			$cols .= ", c.catid, c.name as catname";
			$where[] = 'm.catid = c.catid';
			$headers[] = 'Category';
		}
		$query = "SELECT $cols FROM $tables WHERE ".implode(' AND ',$where);

    //echo "<p>".$query."</p>";

		if (!$res = mysql_query($query))
    {
			echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
		}
    else
    {
			if (!$count = mysql_num_rows($res)) {
				echo "Found no files matching your query.\n\n";
			} else {
				$pl = $count > 1 ? 's' : '';
				echo "Found $count file$pl matching your query.\n<br><br>\n";
				
				// Output
				if ($config['use_categories']) {
					mb_table_start('Filename','Path','Size','Timestamp','Medium','Media ID','Category');
				} else {
					mb_table_start('Filename','Path','Size','Timestamp','Medium','Media ID');
				}

				while ($row = mysql_fetch_array($res)) {
					if (substr($row['name'],-1) == '/') {
						mb_table_col('<nobr>'.mb_icon('__folder').'<a href="index.php?media='.$row['mediaid'].'&dir='.urlencode($row['path']).urlencode($row['name']).'">'.$row['name'].'</a></nobr>');
						$showsize = false;
					} else {
						mb_table_col('<nobr>'.mb_icon('__unknown').$row['name'].'</nobr>');
						$showsize = true;
					}
					mb_table_col('<nobr><a href="index.php?media='.$row['mediaid'].'&dir='.urlencode($row['path']).'">'.$row['path'].'</a></nobr>');
					mb_table_col($showsize?mb_sizetext($row['size']):'');
					mb_table_col(mb_datetext($row['time']));
					mb_table_col('<a href="index.php?media='.$row['mediaid'].'">'.$row['medianame'].'</a>');
					mb_table_col($row['mediaid']);
					if ($config['use_categories']) {
						mb_table_col('<a href="index.php?cat='.$row['catid'].'">'.$row['catname'].'</a>');
					}
				}

				mb_table_end();
			}
		}
	// }}}
	echo "<p><a href=\"$self\">New search</a>\n";
}
else
{
	ShowDataSearchForm();
}

require('inc.footer.php');

?>
