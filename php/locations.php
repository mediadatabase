<?php

require('inc.header.php');

function ShowLocations()
{
	echo "<p>\n";
	$query  = "SELECT l.description,lt.description,l.id FROM ".
    $GLOBALS['config']['tbl_locations']." AS l, ".
    $GLOBALS['config']['tbl_location_types']." AS lt ".
    "WHERE l.type = lt.id AND l.id != 0 ORDER BY lt.description, l.description";
	if (!$res = mysql_query($query))
  {
		echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
    return;
	}

  if (!mysql_num_rows($res))
  {
    echo "No locations found.\n";
    return;
  }

  mb_table_start('Location','Type');
  while ($row = mysql_fetch_array($res))
  {
    mb_table_col("<a href=\"".$GLOBALS['self']."?id=".$row[2]."\">".$row[0]."</a>");
    mb_table_col($row[1]);
  }
  mb_table_end();
	echo "</p>\n";
}

function ShowAddLocationForm()
{
	$query  = "SELECT * FROM ".
    $GLOBALS['config']['tbl_location_types']." AS lt ".
    "WHERE id != 0 ORDER BY description";
	if (!$res = mysql_query($query))
  {
		echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
    return;
	}

	echo "<p>\n";
  echo "<form action=\"".$GLOBALS['self']."\" method=\"post\">\n";
  echo "	Add new location <input type=\"text\" class=\"input-text\" name=\"location_add\">\n";
  echo "<select name=\"location_type\" class=\"input-select\">\n";

  $first = TRUE;

  while ($row = mysql_fetch_assoc($res))
  {
    echo "  <option value=\"".$row['id']."\"";
    if ($first)
    {
      echo " selected=\"selected\"";
      $first = FALSE;
    }
    echo ">".$row['description']."</option>\n";
  }
  echo "</select>\n";
  echo "	<input type=\"submit\" class=\"input-button\" value=\"Add\">\n";
  echo "</form>\n";
	echo "</p>\n";
}

function AddLocation()
{
  if ($GLOBALS['config']['readonly_mode'])
  {
    echo "<p>Cannot modify database in read-only mode.</p>\n";
  }
  else
  {
    $query = "INSERT INTO ".$GLOBALS['config']['tbl_locations'].
      "(description,type)".
      " VALUES ('".addslashes($_POST['location_add'])."', ".$_POST['location_type'].")";
    if (!$res = mysql_query($query))
    {
      echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n<p>\n";
    }
    else if (mysql_affected_rows() != 0)
    {
      echo "Location added.\n\n";
      return;
    }
  }

  echo "Could not add location.\n\n";
}

function ShowLocation($location_id)
{
	$query  = "SELECT l.description,lt.description FROM ".
    $GLOBALS['config']['tbl_locations']." AS l, ".
    $GLOBALS['config']['tbl_location_types']." AS lt ".
    "WHERE l.type = lt.id AND l.id = ".$location_id;
	if (!$res = mysql_query($query))
  {
		echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
    return;
	}

  if (!mysql_num_rows($res))
  {
    echo "No locations found.\n";
    return;
  }

  $row = mysql_fetch_array($res);
  echo "<table class=\"ObjectParametersTable\">\n";
  echo "  <tr>\n";
  echo "    <td>Location description</td>\n";
  echo "    <td>".$row[0]."</td>";
  echo "  </tr>\n";
  echo "  <tr>\n";
  echo "    <td>Location type</td>\n";
  echo "    <td>".$row[1]."</td>";
  echo "  </tr>\n";
  echo "</table>\n";

  $query = "SELECT l.description,m.* FROM ".
    $GLOBALS['config']['tbl_media']." AS m, ".
    $GLOBALS['config']['tbl_locations']." AS l ".
    "WHERE m.location = l.id AND m.location = ".$location_id." ".
    "ORDER BY m.mediaid DESC";

	if (!$res = mysql_query($query))
  {
		echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
    return;
	}

  if (!mysql_num_rows($res))
  {
    echo "No media at this location.\n";
    return;
  }

  echo "<p>\n";

  mb_table_start('Media ID',
                 'Name');

  while ($row = mysql_fetch_assoc($res))
  {
    $link = (($row['type'] == MB_T_AUDIO) || ($row['type'] == MB_T_DATA));
    mb_table_col($row['mediaid']);
    mb_table_col(mb_iconbytype($row['type']).
                 ($link?"<a href=\"index.php?media=".
                  $row['mediaid'].
                  '">':'').
                 $row['name'].
                 ($link?'</a>':''));
  }
  mb_table_end();
  echo "</p>\n";
}

if (isset($_GET['id']))
{
  ShowLocation($_GET['id']);
}
else
{
  if (isset($_POST['location_add']) && strlen($_POST['location_add']) > 0)
  {
    AddLocation();
  }

  ShowLocations();
  ShowAddLocationForm();
}

require('inc.footer.php');

?>
