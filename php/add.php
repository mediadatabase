<?php

require('inc.header.php');

function showform () 
{
	global $config, $self;
	echo "<p><b>Upload .mb file</b>\n";
	echo "<p>\n<i>\n";
	echo "	Note: Any title you enter here will override the title<br>\n";
	echo "	(if any) contained in the file you are about to upload.<br>\n";
	echo "	If no title is found, &quot;".$config['default_media_title']."&quot; is used.<br>\n</i>\n";
	echo "<form enctype=\"multipart/form-data\" action=\"$self\" method=\"post\">\n";
	echo "	<p>Title<br>\n<input type=\"text\" name=\"add_title\">\n";
	if ($config['use_categories']) {
		echo "	<p>Category<br>\n";
		echo "	<select name=\"add_category\">\n";
		if (!mb_fetch_select_categories(2)) {
			echo "			<option value=\"\">ERROR</option>\n";
		}
		echo "	</select>\n";
	}
	echo "	<p>File<br>\n	<input type=\"file\" name=\"add_file\">\n";
	echo "	<p><input type=\"submit\" value=\"Upload\">\n";
	echo "</form>\n";
}

if (isset($_FILES['add_file']['name']) && is_uploaded_file($_FILES['add_file']['tmp_name'])) {
	$file = file($_FILES['add_file']['tmp_name']);
	$verstr = mb_chomp(array_shift($file));
	if ($verstr != "MediaBase 1.2") {
		echo "<p><b>Error:</b> Invalid header '$verstr'\n\n";
	} else {
		$media = array();
		$media['files'] = array();
		$media['tracks'] = array();
		$media['created'] = time();
		$media['info1'] = 0;
		$media['info2'] = 0;
		$errors = array();
		$lineno = 1;
		echo "<p>Parsing ".basename($_FILES['add_file']['name'])."...\n\n";
		foreach ($file as $line) {
			$lineno++;
			$line = mb_chomp($line);
			// remove comments and empty lines
			if (preg_match('/^\s*(#.*?)?$/',$line)) { continue; }
			
			list($key,$param) = explode("\t",$line,2);
			switch (strtolower($key)) {
				case 'title':	$media['title'] = $param; break;
				case 'type':	switch(strtolower($param)) {
									case 'data': $media['type'] = MB_T_DATA; break;
									case 'audio': $media['type'] = MB_T_AUDIO; break;
									case 'emtpy': $media['type'] = MB_T_EMPTY; break;
									default: $errors[] = "Invalid type '$param' on line $lineno";
								}
								break;
				case 'created':	if (is_numeric($param)) {
									$media['created'] = $param;
								} else {
									$errors[] = "Invalid timestamp '$param' on line $lineno";
								}
								break;
				case 'info':	list($info1,$info2) = explode("\t",$param,2);
								if (is_numeric($info1) && is_numeric($info2)) {
									$media['info1'] = $info1;
									$media['info2'] = $info2;
								} else {
									$errors[] = "Invalid infoline '$param' on line $lineno";
								}
								break;
				case 'track':	if ($media['type'] != MB_T_AUDIO) {
									$errors[] = "Invalid media type for 'track' on line $lineno";
								} else {
									list($track,$length,$name) = explode("\t",$param,3);
									if (!is_numeric($track)) {
										$errors[] = "Invalid track number '$track' for track on line $lineno";
									}
									if (!is_numeric($length)) {
										$errors[] = "Invalid length '$length' for track on line $lineno";
									}
									$media['tracks'][] = array('track'=>$track,'length'=>$length,'name'=>$name);
								}
								break;
				case 'file':	if ($media['type'] != MB_T_DATA) {
									$errors[] = "Invalid media type for 'file' on line $lineno";
								} else {
									list($size,$timestamp,$path,$filename) = explode("\t",$param,4);
									if (!is_numeric($size)) {
										$errors[] = "Invalid size '$size' for file on line $lineno";
									}
									if (!is_numeric($timestamp)) {
										$errors[] = "Invalid timestamp '$timestamp' for file on line $lineno";
									}
									$media['files'][] = array('size'=>$size,'timestamp'=>$timestamp,'path'=>$path,'filename'=>$filename);
								}
								break;
				default: $errors[] = "Invalid directive '$key' on line $lineno";
			}
		}
		if (count($errors) > 0) {
			echo "<p><b>Errors occured:</b>\n\n";
			echo "<ul>\n";
			foreach ($errors as $error) {
				echo "	<li>$error\n";
			}
			echo "</ul>\n";
			echo "Media was not added.\n";
		} else {
			if (isset($_POST['add_category']) && is_numeric($_POST['add_category'])) {
				$category = $_POST['add_category'];
			} else {
				$category = 0;
			}
			if (isset($_POST['add_title']) && strlen($_POST['add_title']) > 0) {
				$media['title'] = $_POST['add_title'];
			}
			$query  = "INSERT INTO ".$config['tbl_media']." VALUES (NULL,$category,";
			$query .= $media['type'].','.$media['created'].','.$media['info1'].','.$media['info2'];
			$query .= ",'".addslashes($media['title'])."','')";
			if (!$res = mysql_query($query)) {
				echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
			} else {
				$mediaid = mysql_insert_id();
				$queries = array();
				if ($media['type'] == MB_T_AUDIO) {
					foreach ($media['tracks'] as $track) {
						$queries[] = "INSERT INTO ".$config['tbl_tracks']." VALUES (".$track['track'].",$mediaid,'".addslashes($track['name'])."',".$track['length'].")";
					}
				} elseif ($media['type'] == MB_T_DATA) {
					foreach($media['files'] as $file) {
						$queries[] = "INSERT INTO ".$config['tbl_files']." VALUES (NULL,$mediaid,".$file['size'].','.$file['timestamp'].",'".addslashes($file['path'])."','".addslashes($file['filename'])."')";
					}
				}
				$error = false;
				foreach ($queries as $query) {
					if (!mysql_query($query)) {
						echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
						echo "Query: $query";
						$error = true;
						break;
					}
				}
				if ($error) {
					echo "<p><b>".$media['title']."</b> was <b>not</b> added.\n\n";
				} else {
					echo "<p><b>".$media['title']."</b> was added.\n\n";
				}
			}
		}
	}
}

showform();

require('inc.footer.php');

?>
