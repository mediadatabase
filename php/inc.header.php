<?php
require('inc.config.php');
require('inc.functions.php');

define('MB_T_AUDIO',1);
define('MB_T_DATA',2);
define('MB_T_EMPTY',3);

$self = $_SERVER['PHP_SELF'];

if (!mysql_connect($config['mysql_hostname'],$config['mysql_username'],$config['mysql_password'])) {
	echo "<b>Fatal error:</b> ".mysql_error()."\n";
	exit;
}

if (!mysql_select_db($config['mysql_database'])) {
	echo "<b>Fatal error:</b> ".mysql_error()."\n";
	exit;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
	<head>
		<title>Offline media content database</title>
		<link rel="stylesheet" href="styles.css" type="text/css">
	</head>
	
	<body>
		<p>
		<a href="index.php">Browse</a>
		&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href="locations.php">Locations</a>
<?php
 //		&nbsp;&nbsp;|&nbsp;&nbsp;
  //		<a href="searchaudio.php">Search audio</a>
?>
		&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href="searchfiles.php">Search files</a>
<?php
    if ($config['use_categories'])
    {
?>
		&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href="categories.php">Categories</a>
<?php
    }
?>

		<p>
