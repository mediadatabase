<?php

$config = array(

  // MySQL settings
  'mysql_hostname'        =>      'localhost',
  'mysql_username'        =>      'mediadatabase',
  'mysql_password'        =>      '',
  'mysql_database'        =>      'mediadatabase',

  // Table names
  'tbl_media'             =>      'media',
  'tbl_categories'        =>      'categories',
  'tbl_files'             =>      'files',
  'tbl_tracks'            =>      'tracks',
  'tbl_locations'         =>      'locations',
  'tbl_location_types'    =>      'location_types',

  // How to format the file timestamps
  'datestring'            =>      'Y-m-d H:i:s',

  // Whether or not to display file sizes as KB,MB etc.
  'human_readable_size'   =>      true,

  // Whether or not to use categories
  // WARNING: Categories support is currently broken 
  'use_categories'        =>      false,

  // Icon prefix
  // Default is 'img/icon_' - e.g. 'img/icon_folder.jpg'
  'icon_prefix'           =>      'img/icon_',

  // Default title for new media
  'default_media_title'   =>      'New CD',

  'media_title_ellipsis_limit' => 50,

  'readonly_mode'         =>      false,

  '');
?>
