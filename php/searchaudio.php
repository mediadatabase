<?php

require('inc.header.php');

function showform () // Audio search form {{{
{ global $self, $config; ?>
<form action="<?php echo $self; ?>" method="get">
	<b>Audio search</b><br>
	<ul>
		<li>Track name <input type="text" name="search_trackname"> Exact <input type="checkbox" name="search_exact">
		<li>Min length <input type="text" name="search_minlength" size="8"> Max length <input type="text" name="search_maxlength" size="8">
		<li>CD Title <input type="text" name="search_cd"> Exact <input type="checkbox" name="search_cdexact">
<?php if ($config['use_categories']) { ?>
		<li>Category
			<select name="search_category">
				<option value="">Any category</option>
				<option value="">--</option>
<?php mb_fetch_select_categories(4); ?>
			</select>
<?php } ?>
	</ul>
	<input type="submit" value="Search">
</form>
<?php } // }}}

// Query builder {{{
$where = array();
if (isset($_GET['search_trackname']) && strlen($_GET['search_trackname']) > 0) {
	if (isset($_GET['search_exact'])) {
		$where[] = "t.name = '".addslashes($_GET['search_trackname'])."'";
	} else {
		$where[] = "t.name LIKE '%".addslashes($_GET['search_trackname'])."%'";
	}
}
if (isset($_GET['search_minlength'])) {
	if (is_numeric($_GET['search_minlength'])) {
		$where[] = "t.length >= ".$_GET['search_minlength'];
	} elseif ((list($min,$sec) = explode(':',$_GET['search_minlength'],2)) && is_numeric($min) && is_numeric($sec)) {
		$sec += $min * 60;
		$where[] = "t.length >= $sec";
	}	
}
if (isset($_GET['search_maxlength'])) {
	if (is_numeric($_GET['search_maxlength'])) {
		$where[] = "t.length <= ".$_GET['search_maxlength'];
	} elseif ((list($min,$sec) = explode(':',$_GET['search_maxlength'],2)) && is_numeric($min) && is_numeric($sec)) {
		$sec += $min * 60;
		$where[] = "t.length <= $sec";
	}	
}
if (isset($_GET['search_cd']) && strlen($_GET['search_cd']) > 0) {
	if (isset($_GET['search_cdexact'])) {
		$where[] = "m.name = '".addslashes($_GET['search_cd'])."'";
	} else {
		$where[] = "m.name LIKE '%".addslashes($_GET['search_cd'])."%'";
	}
}
// }}}

if (count($where) > 0) {
	// Database query {{{
		$tables = $config['tbl_tracks']." AS t, ".$config['tbl_media']." AS m";
		$where[] = 't.mediaid = m.mediaid';
		$cols = 't.trackid, t.name, t.length, m.mediaid, m.name as medianame';
		if ($config['use_categories']) {
			$tables .= ", ".$config['tbl_categories']." AS c";
			$cols .= ", c.catid, c.name as catname";
			$where[] = 'm.catid = c.catid';
			$headers[] = 'Category';
		}
		$query = "SELECT $cols FROM $tables WHERE ".implode(' AND ',$where);
		if (!$res = mysql_query($query)) {
			echo "<b>Error:</b> ".mysql_error()." <i>(".basename(__FILE__).", line ".__LINE__.")</i>\n\n";
		} else {
			if (!$count = mysql_num_rows($res)) {
				echo "Found no tracks matching your query.\n\n";
			} else {
				$pl = $count > 1 ? 's' : '';
				echo "Found $count track$pl matching your query.\n<br><br>\n";
				
				// Output
				if ($config['use_categories']) {
					mb_table_start('Track #','Track Name','Length','CD','Category');
				} else {
					mb_table_start('Track #','Track Name','Length','CD');
				}

				while ($row = mysql_fetch_array($res)) {
					mb_table_col($row['trackid']);
					mb_table_col($row['name']);
					mb_table_col(mb_lengthtext($row['length']));
					mb_table_col('<a href="index.php?media='.$row['mediaid'].'">'.$row['medianame'].'</a>');
					if ($config['use_categories']) {
						mb_table_col('<a href="index.php?cat='.$row['catid'].'">'.$row['catname'].'</a>');
					}
				}

				mb_table_end();
			}
		}
	// }}}
	echo "<p><a href=\"$self\">New search</a>\n";
} else {
	showform();
}

require('inc.footer.php');

?>
