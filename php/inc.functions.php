<?php
function mb_get_extension ($filename) // {{{
{
	return strtolower(substr(strrchr($filename,'.'),1));
} // }}}
function mb_chomp ($string) // {{{
{
	return substr($string, 0, -1);
} // }}}
function mb_datetext ($timestamp) // {{{
{
	global $config;
	return date($config['datestring'],$timestamp);
} // }}}
function mb_sizetext ($sizeinbytes) // {{{
{
	global $config;
	// thresholds
	$t_b = 1024; // use bytes when size < 1024
	$t_kb = 1048576; // use kb when size < 1 MB
	if (!$config['human_readable_size']) {
		return "$sizeinbytes";
	}
	if ($sizeinbytes < $t_b) {
		return "$sizeinbytes B";
	} elseif ($sizeinbytes < $t_kb) {
		$kb = floor($sizeinbytes / 1024);
		return "$kb KB";
	} else {
		$mb = floor($sizeinbytes / 1048576);
		return "$mb MB";
	}
} // }}}
function mb_lengthtext ($seconds) // {{{
{
	$minutes = floor($seconds/60);
	$seconds = $seconds % 60;
	if (strlen($seconds) == 1) { $seconds = '0'.$seconds; }
	return "$minutes:$seconds";
} // }}}
function mb_typetext ($type) // {{{
{
	switch ($type) {
		case MB_T_AUDIO:	return 'Audio CD';
		case MB_T_DATA:		return 'Data CD';
		case MB_T_EMPTY:	return 'CD';
		default:			return 'Unknown';
	}
} // }}}
function mb_icon ($extension) // {{{
{
	global $config;
	$icon = $config['icon_prefix'];
	// I'm planning to add icon-by-extension sometime :-)
	switch ($extension) {
		case '__folder':	$icon .= 'folder.png';		break;
		case '__unknown':	$icon .= 'unknown.png';		break;
		case '__category':	$icon .= 'category.png';	break;
		case '__media_cd':	$icon .= 'cd.png';			break;
		case '__media_acd':	$icon .= 'acd.png';			break;
	}
	return "<img alt=\"icon\" src=\"$icon\">&nbsp;";
} // }}}
function mb_iconbytype ($type) // {{{
{
	switch ($type) {
		case MB_T_DATA:		return mb_icon('__media_cd');
		case MB_T_AUDIO:	return mb_icon('__media_acd');
		case MB_T_EMPTY:	return mb_icon('__media_cd');
		default:			return mb_icon('__unknown');
	}
} // }}}
function mb_getparentdir ($dir) // {{{
{	
	// I'm sure there's a better way to do this,
	// I was just lazy. Sorry about that :)
	if ($dir == '/') { return false; }
	$parts = explode('/',$dir);
	array_pop($parts);
	array_pop($parts);
	$dir = implode('/',$parts) . '/';
	return $dir;
} // }}}
function mb_table_start () // {{{
{
	global $TABLE_COLUMNS;
	if (!(isset($TABLE_COLUMNS) && is_array($TABLE_COLUMNS))) {
		$TABLE_COLUMNS = array();
	}
	$c = func_num_args();
	array_unshift($TABLE_COLUMNS,$c);
	echo "<table>\n";
	echo "	<tr>\n";
	for ($x=0;$x<$c;$x++) {
		echo "		<th>".func_get_arg($x)."</th>\n";
	}
	echo "	</tr>\n";
} // }}}
function mb_table_widecol ($content) // {{{
{
	global $TABLE_COLUMNS;
	global $TABLE_ROW;
	global $TABLE_CSSCLASS;

	$cols = $TABLE_COLUMNS[0];

	if (!isset($TABLE_ROW))
  {
		$TABLE_ROW = 1;
	}

  if (($TABLE_ROW % 2) != 0)
  {
    $TABLE_CSSCLASS="OddTableRow";
  }
  else
  {
    $TABLE_CSSCLASS="EvenTableRow";
  }

  $TABLE_ROW++;

	echo "	<tr>\n";
	echo "		<td colspan=\"$cols\" class=\"".$TABLE_CSSCLASS."\">$content</td>\n";
	echo "	</tr>\n";
} // }}}
function mb_table_row () // {{{
{
	echo "	<tr>\n";
	$c = func_num_args();
	echo "	<tr>\n";
	for ($x=0;$x<$c;$x++) {
		echo "		<td>".func_get_arg($x)."</td>\n";
	}
	echo "	</tr>\n";
} // }}}
function mb_table_col ($content)
{
	global $TABLE_COLUMNS;
	global $TABLE_COLUMN;
	global $TABLE_ROW;
	global $TABLE_CSSCLASS;
	$cols = $TABLE_COLUMNS[0];

	if (!isset($TABLE_COLUMN))
  {
		$TABLE_COLUMN = 1;
	}

	if (!isset($TABLE_ROW))
  {
		$TABLE_ROW = 1;
	}

	$curcol = $TABLE_COLUMN++;

	if ($curcol == 1)
  {
    if (($TABLE_ROW % 2) != 0)
    {
      $TABLE_CSSCLASS="OddTableRow";
    }
    else
    {
      $TABLE_CSSCLASS="EvenTableRow";
    }

		$TABLE_ROW++;
    echo "\t<tr>\n";
  }

	echo "		<td class=\"".$TABLE_CSSCLASS."\"><nobr>$content</nobr></td>\n";
	if ($curcol == $cols) { echo "\t</tr>\n"; $TABLE_COLUMN = 1; }
	return;
} // }}}
function mb_table_end () // {{{
{
	global $TABLE_COLUMNS;
	array_shift($TABLE_COLUMNS);
	echo "</table>\n\n";
} // }}}
function mb_fetch_select_categories ($indent=0) // {{{
{
	global $config;
	$indent = str_repeat("\t",$indent);
	if (!$res = mysql_query("SELECT * FROM ".$config['tbl_categories']." ORDER BY name")) {
		return false;
	}
	if (!mysql_num_rows($res)) {
		return true;
	}
	while ($row = mysql_fetch_assoc($res)) {
		echo $indent.'<option value="'.$row['catid'].'">'.$row['name']."</option>\n";
	}
	return true;
} // }}}

function truncate_ellipsis ($string, $max)
{
  if (strlen($string) > $max + 3)
    return substr_replace($string, "...", $max - 3);
  else
    return $string;
}

?>
