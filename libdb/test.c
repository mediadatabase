/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: test.c,v 1.3 2004/05/16 18:58:50 nedko Exp $
 *
 * DESCRIPTION:
 *  libdb linking test.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 * NOTES:
 *  Purpose is to check if there are unresolved externals in libdb.a
 *
 *****************************************************************************/

#include <stdio.h>

#include "../result.h"
#include "libdb.h"

int
main()
{
  volatile int i = 0;

  printf("This linking test, there is no need to run it.");

  if (i)
  {
    mediadb_open(
      MEDIADB_DBTYPE_MYSQL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL);

    mediadb_media_add_new(
      MEDIADB_INVALID_VALUE,
      NULL,
      NULL,
      MEDIADB_MT_EMPTY,
      NULL);

    mediadb_media_update_properties(
      MEDIADB_INVALID_VALUE,
      0,
      0,
      0,
      0);

    mediadb_file_add_new(
      MEDIADB_INVALID_VALUE,
      0,
      MEDIADB_FILETYPE_FILE,
      NULL,
      NULL,
      0,
      0);

    mediadb_close(MEDIADB_INVALID_VALUE);
  }

  return 0;
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: test.c,v $
 *   Revision 1.3  2004/05/16 18:58:50  nedko
 *   mediadb_result has became more general, it is not specific to libdb
 *
 *   Revision 1.2  2004/05/02 13:16:58  nedko
 *   Supply filetype when adding new file
 *
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
