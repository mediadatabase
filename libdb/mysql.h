/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: mysql.h,v 1.9 2005/03/05 21:39:44 nedko Exp $
 *
 * DESCRIPTION:
 *  MySQL backend header.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef MYSQL_H__13CFC3E2_C74B_435B_B843_6547D43FEB52__INCLUDED
#define MYSQL_H__13CFC3E2_C74B_435B_B843_6547D43FEB52__INCLUDED

#include <mysql/mysql.h>

typedef struct
{
  MYSQL *pMYSQL;
  char *pszSQLQueryBuffer;
  size_t sizeSQLQueryBuffer;
  char *pErrorMsgBuffer;
  size_t sizeErrorMsgBuffer;
} mediadb_mysql;

mediadb_result
mediadb_mysql_open(
  mediadb_mysql *pDB,
  const char *pszHost,
  const char *pszUser,
  const char *pszPass,
  const char *pszDB);

mediadb_result
mediadb_mysql_media_add_new(
  mediadb_mysql *pDB,
  const char *pszName,
  const char *pszComment,
  mediadb_mediatype nType,
  mediadb_uint *pnNewMediaID);

mediadb_result
mediadb_mysql_media_update_properties(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize);

mediadb_result
mediadb_mysql_file_add_new(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  mediadb_filetype Filetype,
  const char *pszPath,
  const char *pszName,
  mediadb_uint nFileSize,
  mediadb_uint nFileTime);

mediadb_result
mediadb_mysql_media_get_properties(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTimeAdded,
  mediadb_mediatype *pnType,
  mediadb_uint *pnLocationID,
  char **ppszTitle);

mediadb_result
mediadb_mysql_location_get_properties(
  mediadb_mysql *pDB,
  mediadb_uint nLocationID,
  mediadb_uint *pnLocationTypeID,
  char **ppszDescription);

mediadb_result
mediadb_mysql_location_type_get_properties(
  mediadb_mysql *pDB,
  mediadb_uint nLocationTypeID,
  char **ppszDescription);

mediadb_result
mediadb_mysql_media_get_properties_data(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTotalFiles,
  mediadb_uint *pnTotalSize);

mediadb_result
mediadb_mysql_delete_media_files(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID);

mediadb_result
mediadb_mysql_media_update_name(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  const char *pszName);

mediadb_result
mediadb_mysql_close(
  mediadb_mysql *pDB);

const char *
mediadb_mysql_get_error_message(
  mediadb_mysql *pDB);

mediadb_result
mediadb_mysql_media_get_all(
  mediadb_mysql *pDB,
  mediadb_media_callback pCallback,
  void *pUserContext);

mediadb_result
mediadb_mysql_files_get(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  mediadb_files_callback pCallback,
  void *pUserContext);

mediadb_result
mediadb_mysql_get_pattern_match_methods(
  mediadb_mysql *pDB,
  const struct mediadb_pattern_match_method **ppPMM);

mediadb_result
mediadb_mysql_files_search(
  mediadb_mysql *pDB,
  mediadb_uint nFilenamePMMID,
  const char *pszFilenamePattern,
  mediadb_uint nPathPMMID,
  const char *pszPathPattern,
  const mediadb_uint *pnMinSize,
  const mediadb_uint *pnMaxSize,
  mediadb_files_search_callback pCallback,
  void *pUserContext);

mediadb_result
mediadb_mysql_file_get_properties(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype *pFiletype,
  mediadb_uint *pnSize,
  mediadb_uint *pnTime);

#endif /* #ifndef MYSQL_H__13CFC3E2_C74B_435B_B843_6547D43FEB52__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: mysql.h,v $
 *   Revision 1.9  2005/03/05 21:39:44  nedko
 *   new functionality - file_get_properties()
 *
 *   Revision 1.8  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *   Revision 1.7  2004/08/08 00:47:42  nedko
 *   Get more info for media from database.
 *
 *   Revision 1.6  2004/05/21 23:40:46  nedko
 *   New functionality: mediadb_files_get()
 *
 *   Revision 1.5  2004/05/16 18:57:12  nedko
 *   media_get_all functionality implemented.
 *
 *   Revision 1.4  2004/05/03 20:47:15  nedko
 *   Update mode for cui
 *
 *   Revision 1.3  2004/05/02 20:12:11  nedko
 *   Improve error dumps.
 *
 *   Revision 1.2  2004/05/02 13:16:58  nedko
 *   Supply filetype when adding new file
 *
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
