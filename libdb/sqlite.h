/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: sqlite.h,v 1.10 2005/03/05 21:39:44 nedko Exp $
 *
 * DESCRIPTION:
 *  SQLite backend header.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef SQLITE_H__B1AEAB83_AA4D_4E16_9036_43389679E923__INCLUDED
#define SQLITE_H__B1AEAB83_AA4D_4E16_9036_43389679E923__INCLUDED

#include <sqlite.h>

typedef struct
{
  sqlite * hDB;
  char *pErrorMsgBuffer;
  size_t sizeErrorMsgBuffer;
} mediadb_sqlite;

mediadb_result
mediadb_sqlite_open(
  mediadb_sqlite *pDB,
  const char *pszHost,
  const char *pszUser,
  const char *pszPass,
  const char *pszDB);

mediadb_result
mediadb_sqlite_media_add_new(
  mediadb_sqlite *pDB,
  const char *pszName,
  const char *pszComment,
  mediadb_mediatype nType,
  mediadb_uint *pnNewMediaID);

mediadb_result
mediadb_sqlite_media_update_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize);

mediadb_result
mediadb_sqlite_file_add_new(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  mediadb_filetype Filetype,
  const char *pszPath,
  const char *pszName,
  mediadb_uint nFileSize,
  mediadb_uint nFileTime);

mediadb_result
mediadb_sqlite_media_get_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTimeAdded,
  mediadb_mediatype *pnType,
  mediadb_uint *pnLocationID,
  char **ppszTitle);

mediadb_result
mediadb_sqlite_location_get_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nLocationID,
  mediadb_uint *pnLocationTypeID,
  char **ppszDescription);

mediadb_result
mediadb_sqlite_location_type_get_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nLocationTypeID,
  char **ppszDescription);

mediadb_result
mediadb_sqlite_media_get_properties_data(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTotalFiles,
  mediadb_uint *pnTotalSize);

mediadb_result
mediadb_sqlite_delete_media_files(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID);

mediadb_result
mediadb_sqlite_media_update_name(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  const char *pszName);

mediadb_result
mediadb_sqlite_close(
  mediadb_sqlite *pDB);

const char *
mediadb_sqlite_get_error_message(
  mediadb_sqlite *pDB);

mediadb_result
mediadb_sqlite_media_get_all(
  mediadb_sqlite *pDB,
  mediadb_media_callback pCallback,
  void *pUserContext);

mediadb_result
mediadb_sqlite_files_get(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  mediadb_files_callback pCallback,
  void *pUserContext);

mediadb_result
mediadb_sqlite_get_pattern_match_methods(
  mediadb_sqlite *pDB,
  const struct mediadb_pattern_match_method **ppPMM);

mediadb_result
mediadb_sqlite_files_search(
  mediadb_sqlite *pDB,
  mediadb_uint nFilenamePMMID,
  const char *pszFilenamePattern,
  mediadb_uint nPathPMMID,
  const char *pszPathPattern,
  const mediadb_uint *pnMinSize,
  const mediadb_uint *pnMaxSize,
  mediadb_files_search_callback pCallback,
  void *pUserContext);

mediadb_result
mediadb_sqlite_file_get_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype *pFiletype,
  mediadb_uint *pnSize,
  mediadb_uint *pnTime);

#endif /* #ifndef SQLITE_H__B1AEAB83_AA4D_4E16_9036_43389679E923__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: sqlite.h,v $
 *   Revision 1.10  2005/03/05 21:39:44  nedko
 *   new functionality - file_get_properties()
 *
 *   Revision 1.9  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *   Revision 1.8  2004/08/08 00:47:42  nedko
 *   Get more info for media from database.
 *
 *   Revision 1.7  2004/05/21 23:40:46  nedko
 *   New functionality: mediadb_files_get()
 *
 *   Revision 1.6  2004/05/16 18:57:12  nedko
 *   media_get_all functionality implemented.
 *
 *   Revision 1.5  2004/05/11 01:18:53  nedko
 *   Implement SQLite backend.
 *
 *   Revision 1.4  2004/05/03 20:47:15  nedko
 *   Update mode for cui
 *
 *   Revision 1.3  2004/05/02 20:12:10  nedko
 *   Improve error dumps.
 *
 *   Revision 1.2  2004/05/02 13:16:58  nedko
 *   Supply filetype when adding new file
 *
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
