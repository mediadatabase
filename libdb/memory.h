/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: memory.h,v 1.2 2004/05/16 19:10:54 nedko Exp $
 *
 * DESCRIPTION:
 *  Memory helper declarations.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef MEMORY_H__EA729AC1_5119_42E4_9E89_9F435EED5B4F__INCLUDED
#define MEMORY_H__EA729AC1_5119_42E4_9E89_9F435EED5B4F__INCLUDED

mediadb_result
maybe_enlarge_buffer(
  char ** ppBuffer,
  size_t *pnBufferSize,
  size_t nSizeRequired);

#endif /* #ifndef MEMORY_H__EA729AC1_5119_42E4_9E89_9F435EED5B4F__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: memory.h,v $
 *   Revision 1.2  2004/05/16 19:10:54  nedko
 *   Write header information.
 *
 *   Revision 1.1  2004/05/02 20:12:11  nedko
 *   Improve error dumps.
 *
 *****************************************************************************/
