/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: libdb.c,v 1.10 2005/03/05 21:39:44 nedko Exp $
 *
 * DESCRIPTION:
 *  Unified access to database.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *  
 *****************************************************************************/

#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "../result.h"
#include "internal.h"
#include "memory.h"

char *pszErrorMsg = NULL;

mediadb_result
mediadb_open(
  unsigned int nDBType,
  const char *pszHost,
  const char *pszUser,
  const char *pszPass,
  const char *pszDB,
  mediadb *phDB)
{
  mediadb_result r;
  mediadb_impl *pImpl;

  pImpl = malloc(sizeof(mediadb_impl));
  if (pImpl == NULL)
  {
    r = MEDIADB_MEM;
    goto Fail;
  }

  pImpl->nDBType = nDBType;

  if (nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_open(
      &pImpl->data.mysql,
      pszHost,
      pszUser,
      pszPass,
      pszDB);
    if (MEDIADB_IS_ERROR(r))
    {
      if (pszErrorMsg != NULL)
        free(pszErrorMsg);
      pszErrorMsg = strdup(mediadb_mysql_get_error_message(&pImpl->data.mysql));
      goto FailFreeMem;
    }
  }
  else if (nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_open(
      &pImpl->data.sqlite,
      pszHost,
      pszUser,
      pszPass,
      pszDB);
    if (MEDIADB_IS_ERROR(r))
    {
      if (pszErrorMsg != NULL)
        free(pszErrorMsg);
      pszErrorMsg = strdup(mediadb_sqlite_get_error_message(&pImpl->data.sqlite));
      goto FailFreeMem;
    }
  }
  else
  {
    r = MEDIADB_NOT_IMPL;
    goto FailFreeMem;
  }

  *phDB = (mediadb)pImpl;

  return MEDIADB_OK;

FailFreeMem:
  free(pImpl);
Fail:
  return r;
}

#define pDB ((mediadb_impl *)hDB)

mediadb_result
mediadb_media_add_new(
  mediadb hDB,
  const char *pszName,
  const char *pszComment,
  mediadb_mediatype nType,
  mediadb_uint *pnNewMediaID)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_media_add_new(
      &pDB->data.mysql,
      pszName,
      pszComment,
      nType,
      pnNewMediaID);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_media_add_new(
      &pDB->data.sqlite,
      pszName,
      pszComment,
      nType,
      pnNewMediaID);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_media_update_properties(
  mediadb hDB,
  mediadb_uint nMediaID,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_media_update_properties(
      &pDB->data.mysql,
      nMediaID,
      nTimeAdded,
      nTotalFiles,
      nTotalSize);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_media_update_properties(
      &pDB->data.sqlite,
      nMediaID,
      nTimeAdded,
      nTotalFiles,
      nTotalSize);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_file_add_new(
  mediadb hDB,
  mediadb_uint nMediaID,
  mediadb_filetype Filetype,
  const char *pszPath,
  const char *pszName,
  mediadb_uint nFileSize,
  mediadb_uint nFileTime)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_file_add_new(
      &pDB->data.mysql,
      nMediaID,
      Filetype,
      pszPath,
      pszName,
      nFileSize,
      nFileTime);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_file_add_new(
      &pDB->data.sqlite,
      nMediaID,
      Filetype,
      pszPath,
      pszName,
      nFileSize,
      nFileTime);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_media_get_properties(
  mediadb hDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTimeAdded,
  mediadb_mediatype *pnType,
  mediadb_uint *pnLocationID,
  char **ppszTitle)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_media_get_properties(
      &pDB->data.mysql,
      nMediaID,
      pnTimeAdded,
      pnType,
      pnLocationID,
      ppszTitle);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_media_get_properties(
      &pDB->data.sqlite,
      nMediaID,
      pnTimeAdded,
      pnType,
      pnLocationID,
      ppszTitle);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_location_get_properties(
  mediadb hDB,
  mediadb_uint nLocationID,
  mediadb_uint *pnLocationTypeID,
  char **ppszDescription)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_location_get_properties(
      &pDB->data.mysql,
      nLocationID,
      pnLocationTypeID,
      ppszDescription);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_location_get_properties(
      &pDB->data.sqlite,
      nLocationID,
      pnLocationTypeID,
      ppszDescription);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_location_type_get_properties(
  mediadb hDB,
  mediadb_uint nLocationTypeID,
  char **ppszDescription)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_location_type_get_properties(
      &pDB->data.mysql,
      nLocationTypeID,
      ppszDescription);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_location_type_get_properties(
      &pDB->data.sqlite,
      nLocationTypeID,
      ppszDescription);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_media_get_properties_data(
  mediadb hDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTotalFiles,
  mediadb_uint *pnTotalSize)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_media_get_properties_data(
      &pDB->data.mysql,
      nMediaID,
      pnTotalFiles,
      pnTotalSize);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_media_get_properties_data(
      &pDB->data.sqlite,
      nMediaID,
      pnTotalFiles,
      pnTotalSize);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_delete_media_files(
  mediadb hDB,
  mediadb_uint nMediaID)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_delete_media_files(
      &pDB->data.mysql,
      nMediaID);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_delete_media_files(
      &pDB->data.sqlite,
      nMediaID);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_media_update_name(
  mediadb hDB,
  mediadb_uint nMediaID,
  const char *pszName)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_media_update_name(
      &pDB->data.mysql,
      nMediaID,
      pszName);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_media_update_name(
      &pDB->data.sqlite,
      nMediaID,
      pszName);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_close(
  mediadb hDB)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_close(
      &pDB->data.mysql);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_close(
      &pDB->data.sqlite);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  if (MEDIADB_IS_ERROR(r))
  {
    return r;
  }

  free(pDB);

  return r;
}

const char *
mediadb_get_error_message(
  mediadb hDB)
{
  if (pDB == NULL)
  {
    if (pszErrorMsg == NULL)
    {
      return "";
    }
    else
    {
      return pszErrorMsg;
    }
  }

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    return mediadb_mysql_get_error_message(&pDB->data.mysql);
  }

  if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    return mediadb_sqlite_get_error_message(&pDB->data.sqlite);
  }

  assert(0);
  return "enexpected behaviour";
}

mediadb_result
mediadb_media_get_all(
  mediadb hDB,
  mediadb_media_callback pCallback,
  void *pUserContext)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_media_get_all(
      &pDB->data.mysql,
      pCallback,
      pUserContext);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_media_get_all(
      &pDB->data.sqlite,
      pCallback,
      pUserContext);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_files_get(
  mediadb hDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  mediadb_files_callback pCallback,
  void *pUserContext)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_files_get(
      &pDB->data.mysql,
      nMediaID,
      pszPath,
      pCallback,
      pUserContext);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_files_get(
      &pDB->data.sqlite,
      nMediaID,
      pszPath,
      pCallback,
      pUserContext);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_get_pattern_match_methods(
  mediadb hDB,
  const struct mediadb_pattern_match_method **ppPMM)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_get_pattern_match_methods(
      &pDB->data.mysql,
      ppPMM);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_get_pattern_match_methods(
      &pDB->data.sqlite,
      ppPMM);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_files_search(
  mediadb hDB,
  mediadb_uint nFilenamePMMID,
  const char *pszFilenamePattern,
  mediadb_uint nPathPMMID,
  const char *pszPathPattern,
  const mediadb_uint *pnMinSize,
  const mediadb_uint *pnMaxSize,
  mediadb_files_search_callback pCallback,
  void *pUserContext)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_files_search(
      &pDB->data.mysql,
      nFilenamePMMID,
      pszFilenamePattern,
      nPathPMMID,
      pszPathPattern,
      pnMinSize,
      pnMaxSize,
      pCallback,
      pUserContext);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_files_search(
      &pDB->data.sqlite,
      nFilenamePMMID,
      pszFilenamePattern,
      nPathPMMID,
      pszPathPattern,
      pnMinSize,
      pnMaxSize,
      pCallback,
      pUserContext);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

mediadb_result
mediadb_file_get_properties(
  mediadb hDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype *pFiletype,
  mediadb_uint *pnSize,
  mediadb_uint *pnTime)
{
  mediadb_result r;

  if (pDB->nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_mysql_file_get_properties(
      &pDB->data.mysql,
      nMediaID,
      pszPath,
      pszName,
      pFiletype,
      pnSize,
      pnTime);
  }
  else if (pDB->nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_sqlite_file_get_properties(
      &pDB->data.sqlite,
      nMediaID,
      pszPath,
      pszName,
      pFiletype,
      pnSize,
      pnTime);
  }
  else
  {
    r = MEDIADB_INVAL_ARG;
    assert(0);
  }

  return r;
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: libdb.c,v $
 *   Revision 1.10  2005/03/05 21:39:44  nedko
 *   new functionality - file_get_properties()
 *
 *   Revision 1.9  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *   Revision 1.8  2004/08/08 00:47:42  nedko
 *   Get more info for media from database.
 *
 *   Revision 1.7  2004/05/21 23:40:46  nedko
 *   New functionality: mediadb_files_get()
 *
 *   Revision 1.6  2004/05/16 18:57:12  nedko
 *   media_get_all functionality implemented.
 *
 *   Revision 1.5  2004/05/11 01:17:42  nedko
 *   Allow mediadb_get_error_message() to work after failed database open.
 *
 *   Revision 1.4  2004/05/03 20:47:14  nedko
 *   Update mode for cui
 *
 *   Revision 1.3  2004/05/02 20:12:11  nedko
 *   Improve error dumps.
 *
 *   Revision 1.2  2004/05/02 13:16:58  nedko
 *   Supply filetype when adding new file
 *
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
