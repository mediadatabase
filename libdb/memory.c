/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: memory.c,v 1.3 2004/05/16 19:10:54 nedko Exp $
 *
 * DESCRIPTION:
 *  Memory helper implementations.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "../result.h"
#include "libdb.h"
#include "memory.h"

#define BUFFER_INITIAL_ADD 4096
#define BUFFER_ADD         4096

mediadb_result
maybe_enlarge_buffer(
  char ** ppBuffer,
  size_t *pnBufferSize,
  size_t nSizeRequired)
{
  char *pNewBuffer;
  size_t nNewBufferSize;

  if (nSizeRequired <= *pnBufferSize)
    return MEDIADB_OK;

  if (*pnBufferSize == 0 || *ppBuffer == NULL)
  {
    /* Allocate */

    nNewBufferSize = nSizeRequired + BUFFER_INITIAL_ADD;

    pNewBuffer = (char *)malloc(nNewBufferSize);
    if (pNewBuffer == NULL)
    {
      //"Cannot allocate %u bytes of memory.",
      //(unsigned int)nNewBufferSize
      return MEDIADB_MEM;
    }
  }
  else
  {
    /* Reallocate */

    nNewBufferSize = nSizeRequired + BUFFER_ADD;

    pNewBuffer = (char *)malloc(nNewBufferSize);
    if (pNewBuffer == NULL)
    {
      // "Cannot allocate %u bytes of memory.",
      //(unsigned int)nNewBufferSize
      return MEDIADB_MEM;
    }

    memcpy(pNewBuffer, *ppBuffer, *pnBufferSize);

    free(*ppBuffer);
  }


  *pnBufferSize = nNewBufferSize;
  *ppBuffer = pNewBuffer;

  return MEDIADB_OK;
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: memory.c,v $
 *   Revision 1.3  2004/05/16 19:10:54  nedko
 *   Write header information.
 *
 *   Revision 1.2  2004/05/16 18:58:50  nedko
 *   mediadb_result has became more general, it is not specific to libdb
 *
 *   Revision 1.1  2004/05/02 20:12:11  nedko
 *   Improve error dumps.
 *
 *****************************************************************************/
