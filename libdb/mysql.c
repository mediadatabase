/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: mysql.c,v 1.13 2005/03/05 21:39:44 nedko Exp $
 *
 * DESCRIPTION:
 *  MySQL backend implementation.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "../result.h"
#include "internal.h"
#include "memory.h"

#define MEDIADB_MYSQL_MEDIA_TABLE "media"
#define MEDIADB_MYSQL_FILES_TABLE "files"
#define MEDIADB_MYSQL_LOCATIONS_TABLE "locations"
#define MEDIADB_MYSQL_LOCATION_TYPES_TABLE "location_types"

//#define DUMMY_SQL_QUERIES
#define FORMATTING_BUFFER_SIZE 10240

#define MEDIADB_MYSQL_PMM_EXACT_CASE_INSENSITIVE    1
#define MEDIADB_MYSQL_PMM_EXACT_CASE_SENSITIVE      2
#define MEDIADB_MYSQL_PMM_LIKE_CASE_INSENSITIVE     3
#define MEDIADB_MYSQL_PMM_LIKE_CASE_SENSITIVE       4
#define MEDIADB_MYSQL_PMM_REGEXP_CASE_INSENSITIVE   5
#define MEDIADB_MYSQL_PMM_REGEXP_CASE_SENSITIVE     6

static struct mediadb_pattern_match_method 
mediadb_mysql_pattern_match_methods[] =
{
  {
    MEDIADB_MYSQL_PMM_REGEXP_CASE_INSENSITIVE,
    "ERE - case insensitive",
    "Extended regular expressions, ignore case",
    "Extended regular expressions are powerful pattern matching method.\n"
    "Some characteristics of extended regular expressions are:\n"
    " * `.' matches any single character.\n"
    " * A character class `[...]' matches any character within the brackets. "
    "   For example, `[abc]' matches `a', `b', or `c'. To name a range of characters, "
    "   use a dash. `[a-z]' matches any letter, whereas `[0-9]' matches any digit.\n"
    " * `*' matches zero or more instances of the thing preceding it. "
    "   For example, `x*' matches any number of `x' characters, `[0-9]*' matches any "
    "   number of digits, and `.*' matches any number of anything.\n"
    " * A REGEXP pattern match succeed if the pattern matches anywhere in the value "
    "   being tested.\n"
    " * To anchor a pattern so that it must match the beginning or end of the value "
    "   being tested, use `^' at the beginning or `$' at the end of the pattern."
  },
  {
    MEDIADB_MYSQL_PMM_REGEXP_CASE_SENSITIVE,
    "ERE - case sensitive",
    "Extended regular expressions, do not ignore case",
    "Extended regular expressions are powerful pattern matching method.\n"
    "Some characteristics of extended regular expressions are:\n"
    " * `.' matches any single character.\n"
    " * A character class `[...]' matches any character within the brackets. "
    "   For example, `[abc]' matches `a', `b', or `c'. To name a range of characters, "
    "   use a dash. `[a-z]' matches any letter, whereas `[0-9]' matches any digit.\n"
    " * `*' matches zero or more instances of the thing preceding it. "
    "   For example, `x*' matches any number of `x' characters, `[0-9]*' matches any "
    "   number of digits, and `.*' matches any number of anything.\n"
    " * A REGEXP pattern match succeed if the pattern matches anywhere in the value "
    "   being tested.\n"
    " * To anchor a pattern so that it must match the beginning or end of the value "
    "   being tested, use `^' at the beginning or `$' at the end of the pattern."
  },
  {
    MEDIADB_MYSQL_PMM_LIKE_CASE_INSENSITIVE,
    "SQL \"LIKE\" - case insensitive",
    "Traditional SQL \"LIKE\" oprator pattern matching, ignore case",
    "SQL pattern matching allows you to use `_' to match any single character "
    "and `%' to match an arbitrary number of characters (including zero characters). "
    "LIKE pattern match succeeds only if the pattern matches the entire value."
  },
  {
    MEDIADB_MYSQL_PMM_LIKE_CASE_SENSITIVE,
    "SQL \"LIKE\" - case sensitive",
    "Traditional SQL \"LIKE\" oprator pattern matching, do not ignore case",
    "SQL pattern matching allows you to use `_' to match any single character "
    "and `%' to match an arbitrary number of characters (including zero characters). "
    "LIKE pattern match succeeds only if the pattern matches the entire value."
  },
  {
    MEDIADB_MYSQL_PMM_EXACT_CASE_INSENSITIVE,
    "exact - case insensitive",
    "Match exactly, ignore case",
    "Char by char match"
  },
  {
    MEDIADB_MYSQL_PMM_EXACT_CASE_SENSITIVE,
    "exact - case sensitive",
    "Match exactly, do not ignore case",
    "Char by char match"
  },
  {
    MEDIADB_PMM_NULL,
    NULL,
    NULL
  }
};

static
void
set_error_message(
  mediadb_mysql *pDB,
  const char *pszFormat,
  ...)
{
  mediadb_result r;
  size_t s;
  va_list argList;
  char Buffer[FORMATTING_BUFFER_SIZE];

  va_start(argList, pszFormat);
  vsnprintf(Buffer, FORMATTING_BUFFER_SIZE-1, pszFormat, argList);
  va_end(argList);

  s = strlen(Buffer);

  r = maybe_enlarge_buffer(
    &pDB->pErrorMsgBuffer,
    &pDB->sizeErrorMsgBuffer,
    s+1);
  if (MEDIADB_IS_ERROR(r))
  {
    if (pDB->sizeErrorMsgBuffer > 0)
    {
      pDB->pErrorMsgBuffer[0] = 0;
    }
  }

  memcpy(pDB->pErrorMsgBuffer, Buffer, s + 1);
}

static
void
reset_error_message(
  mediadb_mysql *pDB)
{
  if (pDB->pErrorMsgBuffer != NULL)
  {
    pDB->pErrorMsgBuffer[0] = 0;
  }
}

const char *
mediadb_mysql_get_error_message(
  mediadb_mysql *pDB)
{
  if (pDB->pErrorMsgBuffer == NULL)
  {
    return "";
  }

  return pDB->pErrorMsgBuffer;
}

mediadb_result
mediadb_mysql_open(
  mediadb_mysql *pDB,
  const char *pszHost,
  const char *pszUser,
  const char *pszPass,
  const char *pszDB)
{
  MYSQL *pMYSQL;

  pDB->pszSQLQueryBuffer = NULL;
  pDB->sizeSQLQueryBuffer = 0;
  pDB->pErrorMsgBuffer = NULL;
  pDB->sizeErrorMsgBuffer = 0;

  pMYSQL = mysql_init(NULL);
  if (pMYSQL == NULL)
  {
    set_error_message(
      pDB,
      "Cannot create MYSQL object. Error is \"%s\"",
      mysql_error(pMYSQL));
    return MEDIADB_FAIL;
  }

  if (mysql_real_connect(
        pMYSQL,
        pszHost,
        pszUser,
        pszPass,
        pszDB,
        0,
        NULL,
        0) == NULL)
  {
    set_error_message(
      pDB,
      "Failed to connect to database. Error: %s",
      mysql_error(pMYSQL));
    mysql_close(pMYSQL);
    return MEDIADB_FAIL;
  }

  pDB->pMYSQL = pMYSQL;

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_media_add_new(
  mediadb_mysql *pDB,
  const char *pszName,
  const char *pszComment,
  mediadb_mediatype nType,
  mediadb_uint *pnNewMediaID)
{
  mediadb_result r;
  size_t sizeName, sizeComment, sizePart;
  char *psz, *pszPart;
  my_ulonglong nMediaID;

  /* Ensure we have large enough buffer */

  sizeName = strlen(pszName);
  sizeComment = strlen(pszComment);

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    sizeName*2 + sizeComment*2 + 1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  psz = pDB->pszSQLQueryBuffer;

  pszPart = "INSERT INTO " MEDIADB_MYSQL_MEDIA_TABLE " (name,type,comment) VALUES ('";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszName,
    sizeName);
  psz += sizePart;

  sizePart = sprintf(psz, "',%u,'", (unsigned int)nType);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszComment,
    sizeComment);
  psz += sizePart;

  pszPart = "')";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  *psz = 0;

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
  nMediaID = 123;
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nMediaID = mysql_insert_id(pDB->pMYSQL);
  if (nMediaID == 0ULL)
  {
    set_error_message(
      pDB,
      "Invalid media ID 0 after media INSERT");
    return MEDIADB_FAIL;
  }
#endif

  *pnNewMediaID = nMediaID;

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_media_update_properties(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize)
{
  mediadb_result r;

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  sprintf(
    pDB->pszSQLQueryBuffer,
    "UPDATE " MEDIADB_MYSQL_MEDIA_TABLE " SET added=%u, info1=%u, info2=%u WHERE mediaid = %u",
    (unsigned int)nTimeAdded,
    (unsigned int)nTotalFiles,
    (unsigned int)nTotalSize,
    (unsigned int)nMediaID);

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_file_add_new(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  mediadb_filetype Filetype,
  const char *pszPath,
  const char *pszName,
  mediadb_uint nFileSize,
  mediadb_uint nFileTime)
{
  mediadb_result r;
  size_t sizePath, sizeName, sizePart;
  char *psz, *pszPart;

  /* Ensure we have large enough buffer */

  sizePath = strlen(pszPath);
  sizeName = strlen(pszName);

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    sizePath*2 + sizeName*2 + 1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  psz = pDB->pszSQLQueryBuffer;

  sizePart = sprintf(
    psz,
    "INSERT INTO " MEDIADB_MYSQL_FILES_TABLE " (mediaid,size,time,path,name) VALUES (%u,%u,%u,'",
    (unsigned int)nMediaID,
    (unsigned int)nFileSize,
    (unsigned int)nFileTime);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszPath,
    sizePath);
  psz += sizePart;

  pszPart = "','";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszName,
    sizeName);
  psz += sizePart;

  if (Filetype == MEDIADB_FILETYPE_DIR)
  {
    pszPart = "/";
    sizePart = strlen(pszPart);
    memcpy(psz, pszPart, sizePart);
    psz += sizePart;
  }
  else if (Filetype != MEDIADB_FILETYPE_FILE)
  {
    set_error_message(
      pDB,
      "Cannot add file of unknown type");
    return MEDIADB_INVAL_ARG;
  }

  pszPart = "')";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  *psz = 0;

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_media_get_properties(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTimeAdded,
  mediadb_mediatype *pnType,
  mediadb_uint *pnLocationID,
  char **ppszTitle)
{
  mediadb_result r;
  MYSQL_RES * sqlresult;
  my_ulonglong nRowCount;
  MYSQL_ROW sqlrow;

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  sprintf(
    pDB->pszSQLQueryBuffer,
    "SELECT added, type, name, location FROM " MEDIADB_MYSQL_MEDIA_TABLE " WHERE mediaid = %u",
    (unsigned int)nMediaID);

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  sqlresult = mysql_store_result(pDB->pMYSQL);
  if (sqlresult == NULL)
  {
    set_error_message(
      pDB,
      "Failed to store result after query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nRowCount = mysql_num_rows(sqlresult);
  if (nRowCount != 1)
  {
    set_error_message(
      pDB,
      "%u rows, insted of 1, after query \"%s\"",
      (unsigned int)nRowCount,
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  sqlrow = mysql_fetch_row(sqlresult);
  if (sqlrow == NULL)
  {
    set_error_message(
      pDB,
      "Failed to fetch row after query \"%s\"",
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  if (sqlrow[0] == NULL)
  {
    *pnTimeAdded = 0;
  }
  else
  {
    *pnTimeAdded = strtoull(sqlrow[0], NULL, 10);
  }

  if (sqlrow[1] == NULL)
  {
    *pnType = MEDIADB_MT_UNKNOWN;
  }
  else
  {
    *pnType = strtoull(sqlrow[1], NULL, 10);
  }

  if (sqlrow[2] == NULL)
  {
    *ppszTitle = strdup("");
  }
  else
  {
    *ppszTitle = strdup(sqlrow[2]);
  }

  if (sqlrow[3] == NULL)
  {
    *pnLocationID = 0;
  }
  else
  {
    *pnLocationID = strtoull(sqlrow[3], NULL, 10);
  }

  mysql_free_result(sqlresult);

  if (*ppszTitle == NULL)
  {
    set_error_message(
      pDB,
      "Out of memory");
    return MEDIADB_MEM;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_location_get_properties(
  mediadb_mysql *pDB,
  mediadb_uint nLocationID,
  mediadb_uint *pnLocationTypeID,
  char **ppszDescription)
{
  mediadb_result r;
  MYSQL_RES * sqlresult;
  my_ulonglong nRowCount;
  MYSQL_ROW sqlrow;

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  sprintf(
    pDB->pszSQLQueryBuffer,
    "SELECT type, description FROM " MEDIADB_MYSQL_LOCATIONS_TABLE " WHERE id = %u",
    (unsigned int)nLocationID);

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  sqlresult = mysql_store_result(pDB->pMYSQL);
  if (sqlresult == NULL)
  {
    set_error_message(
      pDB,
      "Failed to store result after query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nRowCount = mysql_num_rows(sqlresult);
  if (nRowCount != 1)
  {
    set_error_message(
      pDB,
      "%u rows, insted of 1, after query \"%s\"",
      (unsigned int)nRowCount,
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  sqlrow = mysql_fetch_row(sqlresult);
  if (sqlrow == NULL)
  {
    set_error_message(
      pDB,
      "Failed to fetch row after query \"%s\"",
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  if (sqlrow[0] == NULL)
  {
    *pnLocationTypeID = 0;
  }
  else
  {
    *pnLocationTypeID = strtoull(sqlrow[0], NULL, 10);
  }

  if (sqlrow[1] == NULL)
  {
    *ppszDescription = strdup("");
  }
  else
  {
    *ppszDescription = strdup(sqlrow[1]);
  }

  mysql_free_result(sqlresult);

  if (*ppszDescription == NULL)
  {
    set_error_message(
      pDB,
      "Out of memory");
    return MEDIADB_MEM;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_location_type_get_properties(
  mediadb_mysql *pDB,
  mediadb_uint nLocationTypeID,
  char **ppszDescription)
{
  mediadb_result r;
  MYSQL_RES * sqlresult;
  my_ulonglong nRowCount;
  MYSQL_ROW sqlrow;

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  sprintf(
    pDB->pszSQLQueryBuffer,
    "SELECT description FROM " MEDIADB_MYSQL_LOCATION_TYPES_TABLE " WHERE id = %u",
    (unsigned int)nLocationTypeID);

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  sqlresult = mysql_store_result(pDB->pMYSQL);
  if (sqlresult == NULL)
  {
    set_error_message(
      pDB,
      "Failed to store result after query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nRowCount = mysql_num_rows(sqlresult);
  if (nRowCount != 1)
  {
    set_error_message(
      pDB,
      "%u rows, insted of 1, after query \"%s\"",
      (unsigned int)nRowCount,
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  sqlrow = mysql_fetch_row(sqlresult);
  if (sqlrow == NULL)
  {
    set_error_message(
      pDB,
      "Failed to fetch row after query \"%s\"",
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  if (sqlrow[0] == NULL)
  {
    *ppszDescription = strdup("");
  }
  else
  {
    *ppszDescription = strdup(sqlrow[0]);
  }

  mysql_free_result(sqlresult);

  if (*ppszDescription == NULL)
  {
    set_error_message(
      pDB,
      "Out of memory");
    return MEDIADB_MEM;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_media_get_properties_data(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTotalFiles,
  mediadb_uint *pnTotalSize)
{
  mediadb_result r;
  MYSQL_RES * sqlresult;
  my_ulonglong nRowCount;
  MYSQL_ROW sqlrow;

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  sprintf(
    pDB->pszSQLQueryBuffer,
    "SELECT info1, info2 FROM " MEDIADB_MYSQL_MEDIA_TABLE " WHERE mediaid = %u",
    (unsigned int)nMediaID);

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  sqlresult = mysql_store_result(pDB->pMYSQL);
  if (sqlresult == NULL)
  {
    set_error_message(
      pDB,
      "Failed to store result after query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nRowCount = mysql_num_rows(sqlresult);
  if (nRowCount != 1)
  {
    set_error_message(
      pDB,
      "%u rows, insted of 1, after query \"%s\"",
      (unsigned int)nRowCount,
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  sqlrow = mysql_fetch_row(sqlresult);
  if (sqlrow == NULL)
  {
    set_error_message(
      pDB,
      "Failed to fetch row after query \"%s\"",
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  if (sqlrow[0] == NULL)
  {
    *pnTotalFiles = 0;
  }
  else
  {
    *pnTotalFiles = strtoull(sqlrow[0], NULL, 10);
  }

  if (sqlrow[1] == NULL)
  {
    *pnTotalSize = 0;
  }
  else
  {
    *pnTotalSize = strtoull(sqlrow[1], NULL, 10);
  }

  mysql_free_result(sqlresult);

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_delete_media_files(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID)
{
  mediadb_result r;

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  sprintf(
    pDB->pszSQLQueryBuffer,
    "DELETE FROM " MEDIADB_MYSQL_FILES_TABLE " WHERE mediaid = %u",
    (unsigned int)nMediaID);

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_media_update_name(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  const char *pszName)
{
  mediadb_result r;
  size_t sizeName, sizePart;
  char *psz, *pszPart;

  /* Ensure we have large enough buffer */

  sizeName = strlen(pszName);

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  psz = pDB->pszSQLQueryBuffer;

  pszPart = "UPDATE " MEDIADB_MYSQL_MEDIA_TABLE " SET name = '";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszName,
    sizeName);
  psz += sizePart;

  sizePart = sprintf(psz, "' WHERE mediaid = %u", (unsigned int)nMediaID);
  psz += sizePart;

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_close(
  mediadb_mysql *pDB)
{
  if (pDB->pszSQLQueryBuffer != NULL)
  {
    free(pDB->pszSQLQueryBuffer);
  }

  if (pDB->pErrorMsgBuffer != NULL)
  {
    free(pDB->pErrorMsgBuffer);
  }

  mysql_close(pDB->pMYSQL);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_media_get_all(
  mediadb_mysql *pDB,
  mediadb_media_callback pCallback,
  void *pUserContext)
{
  mediadb_result r;
  MYSQL_RES * sqlresult;
  my_ulonglong nRowCount;
  MYSQL_ROW sqlrow;
  mediadb_uint nMediaID;
  const char *pszName;
  const char *pszComment;
  mediadb_uint nAdded;
  mediadb_uint nTotalFiles;
  mediadb_uint nTotalSize;
  mediadb_uint nLocationID;
  mediadb_mediatype nType;
  char *pszLocation;

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  strcpy(pDB->pszSQLQueryBuffer,
         "SELECT m.mediaid, m.added, m.info1, m.info2, "
         "m.name, m.comment, l.id, l.description, m.type FROM "
         MEDIADB_MYSQL_MEDIA_TABLE " AS m, "
         MEDIADB_MYSQL_LOCATIONS_TABLE " AS l "
         "WHERE m.location = l.id "
         "ORDER BY m.mediaid ASC");

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  sqlresult = mysql_store_result(pDB->pMYSQL);
  if (sqlresult == NULL)
  {
    set_error_message(
      pDB,
      "Failed to store result after query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nRowCount = mysql_num_rows(sqlresult);

  while (nRowCount--)
  {
    sqlrow = mysql_fetch_row(sqlresult);
    if (sqlrow == NULL)
    {
      set_error_message(
        pDB,
        "Failed to fetch row after query \"%s\"",
        pDB->pszSQLQueryBuffer);

      mysql_free_result(sqlresult);

      return MEDIADB_FAIL;
    }

    /* m.mediaid */
    if (sqlrow[0] == NULL)
    {
      nMediaID = 0;
    }
    else
    {
      nMediaID = strtoull(sqlrow[0], NULL, 10);
    }

    /* m.added */
    if (sqlrow[1] == NULL)
    {
      nAdded = 0;
    }
    else
    {
      nAdded = strtoull(sqlrow[1], NULL, 10);
    }

    /* m.info1 */
    if (sqlrow[2] == NULL)
    {
      nTotalFiles = 0;
    }
    else
    {
      nTotalFiles = strtoull(sqlrow[2], NULL, 10);
    }

    /* m.info2 */
    if (sqlrow[3] == NULL)
    {
      nTotalSize = 0;
    }
    else
    {
      nTotalSize = strtoull(sqlrow[3], NULL, 10);
    }

    /* m.name */
    if (sqlrow[4] == NULL)
    {
      pszName = "";
    }
    else
    {
      pszName = sqlrow[4];
    }

    /* m.comment */
    if (sqlrow[5] == NULL)
    {
      pszComment = "";
    }
    else
    {
      pszComment = sqlrow[5];
    }

    /* l.id */
    if (sqlrow[6] == NULL)
    {
      nLocationID = 0;
    }
    else
    {
      nLocationID = strtoull(sqlrow[6], NULL, 10);
    }

    /* l.description */
    if (sqlrow[7] == NULL)
    {
      pszLocation = "";
    }
    else
    {
      pszLocation = sqlrow[7];
    }

    /* m.type */
    if (sqlrow[8] == NULL)
    {
      nType = MEDIADB_MT_EMPTY;
    }
    else
    {
      nType = strtoull(sqlrow[8], NULL, 10);
    }

    pCallback(
      pUserContext,
      nMediaID,
      nType,
      pszName,
      pszComment,
      nAdded,
      nTotalFiles,
      nTotalSize,
      nLocationID,
      pszLocation);
  }

  mysql_free_result(sqlresult);

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_files_get(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  mediadb_files_callback pCallback,
  void *pUserContext)
{
  mediadb_result r;
  MYSQL_RES * sqlresult;
  my_ulonglong nRowCount;
  MYSQL_ROW sqlrow;
  char *pszName;
  char *pszPath1;
  mediadb_uint nTime;
  mediadb_uint nSize;
  mediadb_filetype nType;
  size_t sizePath, sizePart, s;
  char *psz, *pszPart;

  if (pszPath == NULL)
  {
    pszPath = "";
  }

  sizePath = strlen(pszPath);

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    sizePath*2 + 1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  psz = pDB->pszSQLQueryBuffer;

  sizePart = sprintf(
    psz,
    "SELECT name, size, time, path FROM "
    MEDIADB_MYSQL_FILES_TABLE
    " WHERE mediaid = %u AND path = '", (unsigned int)nMediaID);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszPath,
    sizePath);
  psz += sizePart;

  pszPart = "/'";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  *psz = 0;

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  sqlresult = mysql_store_result(pDB->pMYSQL);
  if (sqlresult == NULL)
  {
    set_error_message(
      pDB,
      "Failed to store result after query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nRowCount = mysql_num_rows(sqlresult);

  while (nRowCount--)
  {
    sqlrow = mysql_fetch_row(sqlresult);
    if (sqlrow == NULL)
    {
      set_error_message(
        pDB,
        "Failed to fetch row after query \"%s\"",
        pDB->pszSQLQueryBuffer);

      mysql_free_result(sqlresult);

      return MEDIADB_FAIL;
    }

    /* name */
    if (sqlrow[0] == NULL)
    {
      pszName = "";
    }
    else
    {
      pszName = sqlrow[0];
    }

    /* size */
    if (sqlrow[1] == NULL)
    {
      nSize = 0;
    }
    else
    {
      nSize = strtoull(sqlrow[1], NULL, 10);
    }

    /* time */
    if (sqlrow[2] == NULL)
    {
      nTime = 0;
    }
    else
    {
      nTime = strtoull(sqlrow[2], NULL, 10);
    }

    /* path */
    if (sqlrow[3] == NULL)
    {
      pszPath1 = "";
    }
    else
    {
      pszPath1 = sqlrow[3];
    }

    s = strlen(pszName);

    if (s > 1 && pszName[s-1] == '/')
    {
      nType = MEDIADB_FILETYPE_DIR;
      pszName[s-1] = 0;
    }
    else
    {
      nType = MEDIADB_FILETYPE_FILE;
    }

    pCallback(
      pUserContext,
      pszPath1,
      pszName,
      nType,
      nSize,
      nTime);
  }

  mysql_free_result(sqlresult);

  reset_error_message(pDB);

  return MEDIADB_OK;
}

const char *
mediadb_mysql_get_match_keyword(mediadb_uint nPMMID)
{
  switch (nPMMID)
  {
  case MEDIADB_MYSQL_PMM_EXACT_CASE_INSENSITIVE:
    return " = ";
  case MEDIADB_MYSQL_PMM_EXACT_CASE_SENSITIVE:
    return " = BINARY ";
  case MEDIADB_MYSQL_PMM_LIKE_CASE_INSENSITIVE:
    return " LIKE ";
  case MEDIADB_MYSQL_PMM_LIKE_CASE_SENSITIVE:
    return " LIKE BINARY ";
  case MEDIADB_MYSQL_PMM_REGEXP_CASE_INSENSITIVE:
    return " REGEXP ";
  case MEDIADB_MYSQL_PMM_REGEXP_CASE_SENSITIVE:
    return " REGEXP BINARY ";
  }

  assert(0);
  return "XXX_BUG_XXX";
}


mediadb_result
mediadb_mysql_get_pattern_match_methods(
  mediadb_mysql *pDB,
  const struct mediadb_pattern_match_method **ppPMM)
{
  *ppPMM =  mediadb_mysql_pattern_match_methods;
  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_files_search(
  mediadb_mysql *pDB,
  mediadb_uint nFilenamePMMID,
  const char *pszFilenamePattern,
  mediadb_uint nPathPMMID,
  const char *pszPathPattern,
  const mediadb_uint *pnMinSize,
  const mediadb_uint *pnMaxSize,
  mediadb_files_search_callback pCallback,
  void *pUserContext)
{
  mediadb_result r;
  int nCount;
  const char *pszAND;
  size_t sizePart;
  size_t sizeFilenamePattern;
  size_t sizePathPattern;
  char *psz;
  MYSQL_RES * sqlresult;
  my_ulonglong nRowCount;
  MYSQL_ROW sqlrow;
  mediadb_uint nMediaID;
  char *pszPath;
  char *pszName;
  char *pszMediaName;
  char *pszMediaLocation;
  mediadb_uint nSize;
  mediadb_uint nTime;
  size_t s;
  mediadb_filetype nType;

  nCount = 0;
  pszAND = " AND ";

  if (nFilenamePMMID != MEDIADB_PMM_NULL)
  {
    nCount++;
    sizeFilenamePattern = strlen(pszFilenamePattern);
  }
  else
  {
    sizeFilenamePattern = 0;
  }

  if (nPathPMMID != MEDIADB_PMM_NULL)
  {
    nCount++;
    sizePathPattern = strlen(pszPathPattern);
  }
  else
  {
    sizePathPattern = 0;
  }

  if (pnMinSize != NULL)
  {
    nCount++;
  }

  if (pnMaxSize != NULL)
  {
    nCount++;
  }

  if (nCount == 0)
  {
    /* Nothing to search - empty result */
    return MEDIADB_OK;
  }

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    sizeFilenamePattern*2 + sizePathPattern + 1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  psz = pDB->pszSQLQueryBuffer;

  sizePart = sprintf(
    psz,
    "SELECT f.mediaid, f.path, f.name, f.size, f.time, m.name, l.description FROM "
    MEDIADB_MYSQL_FILES_TABLE " AS f, "
    MEDIADB_MYSQL_MEDIA_TABLE " AS m, "
    MEDIADB_MYSQL_LOCATIONS_TABLE " AS l "
    "WHERE f.mediaid = m.mediaid AND m.location = l.id AND ");
  psz += sizePart;

  if (nFilenamePMMID != MEDIADB_PMM_NULL)
  {
    nCount--;

    sizePart = sprintf(
      psz,
      "f.name%s'",
      mediadb_mysql_get_match_keyword(nFilenamePMMID));
    psz += sizePart;

    sizePart = mysql_real_escape_string(
      pDB->pMYSQL,
      psz,
      pszFilenamePattern,
      sizeFilenamePattern);
    psz += sizePart;

    sizePart = sprintf(
      psz,
      "'%s",
      nCount == 0?"":pszAND);
    psz += sizePart;
  }

  if (nPathPMMID != MEDIADB_PMM_NULL)
  {
    nCount--;

    sizePart = sprintf(
      psz,
      "f.path%s'",
      mediadb_mysql_get_match_keyword(nPathPMMID));
    psz += sizePart;

    sizePart = mysql_real_escape_string(
      pDB->pMYSQL,
      psz,
      pszPathPattern,
      sizePathPattern);
    psz += sizePart;

    sizePart = sprintf(
      psz,
      "'%s",
      nCount == 0?"":pszAND);
    psz += sizePart;
  }

  if (pnMinSize != NULL)
  {
    nCount--;

    sizePart = sprintf(
      psz,
      "f.size >= %u%s",
      (unsigned int)*pnMinSize,
      nCount == 0?"":pszAND);
    psz += sizePart;
  }

  if (pnMaxSize != NULL)
  {
    nCount--;

    sizePart = sprintf(
      psz,
      "f.size <= %u%s",
      (unsigned int)*pnMaxSize,
      nCount == 0?"":pszAND);
    psz += sizePart;
  }

  assert(nCount == 0);

  *psz = 0;

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  sqlresult = mysql_store_result(pDB->pMYSQL);
  if (sqlresult == NULL)
  {
    set_error_message(
      pDB,
      "Failed to store result after query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nRowCount = mysql_num_rows(sqlresult);

  while (nRowCount--)
  {
    sqlrow = mysql_fetch_row(sqlresult);
    if (sqlrow == NULL)
    {
      set_error_message(
        pDB,
        "Failed to fetch row after query \"%s\"",
        pDB->pszSQLQueryBuffer);

      mysql_free_result(sqlresult);

      return MEDIADB_FAIL;
    }

    /* mediaid */
    if (sqlrow[0] == NULL)
    {
      nMediaID = 0;
    }
    else
    {
      nMediaID = strtoull(sqlrow[0], NULL, 10);
    }

    /* path */
    if (sqlrow[1] == NULL)
    {
      pszPath = "";
    }
    else
    {
      pszPath = sqlrow[1];
    }

    /* name */
    if (sqlrow[2] == NULL)
    {
      pszName = "";
    }
    else
    {
      pszName = sqlrow[2];
    }

    /* size */
    if (sqlrow[3] == NULL)
    {
      nSize = 0;
    }
    else
    {
      nSize = strtoull(sqlrow[3], NULL, 10);
    }

    /* time */
    if (sqlrow[4] == NULL)
    {
      nTime = 0;
    }
    else
    {
      nTime = strtoull(sqlrow[4], NULL, 10);
    }

    /* media name */
    if (sqlrow[5] == NULL)
    {
      pszMediaName = "";
    }
    else
    {
      pszMediaName = sqlrow[5];
    }

    /* media location */
    if (sqlrow[6] == NULL)
    {
      pszMediaLocation = "";
    }
    else
    {
      pszMediaLocation = sqlrow[6];
    }

    s = strlen(pszName);

    if (s > 1 && pszName[s-1] == '/')
    {
      nType = MEDIADB_FILETYPE_DIR;
      pszName[s-1] = 0;
    }
    else
    {
      nType = MEDIADB_FILETYPE_FILE;
    }

    pCallback(
      pUserContext,
      nMediaID,
      MEDIADB_MT_DATA,
      pszMediaName,
      pszMediaLocation,
      pszPath,
      pszName,
      nType,
      (mediadb_uint)nSize,
      (mediadb_uint)nTime);
  }

  mysql_free_result(sqlresult);

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_mysql_file_get_properties(
  mediadb_mysql *pDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype *pFiletype,
  mediadb_uint *pnSize,
  mediadb_uint *pnTime)
{
  mediadb_result r;
  MYSQL_RES * sqlresult;
  my_ulonglong nRowCount;
  MYSQL_ROW sqlrow;
  char *pszName1;
  mediadb_uint nTime;
  mediadb_uint nSize;
  mediadb_filetype nType;
  size_t sizePath, sizeName, sizePart, s;
  char *psz, *pszPart;

  if (pszPath == NULL)
  {
    pszPath = "";
  }

  sizePath = strlen(pszPath);
  sizeName = strlen(pszName);

  /* Ensure we have large enough buffer */

  r = maybe_enlarge_buffer(
    &pDB->pszSQLQueryBuffer,
    &pDB->sizeSQLQueryBuffer,
    sizePath*2 + sizeName*4 + 1024);
  if (MEDIADB_IS_ERROR(r))
  {
    set_error_message(
      pDB,
      "Cannot enlarge buffer");
    return r;
  }

  /* Compose SQL query string */

  psz = pDB->pszSQLQueryBuffer;

  sizePart = sprintf(
    psz,
    "SELECT name, size, time FROM "
    MEDIADB_MYSQL_FILES_TABLE
    " WHERE mediaid = %u AND path = '", (unsigned int)nMediaID);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszPath,
    sizePath);
  psz += sizePart;

  pszPart = "/' AND (name = '";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszName,
    sizeName);
  psz += sizePart;

  pszPart = "/' OR name = '";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  sizePart = mysql_real_escape_string(
    pDB->pMYSQL,
    psz,
    pszName,
    sizeName);
  psz += sizePart;

  pszPart = "')";
  sizePart = strlen(pszPart);
  memcpy(psz, pszPart, sizePart);
  psz += sizePart;

  *psz = 0;

  /* Execute SQL query */

#ifdef DUMMY_SQL_QUERIES
  printf("MySQL query \"%s\"\n", pDB->pszSQLQueryBuffer);
#else
  if (mysql_query(pDB->pMYSQL, pDB->pszSQLQueryBuffer) != 0)
  {
    set_error_message(
      pDB,
      "Failed to execute query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }
#endif

  sqlresult = mysql_store_result(pDB->pMYSQL);
  if (sqlresult == NULL)
  {
    set_error_message(
      pDB,
      "Failed to store result after query \"%s\" Error: %s",
      pDB->pszSQLQueryBuffer,
      mysql_error(pDB->pMYSQL));
    return MEDIADB_FAIL;
  }

  nRowCount = mysql_num_rows(sqlresult);

  if (nRowCount != 1)
  {
      set_error_message(
        pDB,
        "Found %u files instead of one",
        (unsigned int)nRowCount);

      mysql_free_result(sqlresult);

      return MEDIADB_FAIL;
  }

  sqlrow = mysql_fetch_row(sqlresult);
  if (sqlrow == NULL)
  {
    set_error_message(
      pDB,
      "Failed to fetch row after query \"%s\"",
      pDB->pszSQLQueryBuffer);

    mysql_free_result(sqlresult);

    return MEDIADB_FAIL;
  }

  /* name */
  if (sqlrow[0] == NULL)
  {
    pszName1 = "";
  }
  else
  {
    pszName1 = sqlrow[0];
  }

  /* size */
  if (sqlrow[1] == NULL)
  {
    nSize = 0;
  }
  else
  {
    nSize = strtoull(sqlrow[1], NULL, 10);
  }

  /* time */
  if (sqlrow[2] == NULL)
  {
    nTime = 0;
  }
  else
  {
    nTime = strtoull(sqlrow[2], NULL, 10);
  }

  s = strlen(pszName1);

  if (s > 1 && pszName1[s-1] == '/')
  {
    nType = MEDIADB_FILETYPE_DIR;
    pszName1[s-1] = 0;
  }
  else
  {
    nType = MEDIADB_FILETYPE_FILE;
  }

  mysql_free_result(sqlresult);

  reset_error_message(pDB);

  *pnTime = nTime;
  *pnSize = nSize;
  *pFiletype = nType;

  return MEDIADB_OK;
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: mysql.c,v $
 *   Revision 1.13  2005/03/05 21:39:44  nedko
 *   new functionality - file_get_properties()
 *
 *   Revision 1.12  2004/10/03 22:03:11  nedko
 *   Reorder patern match methods, "ERE" first, "LIKE", "exact" last
 *
 *   Revision 1.11  2004/10/03 21:57:52  nedko
 *   Implement file search for MySQL backend.
 *
 *   Revision 1.10  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *   Revision 1.9  2004/08/08 00:47:42  nedko
 *   Get more info for media from database.
 *
 *   Revision 1.8  2004/05/22 00:15:09  nedko
 *   Implement mediadb_mysql_files_get()
 *
 *   Revision 1.7  2004/05/21 23:40:46  nedko
 *   New functionality: mediadb_files_get()
 *
 *   Revision 1.6  2004/05/16 18:57:12  nedko
 *   media_get_all functionality implemented.
 *
 *   Revision 1.5  2004/05/11 01:15:19  nedko
 *   Bugfixes.
 *
 *   Revision 1.4  2004/05/03 20:47:15  nedko
 *   Update mode for cui
 *
 *   Revision 1.3  2004/05/02 20:12:11  nedko
 *   Improve error dumps.
 *
 *   Revision 1.2  2004/05/02 13:18:36  nedko
 *   Supply filetype when adding new file.
 *   Initialize mediaid when in dummy sql queries mode.
 *
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
