/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: sqlite.c,v 1.14 2005/03/05 21:56:35 nedko Exp $
 *
 * DESCRIPTION:
 *  SQLite backend implementation.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "../result.h"
#include "internal.h"
#include "memory.h"

#define MEDIADB_SQLITE_MEDIA_TABLE "media"
#define MEDIADB_SQLITE_FILES_TABLE "files"
#define MEDIADB_SQLITE_LOCATIONS_TABLE "locations"
#define MEDIADB_SQLITE_LOCATION_TYPES_TABLE "location_types"

//#define DUMMY_SQL_QUERIES
#define FORMATTING_BUFFER_SIZE 10240

#define MEDIADB_SQLITE_PMM_EXACT_CASE_INSENSITIVE    1
#define MEDIADB_SQLITE_PMM_EXACT_CASE_SENSITIVE      2
#define MEDIADB_SQLITE_PMM_LIKE_CASE_INSENSITIVE     3
#define MEDIADB_SQLITE_PMM_LIKE_CASE_SENSITIVE       4
#define MEDIADB_SQLITE_PMM_REGEXP_CASE_INSENSITIVE   5
#define MEDIADB_SQLITE_PMM_REGEXP_CASE_SENSITIVE     6

static struct mediadb_pattern_match_method 
mediadb_sqlite_pattern_match_methods[] =
{
  {
    MEDIADB_SQLITE_PMM_EXACT_CASE_INSENSITIVE,
    "exact - case insensitive",
    "Match exactly, ignore case",
    "Char by char match"
  },
/*
  {
    MEDIADB_SQLITE_PMM_EXACT_CASE_SENSITIVE,
    "exact - case sensitive",
    "Match exactly, do not ignore case",
    "Char by char match"
  },
*/
  {
    MEDIADB_SQLITE_PMM_LIKE_CASE_INSENSITIVE,
    "SQL \"LIKE\" - case insensitive",
    "Traditional SQL \"LIKE\" oprator pattern matching, ignore case",
    "SQL pattern matching allows you to use `_' to match any single character "
    "and `%' to match an arbitrary number of characters (including zero characters). "
    "LIKE pattern match succeeds only if the pattern matches the entire value."
  },
/*
  {
    MEDIADB_SQLITE_PMM_LIKE_CASE_SENSITIVE,
    "SQL \"LIKE\" - case sensitive",
    "Traditional SQL \"LIKE\" oprator pattern matching, do not ignore case",
    "SQL pattern matching allows you to use `_' to match any single character "
    "and `%' to match an arbitrary number of characters (including zero characters). "
    "LIKE pattern match succeeds only if the pattern matches the entire value."
  },
*/
  {
    MEDIADB_PMM_NULL,
    NULL,
    NULL
  }
};

static
void
set_error_message(
  mediadb_sqlite *pDB,
  const char *pszFormat,
  ...)
{
  mediadb_result r;
  size_t s;
  va_list argList;
  char Buffer[FORMATTING_BUFFER_SIZE];

  va_start(argList, pszFormat);
  vsnprintf(Buffer, FORMATTING_BUFFER_SIZE-1, pszFormat, argList);
  va_end(argList);

  s = strlen(Buffer);

  r = maybe_enlarge_buffer(
    &pDB->pErrorMsgBuffer,
    &pDB->sizeErrorMsgBuffer,
    s+1);
  if (MEDIADB_IS_ERROR(r))
  {
    if (pDB->sizeErrorMsgBuffer > 0)
    {
      pDB->pErrorMsgBuffer[0] = 0;
    }
  }

  memcpy(pDB->pErrorMsgBuffer, Buffer, s + 1);
}

static
void
reset_error_message(
  mediadb_sqlite *pDB)
{
  if (pDB->pErrorMsgBuffer != NULL)
  {
    pDB->pErrorMsgBuffer[0] = 0;
  }
}

const char *
mediadb_sqlite_get_error_message(
  mediadb_sqlite *pDB)
{
  if (pDB->pErrorMsgBuffer == NULL)
  {
    return "";
  }

  return pDB->pErrorMsgBuffer;
}

mediadb_result
mediadb_sqlite_open(
  mediadb_sqlite *pDB,
  const char *pszHost,
  const char *pszUser,
  const char *pszPass,
  const char *pszDB)
{
  sqlite * hDB;
  char *pErrorMsg;

  pDB->pErrorMsgBuffer = NULL;
  pDB->sizeErrorMsgBuffer = 0;

  hDB = sqlite_open(
    pszDB,
    0,
    &pErrorMsg);
  if (hDB == NULL)
  {
    set_error_message(
      pDB,
      "Failed to open database. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  pDB->hDB = hDB;

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_close(
  mediadb_sqlite *pDB)
{
  if (pDB->pErrorMsgBuffer != NULL)
  {
    free(pDB->pErrorMsgBuffer);
  }

  sqlite_close(pDB->hDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_media_add_new(
  mediadb_sqlite *pDB,
  const char *pszName,
  const char *pszComment,
  mediadb_mediatype nType,
  mediadb_uint *pnNewMediaID)
{
  int nRet;
  char *pErrorMsg;
  my_ulonglong nMediaID;

  nRet = sqlite_exec_printf(
    pDB->hDB,
    "INSERT INTO " MEDIADB_SQLITE_MEDIA_TABLE
    " (catid,type,added,info1,info2,name,comment,location)"
    " VALUES (0,%u,0,0,0,'%q','%q','0')",
    0,
    0,
    &pErrorMsg,
    (unsigned int)nType,
    pszName,
    pszComment);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  nMediaID = sqlite_last_insert_rowid(pDB->hDB);
  if (nMediaID == 0ULL)
  {
    set_error_message(
      pDB,
      "Invalid media ID 0 after media INSERT");
    return MEDIADB_FAIL;
  }

  *pnNewMediaID = nMediaID;

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_media_update_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize)
{
  int nRet;
  char *pErrorMsg;

  nRet = sqlite_exec_printf(
    pDB->hDB,
    "UPDATE " MEDIADB_SQLITE_MEDIA_TABLE " SET added=%u, info1=%u, info2=%u WHERE mediaid = %u",
    0,
    0,
    &pErrorMsg,
    (unsigned int)nTimeAdded,
    (unsigned int)nTotalFiles,
    (unsigned int)nTotalSize,
    (unsigned int)nMediaID);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_file_add_new(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  mediadb_filetype Filetype,
  const char *pszPath,
  const char *pszName,
  mediadb_uint nFileSize,
  mediadb_uint nFileTime)
{
  int nRet;
  char *pErrorMsg;

  if (Filetype != MEDIADB_FILETYPE_FILE &&
      Filetype != MEDIADB_FILETYPE_DIR)
  {
    set_error_message(
      pDB,
      "Cannot add file of unknown type");
    return MEDIADB_INVAL_ARG;
  }

  nRet = sqlite_exec_printf(
    pDB->hDB,
    "INSERT INTO " MEDIADB_SQLITE_FILES_TABLE " (mediaid,size,time,path,name) VALUES (%u,%u,%u,'%q','%q%s')",
    0,
    0,
    &pErrorMsg,
    (unsigned int)nMediaID,
    (unsigned int)nFileSize,
    (unsigned int)nFileTime,
    pszPath,
    pszName,
    (Filetype == MEDIADB_FILETYPE_DIR)?"/":"");
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_media_get_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTimeAdded,
  mediadb_mediatype *pnType,
  mediadb_uint *pnLocationID,
  char **ppszTitle)
{
  int nRet;
  char *pErrorMsg;
  char **pResult;
  int nRows, nColumns;

  nRet = sqlite_get_table_printf(
    pDB->hDB,
    "SELECT added, type, name, location FROM " MEDIADB_SQLITE_MEDIA_TABLE " WHERE mediaid = %u",
    &pResult,
    &nRows,
    &nColumns,
    &pErrorMsg,
    (unsigned int)nMediaID);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  if (nColumns != 4 ||
      nRows != 1)
  {
    set_error_message(
      pDB,
      "SQL query returned unexpected result (%d, %d).",
      nColumns,
      nRows);

    sqlite_free_table(pResult);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  if (pResult[4] == NULL)
  {
    *pnTimeAdded = 0;
  }
  else
  {
    *pnTimeAdded = strtoull(pResult[4], NULL, 10);
  }

  if (pResult[5] == NULL)
  {
    *pnType = MEDIADB_MT_UNKNOWN;
  }
  else
  {
    *pnType = strtoull(pResult[5], NULL, 10);
  }

  if (pResult[6] == NULL)
  {
    *ppszTitle = strdup("");
  }
  else
  {
    *ppszTitle = strdup(pResult[6]);
  }

  if (pResult[7] == NULL)
  {
    *pnLocationID = MEDIADB_MT_UNKNOWN;
  }
  else
  {
    *pnLocationID = strtoull(pResult[7], NULL, 10);
  }

  sqlite_free_table(pResult);

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_location_get_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nLocationID,
  mediadb_uint *pnLocationTypeID,
  char **ppszDescription)
{
  int nRet;
  char *pErrorMsg;
  char **pResult;
  int nRows, nColumns;

  nRet = sqlite_get_table_printf(
    pDB->hDB,
    "SELECT type, description FROM " MEDIADB_SQLITE_LOCATIONS_TABLE " WHERE id = %u",
    &pResult,
    &nRows,
    &nColumns,
    &pErrorMsg,
    (unsigned int)nLocationID);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  if (nColumns != 2 ||
      nRows != 1)
  {
    set_error_message(
      pDB,
      "SQL query returned unexpected result (%d, %d).",
      nColumns,
      nRows);

    sqlite_free_table(pResult);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  if (pResult[2] == NULL)
  {
    *pnLocationTypeID = 0;
  }
  else
  {
    *pnLocationTypeID = strtoull(pResult[2], NULL, 10);
  }

  if (pResult[3] == NULL)
  {
    *ppszDescription = strdup("");
  }
  else
  {
    *ppszDescription = strdup(pResult[3]);
  }

  sqlite_free_table(pResult);

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_location_type_get_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nLocationTypeID,
  char **ppszDescription)
{
  int nRet;
  char *pErrorMsg;
  char **pResult;
  int nRows, nColumns;

  nRet = sqlite_get_table_printf(
    pDB->hDB,
    "SELECT description FROM " MEDIADB_SQLITE_LOCATION_TYPES_TABLE " WHERE id = %u",
    &pResult,
    &nRows,
    &nColumns,
    &pErrorMsg,
    (unsigned int)nLocationTypeID);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  if (nColumns != 1 ||
      nRows != 1)
  {
    set_error_message(
      pDB,
      "SQL query returned unexpected result (%d, %d).",
      nColumns,
      nRows);

    sqlite_free_table(pResult);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  if (pResult[1] == NULL)
  {
    *ppszDescription = strdup("");
  }
  else
  {
    *ppszDescription = strdup(pResult[1]);
  }

  sqlite_free_table(pResult);

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_media_get_properties_data(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTotalFiles,
  mediadb_uint *pnTotalSize)
{
  int nRet;
  char *pErrorMsg;
  char **pResult;
  int nRows, nColumns;

  nRet = sqlite_get_table_printf(
    pDB->hDB,
    "SELECT info1, info2 FROM " MEDIADB_SQLITE_MEDIA_TABLE " WHERE mediaid = %u",
    &pResult,
    &nRows,
    &nColumns,
    &pErrorMsg,
    (unsigned int)nMediaID);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  if (nColumns != 2 ||
      nRows != 1)
  {
    set_error_message(
      pDB,
      "SQL query returned unexpected result (%d, %d).",
      nColumns,
      nRows);

    sqlite_free_table(pResult);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  if (pResult[2] == NULL)
  {
    *pnTotalFiles = 0;
  }
  else
  {
    *pnTotalFiles = strtoull(pResult[2], NULL, 10);
  }

  if (pResult[3] == NULL)
  {
    *pnTotalSize = 0;
  }
  else
  {
    *pnTotalSize = strtoull(pResult[3], NULL, 10);
  }

  sqlite_free_table(pResult);

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_delete_media_files(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID)
{
  int nRet;
  char *pErrorMsg;

  nRet = sqlite_exec_printf(
    pDB->hDB,
    "DELETE FROM " MEDIADB_SQLITE_FILES_TABLE " WHERE mediaid = %u",
    0,
    0,
    &pErrorMsg,
    (unsigned int)nMediaID);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_media_update_name(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  const char *pszName)
{
  int nRet;
  char *pErrorMsg;

  nRet = sqlite_exec_printf(
    pDB->hDB,
    "UPDATE " MEDIADB_SQLITE_MEDIA_TABLE " SET name = '%q' WHERE mediaid = %u",
    0,
    0,
    &pErrorMsg,
    pszName,
    (unsigned int)nMediaID);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

struct mediadb_media_callback_context
{
  mediadb_media_callback pCallback;
  void *pUserContext;
  sqlite * hDB;
};

int
mediadb_sqlite_media_get_callback(
  void *pArg,
  int argc,
  char **argv,
  char **columnNames)
{
  long nMediaID;
  const char *pszName;
  const char *pszComment;
  long nAdded;
  long nTotalFiles;
  long nTotalSize;
  long nLocationID;
  mediadb_mediatype nType;
  char strLocation[1000];
  char *pszLocation;
  char *pLocationDescriptionBuffer;
  size_t s;
  int nRet;
  char *pErrorMsg;
  char **pResult;
  int nRows, nColumns;

  pLocationDescriptionBuffer = NULL;

  if (argv == NULL)
    return 0;

  if (argc != 9)
    return SQLITE_ABORT;

  /* mediaid */
  if (argv[0] == NULL)
    return SQLITE_ABORT;

  nMediaID = atol(argv[0]);
  if (nMediaID <= 0)
    return SQLITE_ABORT;

  /* catid */
  /* not used */

  /* type */
  if (argv[2] == NULL)
    return SQLITE_ABORT;
  nType = atol(argv[2]);
  if (nAdded <= 0)
  {
    nType = MEDIADB_MT_EMPTY;
  }

  /* added */
  if (argv[3] == NULL)
    return SQLITE_ABORT;

  nAdded = atol(argv[3]);
  if (nAdded <= 0)
    return SQLITE_ABORT;

  /* info1 */
  if (argv[4] == NULL)
    return SQLITE_ABORT;
  nTotalFiles = atol(argv[4]);
  if (nTotalFiles < 0)
    return SQLITE_ABORT;

  /* info2 */
  if (argv[5] == NULL)
    return SQLITE_ABORT;
  nTotalSize = atol(argv[5]);
  if (nTotalSize < 0)
    return SQLITE_ABORT;

  /* name */
  if (argv[6] == NULL)
    return SQLITE_ABORT;

  pszName = argv[6];

  /* comment */
  if (argv[7] == NULL)
    return SQLITE_ABORT;

  pszComment = argv[7];

  /* location */
  if (argv[8] == NULL)
    return SQLITE_ABORT;
  nLocationID = atol(argv[8]);
  if (nLocationID < 0)
    return SQLITE_ABORT;

  nRet = sqlite_get_table_printf(
    ((struct mediadb_media_callback_context *) pArg)->hDB,
    "SELECT description FROM " MEDIADB_SQLITE_LOCATIONS_TABLE " WHERE id = %u",
    &pResult,
    &nRows,
    &nColumns,
    &pErrorMsg,
    (unsigned int)nLocationID);
  if (nRet != SQLITE_OK)
  {
    sprintf(strLocation,
            "Cannot get description for location with ID \"%ld\" (SQLite error \"%s\")",
            nLocationID,
            pErrorMsg);

    free(pErrorMsg);

    pszLocation = strLocation;
  }
  else if (nColumns != 1 ||
           nRows != 1)
  {
    sprintf(strLocation,
            "Cannot get description for location with ID \"%ld\" (SQLite query returned unexpected result (%d, %d).",
            nLocationID,
            nColumns,
            nRows);
    sqlite_free_table(pResult);
    pszLocation = strLocation;
  }
  else if (pResult[1] == NULL)
  {
    sprintf(strLocation,
            "Cannot get description for location with ID \"%ld\" (SQLite query returned NULL)",
            nLocationID);
    sqlite_free_table(pResult);
    pszLocation = strLocation;
  }
  else
  {
    s = strlen(pResult[1]);

    if (s < sizeof(strLocation))
    {
      memcpy(strLocation, pResult[1], s);
      strLocation[s] = 0;
      pszLocation = strLocation;
    }
    else
    {
      pLocationDescriptionBuffer = malloc(s+1);
      if (pLocationDescriptionBuffer == NULL)
      {
        sprintf(strLocation,
                "Cannot get description for location with ID \"%ld\" (out of memory)",
                nLocationID);
        pszLocation = strLocation;
      }
      else
      {
        memcpy(pLocationDescriptionBuffer, pResult[1], s);
        pLocationDescriptionBuffer[s] = 0;
        pszLocation = pLocationDescriptionBuffer;
      }
    }

    sqlite_free_table(pResult);
  }

  ((struct mediadb_media_callback_context *) pArg)->pCallback(
    ((struct mediadb_media_callback_context *) pArg)->pUserContext,
    (mediadb_uint)nMediaID,
    nType,
    pszName,
    pszComment,
    (mediadb_uint)nAdded,
    (mediadb_uint)nTotalFiles,
    (mediadb_uint)nTotalSize,
    (mediadb_uint)nLocationID,
    pszLocation);

  if (pLocationDescriptionBuffer != NULL)
  {
    free(pLocationDescriptionBuffer);
  }

  return 0;
}

mediadb_result
mediadb_sqlite_media_get_all(
  mediadb_sqlite *pDB,
  mediadb_media_callback pCallback,
  void *pUserContext)
{
  struct mediadb_media_callback_context context;
  int nRet;
  char *pErrorMsg;

  context.pCallback = pCallback;
  context.pUserContext = pUserContext;
  context.hDB = pDB->hDB;

  nRet = sqlite_exec(
    pDB->hDB,
    "SELECT * FROM " MEDIADB_SQLITE_MEDIA_TABLE " ORDER BY mediaid ASC",
    mediadb_sqlite_media_get_callback,
    &context,
    &pErrorMsg);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

struct mediadb_files_callback_context
{
  mediadb_files_callback pCallback;
  void *pUserContext;
};

int
mediadb_sqlite_files_get_callback(
  void *pArg,
  int argc,
  char **argv,
  char **columnNames)
{
  char *pszName;
  char *pszPath;
  long nSize;
  long nTime;
  mediadb_filetype Filetype = MEDIADB_FILETYPE_FILE;
  size_t s;

  if (argv == NULL)
    return 0;

  if (argc != 4)
    return SQLITE_ABORT;

  /* path */
  if (argv[0] == NULL)
    return SQLITE_ABORT;

  pszPath = argv[0];

  /* name */
  if (argv[1] == NULL)
    return SQLITE_ABORT;

  pszName = argv[1];

  /* size */
  if (argv[2] == NULL)
    return SQLITE_ABORT;

  nSize = atol(argv[2]);

  /* time */
  if (argv[3] == NULL)
    return SQLITE_ABORT;

  nTime = atol(argv[3]);

  s = strlen(pszName);

  if (s > 1 && pszName[s-1] == '/')
  {
    Filetype = MEDIADB_FILETYPE_DIR;
    pszName[s-1] = 0;
  }

  ((struct mediadb_files_callback_context *) pArg)->pCallback(
    ((struct mediadb_files_callback_context *) pArg)->pUserContext,
    pszPath,
    pszName,
    Filetype,
    (mediadb_uint)nSize,
    (mediadb_uint)nTime);

  return 0;
}

mediadb_result
mediadb_sqlite_files_get(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  mediadb_files_callback pCallback,
  void *pUserContext)
{
  struct mediadb_files_callback_context context;
  int nRet;
  char *pErrorMsg;

  context.pCallback = pCallback;
  context.pUserContext = pUserContext;

  nRet = sqlite_exec_printf(
    pDB->hDB,
    "SELECT path, name, size, time FROM " MEDIADB_SQLITE_FILES_TABLE " WHERE mediaid = %u AND path = '%q/'",
    mediadb_sqlite_files_get_callback,
    &context,
    &pErrorMsg,
    (unsigned int)nMediaID,
    pszPath?pszPath:"");
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  reset_error_message(pDB);

  return MEDIADB_OK;
}

mediadb_result
mediadb_sqlite_get_pattern_match_methods(
  mediadb_sqlite *pDB,
  const struct mediadb_pattern_match_method **ppPMM)
{
  *ppPMM =  mediadb_sqlite_pattern_match_methods;
  return MEDIADB_OK;
}

struct mediadb_files_search_callback_context
{
  mediadb_files_search_callback pCallback;
  void *pUserContext;
  sqlite * hDB;
};

int
mediadb_sqlite_files_search_callback(
  void *pArg,
  int argc,
  char **argv,
  char **columnNames)
{
  char *pszName;
  char *pszPath;
  long nSize;
  long nTime;
  long nMediaID;
  long nLocationID;
  mediadb_filetype Filetype = MEDIADB_FILETYPE_FILE;
  size_t s;
  char strMediaName[1000];
  char *pszMediaName;
  char *pMediaNameBuffer;
  char strLocation[1000];
  char *pszLocation;
  char *pLocationDescriptionBuffer;
  int nRet;
  char *pErrorMsg;
  char **pResult;
  int nRows, nColumns;

  pMediaNameBuffer = NULL;
  pLocationDescriptionBuffer = NULL;

  if (argv == NULL)
    return 0;

  if (argc != 5)
    return SQLITE_ABORT;

  /* mediaid */
  if (argv[0] == NULL)
    return SQLITE_ABORT;

  nMediaID = atol(argv[0]);

  /* path */
  if (argv[1] == NULL)
    return SQLITE_ABORT;

  pszPath = argv[1];

  /* name */
  if (argv[2] == NULL)
    return SQLITE_ABORT;

  pszName = argv[2];

  /* size */
  if (argv[3] == NULL)
    return SQLITE_ABORT;

  nSize = atol(argv[3]);

  /* time */
  if (argv[4] == NULL)
    return SQLITE_ABORT;

  nTime = atol(argv[4]);

  s = strlen(pszName);

  if (s > 1 && pszName[s-1] == '/')
  {
    Filetype = MEDIADB_FILETYPE_DIR;
    pszName[s-1] = 0;
  }

  nLocationID = -1;

  nRet = sqlite_get_table_printf(
    ((struct mediadb_files_search_callback_context *) pArg)->hDB,
    "SELECT name, location FROM " MEDIADB_SQLITE_MEDIA_TABLE " WHERE mediaid = %u",
    &pResult,
    &nRows,
    &nColumns,
    &pErrorMsg,
    (unsigned int)nMediaID);
  if (nRet != SQLITE_OK)
  {
    sprintf(strMediaName,
            "Cannot get name of media with ID \"%ld\" (SQLite error \"%s\")",
            nMediaID,
            pErrorMsg);

    free(pErrorMsg);

    pszMediaName = strMediaName;
  }
  else if (nColumns != 2 ||
           nRows != 1)
  {
    sprintf(strMediaName,
            "Cannot get name of media ID \"%ld\" (SQLite query returned unexpected result (%d, %d).",
            nMediaID,
            nColumns,
            nRows);
    sqlite_free_table(pResult);
    pszMediaName = strMediaName;
  }
  else
  {
    if (pResult[2] == NULL)
    {
      sprintf(strMediaName,
              "Cannot get name of media ID \"%ld\" (SQLite query returned NULL)",
              nMediaID);
      sqlite_free_table(pResult);
      pszMediaName = strMediaName;
    }
    else
    {
      s = strlen(pResult[2]);

      if (s < sizeof(strMediaName))
      {
        memcpy(strMediaName, pResult[2], s);
        strMediaName[s] = 0;
        pszMediaName = strMediaName;
      }
      else
      {
        pMediaNameBuffer = malloc(s+1);
        if (pMediaNameBuffer == NULL)
        {
          sprintf(strMediaName,
                  "Cannot get description for location with ID \"%ld\" (out of memory)",
                  nMediaID);
          pszMediaName = strMediaName;
        }
        else
        {
          memcpy(pMediaNameBuffer, pResult[2], s);
          pMediaNameBuffer[s] = 0;
          pszMediaName = pMediaNameBuffer;
        }
      }
    }

    if (pResult[3] != NULL)
    {
      nLocationID = atol(pResult[3]);
    }

    sqlite_free_table(pResult);
  }

  if (nLocationID >= 0)
  {
    nRet = sqlite_get_table_printf(
      ((struct mediadb_media_callback_context *) pArg)->hDB,
      "SELECT description FROM " MEDIADB_SQLITE_LOCATIONS_TABLE " WHERE id = %u",
      &pResult,
      &nRows,
      &nColumns,
      &pErrorMsg,
      (unsigned int)nLocationID);
    if (nRet != SQLITE_OK)
    {
      sprintf(strLocation,
              "Cannot get description for location with ID \"%ld\" (SQLite error \"%s\")",
              nLocationID,
              pErrorMsg);

      free(pErrorMsg);

      pszLocation = strLocation;
    }
    else if (nColumns != 1 ||
             nRows != 1)
    {
      sprintf(strLocation,
              "Cannot get description for location with ID \"%ld\" (SQLite query returned unexpected result (%d, %d).",
              nLocationID,
              nColumns,
              nRows);
      sqlite_free_table(pResult);
      pszLocation = strLocation;
    }
    else if (pResult[1] == NULL)
    {
      sprintf(strLocation,
              "Cannot get description for location with ID \"%ld\" (SQLite query returned NULL)",
              nLocationID);
      sqlite_free_table(pResult);
      pszLocation = strLocation;
    }
    else
    {
      s = strlen(pResult[1]);

      if (s < sizeof(strLocation))
      {
        memcpy(strLocation, pResult[1], s);
        strLocation[s] = 0;
        pszLocation = strLocation;
      }
      else
      {
        pLocationDescriptionBuffer = malloc(s+1);
        if (pLocationDescriptionBuffer == NULL)
        {
          sprintf(strLocation,
                  "Cannot get description for location with ID \"%ld\" (out of memory)",
                  nLocationID);
          pszLocation = strLocation;
        }
        else
        {
          memcpy(pLocationDescriptionBuffer, pResult[1], s);
          pLocationDescriptionBuffer[s] = 0;
          pszLocation = pLocationDescriptionBuffer;
        }
      }

      sqlite_free_table(pResult);
    }
  }
  else
  {
    pszLocation = "Cannot retrieve location from database";
  }

  ((struct mediadb_files_search_callback_context *) pArg)->pCallback(
    ((struct mediadb_files_search_callback_context *) pArg)->pUserContext,
    nMediaID,
    MEDIADB_MT_DATA,
    pszMediaName,
    pszLocation,
    pszPath,
    pszName,
    Filetype,
    (mediadb_uint)nSize,
    (mediadb_uint)nTime);

  if (pMediaNameBuffer != NULL)
  {
    free(pMediaNameBuffer);
  }

  if (pLocationDescriptionBuffer != NULL)
  {
    free(pLocationDescriptionBuffer);
  }

  return 0;
}

const char *
mediadb_sqlite_get_match_keyword(mediadb_uint nPMMID)
{
  switch (nPMMID)
  {
  case MEDIADB_SQLITE_PMM_EXACT_CASE_INSENSITIVE:
    return " == ";
  case MEDIADB_SQLITE_PMM_LIKE_CASE_INSENSITIVE:
    return " LIKE ";
  }

  assert(0);
  return "XXX_BUG_XXX";
}

mediadb_result
mediadb_sqlite_files_search(
  mediadb_sqlite *pDB,
  mediadb_uint nFilenamePMMID,
  const char *pszFilenamePattern,
  mediadb_uint nPathPMMID,
  const char *pszPathPattern,
  const mediadb_uint *pnMinSize,
  const mediadb_uint *pnMaxSize,
  mediadb_files_search_callback pCallback,
  void *pUserContext)
{
  struct mediadb_files_search_callback_context context;
  int nRet;
  char *pErrorMsg;
  mediadb_result r;
  char *pszWhere;
  size_t sizeWhereAllocated;
  size_t sizeWhereUsed;
  size_t sizeSearchCriteria;
  size_t sizeAND;
  int nCount;
  const char *pszAND;
  char *pszTemp;

  context.pCallback = pCallback;
  context.pUserContext = pUserContext;
  context.hDB = pDB->hDB;

  /* Compose SQL WHERE expression */

  pszWhere = NULL;
  sizeWhereAllocated = 0;
  sizeWhereUsed = 0;
  sizeSearchCriteria = 0;
  nCount = 0;
  pszAND = " AND ";
  sizeAND = strlen(pszAND);

  if (nFilenamePMMID != MEDIADB_PMM_NULL)
  {
    nCount++;
  }

  if (nPathPMMID != MEDIADB_PMM_NULL)
  {
    nCount++;
  }

  if (pnMinSize != NULL)
  {
    nCount++;
  }

  if (pnMaxSize != NULL)
  {
    nCount++;
  }

  if (nFilenamePMMID != MEDIADB_PMM_NULL)
  {
    nCount--;

    pszTemp = sqlite_mprintf(
      "name%s'%q'",
      mediadb_sqlite_get_match_keyword(nFilenamePMMID),
      pszFilenamePattern);
    if (pszTemp == NULL)
    {
      r = MEDIADB_MEM;
      goto FailFreeWhere;
    }

    sizeSearchCriteria = strlen(pszTemp);
    r = maybe_enlarge_buffer(
      &pszWhere,
      &sizeWhereAllocated,
      sizeWhereUsed + sizeSearchCriteria + (nCount == 0?0:sizeAND));
    if (MEDIADB_IS_ERROR(r))
    {
      goto FailFreeTemp;
    }

    sizeWhereUsed += sprintf(
      pszWhere + sizeWhereUsed,
      "%s%s",
      pszTemp,
      nCount == 0?"":pszAND);

    sqlite_freemem(pszTemp);
  }

  if (nPathPMMID != MEDIADB_PMM_NULL)
  {
    nCount--;

    pszTemp = sqlite_mprintf(
      "path%s'%q'",
      mediadb_sqlite_get_match_keyword(nPathPMMID),
      pszPathPattern);
    if (pszTemp == NULL)
    {
      r = MEDIADB_MEM;
      goto FailFreeWhere;
    }

    sizeSearchCriteria = strlen(pszTemp);
    r = maybe_enlarge_buffer(
      &pszWhere,
      &sizeWhereAllocated,
      sizeWhereUsed + sizeSearchCriteria + (nCount == 0?0:sizeAND));
    if (MEDIADB_IS_ERROR(r))
    {
      goto FailFreeTemp;
    }

    sizeWhereUsed += sprintf(
      pszWhere + sizeWhereUsed,
      "%s%s",
      pszTemp,
      nCount == 0?"":pszAND);

    sqlite_freemem(pszTemp);
  }

  if (pnMinSize != NULL)
  {
    nCount--;

    pszTemp = sqlite_mprintf(
      "size >= %u",
      (unsigned int)*pnMinSize);
    if (pszTemp == NULL)
    {
      r = MEDIADB_MEM;
      goto FailFreeWhere;
    }

    sizeSearchCriteria = strlen(pszTemp);
    r = maybe_enlarge_buffer(
      &pszWhere,
      &sizeWhereAllocated,
      sizeWhereUsed + sizeSearchCriteria + (nCount == 0?0:sizeAND));
    if (MEDIADB_IS_ERROR(r))
    {
      goto FailFreeTemp;
    }

    sizeWhereUsed += sprintf(
      pszWhere + sizeWhereUsed,
      "%s%s",
      pszTemp,
      nCount == 0?"":pszAND);

    sqlite_freemem(pszTemp);
  }

  if (pnMaxSize != NULL)
  {
    nCount--;

    pszTemp = sqlite_mprintf(
      "size <= %u",
      (unsigned int)*pnMaxSize);
    if (pszTemp == NULL)
    {
      r = MEDIADB_MEM;
      goto FailFreeWhere;
    }

    sizeSearchCriteria = strlen(pszTemp);
    r = maybe_enlarge_buffer(
      &pszWhere,
      &sizeWhereAllocated,
      sizeWhereUsed + sizeSearchCriteria + (nCount == 0?0:sizeAND));
    if (MEDIADB_IS_ERROR(r))
    {
      goto FailFreeTemp;
    }

    sizeWhereUsed += sprintf(
      pszWhere + sizeWhereUsed,
      "%s%s",
      pszTemp,
      nCount == 0?"":pszAND);

    sqlite_freemem(pszTemp);
  }

  if (pszWhere == NULL)
  {
    /* Nothing to search - empty result */
    return MEDIADB_OK;
  }

  assert(nCount == 0);

  /* Execute SQL query */
  nRet = sqlite_exec_printf(
    pDB->hDB,
    "SELECT mediaid, path, name, size, time FROM " MEDIADB_SQLITE_FILES_TABLE " WHERE %s",
    mediadb_sqlite_files_search_callback,
    &context,
    &pErrorMsg,
    pszWhere);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);
    free(pszWhere);

    return MEDIADB_FAIL;
  }

  free(pszWhere);

  reset_error_message(pDB);

  return MEDIADB_OK;

FailFreeTemp:
  sqlite_freemem(pszTemp);

FailFreeWhere:
  if (pszWhere != NULL)
  {
    free(pszWhere);
  }

  return r;
}

struct mediadb_file_get_properties_callback_context
{
  mediadb_filetype Filetype;
  mediadb_uint nSize;
  mediadb_uint nTime;
};

int
mediadb_sqlite_file_get_properties_callback(
  void *pArg,
  int argc,
  char **argv,
  char **columnNames)
{
  char *pszName;
  long nSize;
  long nTime;
  mediadb_filetype Filetype = MEDIADB_FILETYPE_FILE;
  size_t s;

  if (argv == NULL)
    return 0;

  if (argc != 3)
    return SQLITE_ABORT;

  /* name */
  if (argv[0] == NULL)
    return SQLITE_ABORT;

  pszName = argv[0];

  /* size */
  if (argv[1] == NULL)
    return SQLITE_ABORT;

  nSize = atol(argv[1]);

  /* time */
  if (argv[2] == NULL)
    return SQLITE_ABORT;

  nTime = atol(argv[2]);

  s = strlen(pszName);

  if (s > 1 && pszName[s-1] == '/')
  {
    Filetype = MEDIADB_FILETYPE_DIR;
    pszName[s-1] = 0;
  }

  ((struct mediadb_file_get_properties_callback_context *) pArg)->Filetype = Filetype;
  ((struct mediadb_file_get_properties_callback_context *) pArg)->nSize = (mediadb_uint)nSize;
  ((struct mediadb_file_get_properties_callback_context *) pArg)->nTime = (mediadb_uint)nTime;

  return 0;
}

mediadb_result
mediadb_sqlite_file_get_properties(
  mediadb_sqlite *pDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype *pFiletype,
  mediadb_uint *pnSize,
  mediadb_uint *pnTime)
{
  struct mediadb_file_get_properties_callback_context context;
  int nRet;
  char *pErrorMsg;

  context.Filetype = 0;
  context.nSize = 0;
  context.nTime = 0;

  nRet = sqlite_exec_printf(
    pDB->hDB,
    "SELECT name, size, time FROM " MEDIADB_SQLITE_FILES_TABLE " WHERE mediaid = %u AND path = '%q/'"
    " AND (name = '%q' OR name = '%q/')",
    mediadb_sqlite_file_get_properties_callback,
    &context,
    &pErrorMsg,
    (unsigned int)nMediaID,
    pszPath?pszPath:"",
    pszName,
    pszName);
  if (nRet != SQLITE_OK)
  {
    set_error_message(
      pDB,
      "Failed to execute SQL query. Error: %s",
      pErrorMsg);

    free(pErrorMsg);

    return MEDIADB_FAIL;
  }

  *pFiletype = context.Filetype;
  *pnSize = context.nSize;
  *pnTime = context.nTime;

  reset_error_message(pDB);

  return MEDIADB_OK;
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: sqlite.c,v $
 *   Revision 1.14  2005/03/05 21:56:35  nedko
 *   Implement mediadb_sqlite_file_get_properties()
 *
 *   Revision 1.13  2005/03/05 21:39:44  nedko
 *   new functionality - file_get_properties()
 *
 *   Revision 1.12  2004/10/03 21:56:58  nedko
 *   Fix typo.
 *
 *   Revision 1.11  2004/09/18 21:32:34  nedko
 *   Use min and max size when searching files.
 *   Return media name, media type and media location when searching files.
 *
 *   Revision 1.10  2004/09/01 05:02:17  nedko
 *   Implement search by filename and path
 *
 *   Revision 1.9  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *   Revision 1.8  2004/08/08 00:47:42  nedko
 *   Get more info for media from database.
 *
 *   Revision 1.7  2004/05/21 23:40:46  nedko
 *   New functionality: mediadb_files_get()
 *
 *   Revision 1.6  2004/05/16 18:57:12  nedko
 *   media_get_all functionality implemented.
 *
 *   Revision 1.5  2004/05/11 01:18:53  nedko
 *   Implement SQLite backend.
 *
 *   Revision 1.4  2004/05/03 20:47:15  nedko
 *   Update mode for cui
 *
 *   Revision 1.3  2004/05/02 20:12:11  nedko
 *   Improve error dumps.
 *
 *   Revision 1.2  2004/05/02 13:16:58  nedko
 *   Supply filetype when adding new file
 *
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
