/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: libdb.h,v 1.10 2005/03/05 21:39:44 nedko Exp $
 *
 * DESCRIPTION:
 *  Unified access to database.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef LIBDB_H__3CA527C8_33FF_45D6_9D36_228F0E920EEA__INCLUDED
#define LIBDB_H__3CA527C8_33FF_45D6_9D36_228F0E920EEA__INCLUDED

/* mediadb type is *handle*, use it as *handle* */
typedef struct { int unused; } * mediadb;
#define MEDIADB_INVALID_VALUE NULL

typedef unsigned long long mediadb_uint;

#define MEDIADB_MT_UNKNOWN    0
#define MEDIADB_MT_AUDIO      1
#define MEDIADB_MT_DATA       2
#define MEDIADB_MT_EMPTY      3

/* Contains on of MEDIADB_MT_XXX values */
typedef unsigned int mediadb_mediatype;

#define MEDIADB_DBTYPE_UNKNOWN  0
#define MEDIADB_DBTYPE_MYSQL    1
#define MEDIADB_DBTYPE_SQLITE   2

typedef unsigned int mediadb_filetype;

#define MEDIADB_FILETYPE_FILE     1
#define MEDIADB_FILETYPE_DIR      2

typedef unsigned int mediadb_bool;

#define MEDIADB_FALSE   0
#define MEDIADB_TRUE    1

struct mediadb_pattern_match_method
{
  mediadb_uint nID;
  const char *pszName;
  const char *pszDescription;
  const char *pszUsage;
};

#define MEDIADB_PMM_NULL      0

/* Open handle to media database */
mediadb_result
mediadb_open(
  unsigned int nDBType,         /* MEDIADB_DBTYPE_XXX */
  const char *pszHost,
  const char *pszUser,
  const char *pszPass,
  const char *pszDB,
  mediadb *phDB);

/* Add new media to database */
mediadb_result
mediadb_media_add_new(
  mediadb hDB,
  const char *pszName,
  const char *pszComment,
  mediadb_mediatype nType,
  mediadb_uint *pnNewMediaID);

/* Update media properties */
mediadb_result
mediadb_media_update_properties(
  mediadb hDB,
  mediadb_uint nMediaID,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize);

/* Add new file to database */
mediadb_result
mediadb_file_add_new(
  mediadb hDB,
  mediadb_uint nMediaID,
  mediadb_filetype Filetype,
  const char *pszPath,
  const char *pszName,
  mediadb_uint nFileSize,
  mediadb_uint nFileTime);

mediadb_result
mediadb_media_get_properties(
  mediadb hDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTimeAdded,
  mediadb_mediatype *pnType,
  mediadb_uint *pnLocationID,
  char **ppszTitle);

mediadb_result
mediadb_location_get_properties(
  mediadb hDB,
  mediadb_uint nLocationID,
  mediadb_uint *pnLocationTypeID,
  char **ppszDescription);

mediadb_result
mediadb_location_type_get_properties(
  mediadb hDB,
  mediadb_uint nLocationTypeID,
  char **ppszDescription);

mediadb_result
mediadb_media_get_properties_data(
  mediadb hDB,
  mediadb_uint nMediaID,
  mediadb_uint *pnTotalFiles,
  mediadb_uint *pnTotalSize);

/*
mediadb_result
mediadb_media_get_properties_audio(
  mediadb hDB,
  mediadb_uint nMediaID);
*/

mediadb_result
mediadb_delete_media_files(
  mediadb hDB,
  mediadb_uint nMediaID);

mediadb_result
mediadb_media_update_name(
  mediadb hDB,
  mediadb_uint nMediaID,
  const char *pszName);

typedef void (* mediadb_media_callback)(
  void *pUserContext,
  mediadb_uint nMediaID,
  mediadb_mediatype nType,
  const char *pszName,
  const char *pszComment,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize,
  mediadb_uint nLocationID,
  const char *pszLocation
  );

mediadb_result
mediadb_media_get_all(
  mediadb hDB,
  mediadb_media_callback pCallback,
  void *pUserContext);

typedef void (* mediadb_files_callback)(
  void *pUserContext,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype Filetype,
  mediadb_uint nSize,
  mediadb_uint nTime);

mediadb_result
mediadb_files_get(
  mediadb hDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  mediadb_files_callback pCallback,
  void *pUserContext);

mediadb_result
mediadb_file_get_properties(
  mediadb hDB,
  mediadb_uint nMediaID,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype *pFiletype,
  mediadb_uint *pnSize,
  mediadb_uint *pnTime);

/* Close previously opened handle to database */
mediadb_result
mediadb_close(
  mediadb hDB);

const char *
mediadb_get_error_message(
  mediadb hDB);

/* *ppPSM is valid until hDB is valid */
mediadb_result
mediadb_get_pattern_match_methods(
  mediadb hDB,
  const struct mediadb_pattern_match_method **ppPMM);

typedef void (* mediadb_files_search_callback)(
  void *pUserContext,
  mediadb_uint nMediaID,
  mediadb_mediatype nMediaType,
  const char *pszMediaName,
  const char *pszMediaLocation,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype Filetype,
  mediadb_uint nSize,
  mediadb_uint nTime);

mediadb_result
mediadb_files_search(
  mediadb hDB,
  mediadb_uint nFilenamePMMID,
  const char *pszFilenamePattern,
  mediadb_uint nPathPMMID,
  const char *pszPathPattern,
  const mediadb_uint *pnMinSize,
  const mediadb_uint *pnMaxSize,
  mediadb_files_search_callback pCallback,
  void *pUserContext);

#endif /* #ifndef LIBDB_H__3CA527C8_33FF_45D6_9D36_228F0E920EEA__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: libdb.h,v $
 *   Revision 1.10  2005/03/05 21:39:44  nedko
 *   new functionality - file_get_properties()
 *
 *   Revision 1.9  2004/09/18 21:34:35  nedko
 *   Return media name, media type and media location when searching files.
 *
 *   Revision 1.8  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *   Revision 1.7  2004/08/08 00:47:42  nedko
 *   Get more info for media from database.
 *
 *   Revision 1.6  2004/05/21 23:40:46  nedko
 *   New functionality: mediadb_files_get()
 *
 *   Revision 1.5  2004/05/16 18:57:12  nedko
 *   media_get_all functionality implemented.
 *
 *   Revision 1.4  2004/05/03 20:47:15  nedko
 *   Update mode for cui
 *
 *   Revision 1.3  2004/05/02 20:12:11  nedko
 *   Improve error dumps.
 *
 *   Revision 1.2  2004/05/02 13:16:58  nedko
 *   Supply filetype when adding new file
 *
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
