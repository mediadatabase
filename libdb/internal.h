/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: internal.h,v 1.1 2004/04/27 09:12:28 nedko Exp $
 *
 * DESCRIPTION:
 *  Internal data structures.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *  
 * NOTES:
 *  There is no need to include this header directly or indirectly
 *  into source that uses libdb. This is the reason it is called *internal*
 *
 *****************************************************************************/

#ifndef INTERNAL_H__E8BC6434_EC2B_4DAB_AA47_D4C2FD072866__INCLUDED
#define INTERNAL_H__E8BC6434_EC2B_4DAB_AA47_D4C2FD072866__INCLUDED

#include "libdb.h"
#include "mysql.h"
#include "sqlite.h"

typedef struct {
  unsigned int nDBType;
  union
  {
    mediadb_mysql mysql;
    mediadb_sqlite sqlite;
  } data;
} mediadb_impl;

#endif /* #ifndef INTERNAL_H__E8BC6434_EC2B_4DAB_AA47_D4C2FD072866__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: internal.h,v $
 *   Revision 1.1  2004/04/27 09:12:28  nedko
 *   Initial revision.
 *
 *****************************************************************************/
