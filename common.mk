##############################################################################
#
# $Id: common.mk,v 1.3 2005/02/18 21:32:26 nedko Exp $
#
# DESCRIPTION:
#  Top level Makefile for mediadatabase
#
# AUTHOR:
#  Nedko Arnaudov <nedko@users.sourceforge.net>
#
# LICENSE:
#  GNU GENERAL PUBLIC LICENSE version 2
#  
##############################################################################

ifndef VERBOSE_BUILD
TOOL_PREFIX := @
endif

CC := $(TOOL_PREFIX)gcc -c
CXX := $(TOOL_PREFIX)g++ -c
LINK := $(TOOL_PREFIX)gcc
LINK_CXX := $(TOOL_PREFIX)g++
GENDEP_SED_EXPR = "s/^\\(.*\\)\\.o *: /$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g"
GENDEP_C = $(TOOL_PREFIX)set -e; gcc -MM $(CFLAGS) $< | sed $(GENDEP_SED_EXPR) > $@; [ -s $@ ] || rm -f $@
GENDEP_CXX = $(TOOL_PREFIX)set -e; g++ -MM $(CXXFLAGS) $< | sed $(GENDEP_SED_EXPR) > $@; [ -s $@ ] || rm -f $@
CCXXFLAGS := -D_GNU_SOURCE -DVERSION="\"0.1+\""
CFLAGS := $(CCXXFLAGS)
CXXFLAGS := $(CCXXFLAGS)

ECHO := $(TOOL_PREFIX)echo
RM_BEGIN := -$(TOOL_PREFIX)rm
RM_END :=  2>/dev/null ; echo -n

##############################################################################
#
# Modifications log:
#
# !!! WARNING !!! Following lines are automatically updated by the CVS system.
#
#   $Log: common.mk,v $
#   Revision 1.3  2005/02/18 21:32:26  nedko
#   Define version
#
#   Revision 1.2  2004/09/01 05:00:35  nedko
#   Use common c/c++ flags
#   Enable libc features.
#
#   Revision 1.1  2004/06/20 12:24:21  nedko
#   Use dependency files.
#   Use common tool variables.
#
##############################################################################
