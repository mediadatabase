/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: medias.h,v 1.1 2004/05/21 23:43:38 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#ifndef MEDIAS_H__60D45B56_6E25_4DB3_89B0_CF7069076009__INCLUDED
#define MEDIAS_H__60D45B56_6E25_4DB3_89B0_CF7069076009__INCLUDED

GtkWidget *
CreateMediasWindow();

#endif /* #ifndef MEDIAS_H__60D45B56_6E25_4DB3_89B0_CF7069076009__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: medias.h,v $
 *   Revision 1.1  2004/05/21 23:43:38  nedko
 *   Implement media window.
 *
 *****************************************************************************/
