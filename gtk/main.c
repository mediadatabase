/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: main.c,v 1.5 2005/02/19 20:58:32 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#include <stdlib.h>
#include <gtk/gtk.h>
#include <cfl.h>

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libfrontend/conf.h"
#include "../libfrontend/db.h"
#include "../libfrontend/disk.h"
#include "../libfrontend/error.h"
#include "medias.h"
#include "path.h"

GtkWidget *pMainWindow = NULL;

void
SetDefaults()
{
  db_set_defaults();

  disk_set_defaults();
}

void
mediadb_error_callback(
  unsigned int nCritical,
  const char *pszErrorDescription
  )
{
  GtkWidget *pDialog;

  g_warning("%s\n", pszErrorDescription);

  pDialog = gtk_message_dialog_new(
    GTK_WINDOW(pMainWindow),
    GTK_DIALOG_DESTROY_WITH_PARENT,
    GTK_MESSAGE_ERROR,
    GTK_BUTTONS_CLOSE,
    "%s",
    pszErrorDescription);
  gtk_dialog_run(GTK_DIALOG(pDialog));
  gtk_widget_destroy(pDialog);

  if (nCritical == MEDIADB_ERROR_CRITICAL)
  {
    exit(1);
  }
}

int
main(
  int argc,
  char ** argv
  )
{
  gtk_init(&argc, &argv);

  path_init(argv[0]);

  /* Look into configuration file */

  conf_parse();

  SetDefaults();

  db_open();

  pMainWindow = CreateMediasWindow();

  gtk_main();

  if (g_hDB != NULL)
  {
    db_close();
  }

  db_uninit();

  conf_cleanup();

  path_uninit();

  return 0;
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: main.c,v $
 *   Revision 1.5  2005/02/19 20:58:32  nedko
 *   Implement conf file writing.
 *
 *   Revision 1.4  2005/02/18 02:16:42  nedko
 *   Setup path module used to access project specific data files.
 *
 *   Revision 1.3  2004/05/21 23:43:38  nedko
 *   Implement media window.
 *
 *   Revision 1.2  2004/05/16 20:19:08  nedko
 *   Display MessageDialog on error.
 *   Detect row activation within media list.
 *   Set sane default windows size.
 *
 *   Revision 1.1  2004/05/16 19:05:38  nedko
 *   Initial revision of the gtk frontend.
 *
 *****************************************************************************/
