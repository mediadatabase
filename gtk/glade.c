/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: glade.c,v 1.6 2005/02/19 13:37:47 nedko Exp $
 *
 * DESCRIPTION:
 *  libglade helpers
 *
 *****************************************************************************/

#include <stdlib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#include "glade.h"
#include "path.h"

static void
glade_signal_connect_func(
  const gchar *cb_name, GObject *obj, 
  const gchar *signal_name, const gchar *signal_data,
  GObject *conn_obj, gboolean conn_after,
  gpointer user_data)
{
  /** Module with all the symbols of the program */
  static GModule *mod_self = NULL;
  gpointer handler_func;

  /* initialize gmodule */
  if (mod_self == NULL)
  {
    mod_self = g_module_open(NULL, 0);
    g_assert(mod_self != NULL);
  }

/*   g_print("glade_signal_connect_func:" */
/*           " cb_name = '%s', signal_name = '%s', " */
/*           "signal_data = '%s', obj = 0x%08X, conn_after = %s\n", */
/*           cb_name, signal_name, */
/*           signal_data, (unsigned int)obj, conn_after?"true":"false"); */

  if (g_module_symbol(mod_self, cb_name, &handler_func))
  {
    /* found callback */
    if (conn_obj)
    {
      if (conn_after)
      {
        g_signal_connect_object(
          obj,
          signal_name, 
          handler_func,
          conn_obj,
          G_CONNECT_AFTER);
      }
      else
      {
        g_signal_connect_object(
          obj,
          signal_name, 
          handler_func,
          conn_obj,
          G_CONNECT_SWAPPED);
      }
    }
    else
    {
      /* no conn_obj; use standard connect */
      gpointer data = NULL;
      
      data = user_data;
      
      if (conn_after)
      {
        g_signal_connect_after(
          obj,
          signal_name, 
          handler_func,
          data);
      }
      else
      {
        g_signal_connect(
          obj,
          signal_name, 
          handler_func,
          data);
      }
    }
  }
  else
  {
    g_warning("callback function not found: %s", cb_name);
  }
}

GtkWidget *
construct_glade_widget(
  const gchar * id)
{
  gchar * glade_filename;
  GtkWidget * widget;
  GladeXML * xml;

  glade_filename = path_get_data_filename("mediadatabase.glade");
  if (glade_filename == NULL)
  {
    g_warning("Cannot find glade UI description file.");
    exit(1);
  }

  /* load the interface */
  xml = glade_xml_new(glade_filename, id, NULL);

  g_free(glade_filename);

  widget = glade_xml_get_widget(xml, id);

  /* connect the signals in the interface */
  glade_xml_signal_autoconnect_full(
    xml,
    (GladeXMLConnectFunc)glade_signal_connect_func,
    widget);

  return widget;
}

GtkWidget *
get_glade_widget_child(
  GtkWidget * root,
  const gchar * id)
{
  GladeXML * xml;
  GtkWidget * widget;

  xml = glade_get_widget_tree(root);

  widget = glade_xml_get_widget(xml, id);

  return widget;
}

void
setup_text_combo(
  GtkWidget * combo)
{
  GtkCellRenderer * cell;
  GtkListStore * store;

  store = gtk_list_store_new(1, G_TYPE_STRING);
  gtk_combo_box_set_model(GTK_COMBO_BOX(combo), GTK_TREE_MODEL(store));
  g_object_unref(store);

  cell = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo), cell, TRUE);

  gtk_cell_layout_set_attributes(
    GTK_CELL_LAYOUT(combo), cell,
    "text", 0,
    NULL);
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: glade.c,v $
 *   Revision 1.6  2005/02/19 13:37:47  nedko
 *   gtk_combo_box_new_text for glade
 *
 *   Revision 1.5  2005/02/18 22:41:26  nedko
 *   Improve connect dump
 *
 *   Revision 1.4  2005/02/18 22:18:07  nedko
 *   Cleanup things and make them better way.
 *
 *   Revision 1.3  2005/02/18 21:38:28  nedko
 *   NOTSURE: Load more than one window.
 *
 *   Revision 1.2  2005/02/18 02:14:22  nedko
 *   Set source file description
 *
 *   Revision 1.1  2005/02/18 02:13:35  nedko
 *   libglade helpers.
 *
 *****************************************************************************/
