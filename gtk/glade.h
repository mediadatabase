/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: glade.h,v 1.4 2005/02/19 13:37:47 nedko Exp $
 *
 * DESCRIPTION:
 *  libglade helpers
 *
 *****************************************************************************/

#ifndef GLADE_H__732DAEDD_44BD_48CC_B8CA_B43FBB672A1D__INCLUDED
#define GLADE_H__732DAEDD_44BD_48CC_B8CA_B43FBB672A1D__INCLUDED

GtkWidget *
construct_glade_widget(
  const gchar * id);

GtkWidget *
get_glade_widget_child(
  GtkWidget * root,
  const gchar * id);

void
setup_text_combo(
  GtkWidget * combo);
  

#endif /* #ifndef GLADE_H__732DAEDD_44BD_48CC_B8CA_B43FBB672A1D__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: glade.h,v $
 *   Revision 1.4  2005/02/19 13:37:47  nedko
 *   gtk_combo_box_new_text for glade
 *
 *   Revision 1.3  2005/02/18 22:18:07  nedko
 *   Cleanup things and make them better way.
 *
 *   Revision 1.2  2005/02/18 02:14:22  nedko
 *   Set source file description
 *
 *   Revision 1.1  2005/02/18 02:13:35  nedko
 *   libglade helpers.
 *
 *****************************************************************************/
