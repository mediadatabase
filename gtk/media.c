/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: media.c,v 1.9 2005/02/20 15:32:23 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libfrontend/db.h"
#include "../libfrontend/error.h"
#include "common.h"
#include "media.h"
#include "helper.h"
#include "path.h"
#include "glade.h"

enum
{
  COL_ICON,
  COL_NAME,
  COL_PATH,
  COL_SIZE,
  COL_TIME,
  NUM_COLS
};

struct InsertChildsContext
{
  GtkTreeStore *pTreeStore;
  GtkTreeIter *pIter;
};

struct MediaWindowContext
{
  mediadb_uint nMediaID;
};

static void
InsertPseudoChild(
  GtkTreeStore *pTreeStore,
  GtkTreeIter *pIter
  )
{
  GtkTreeIter IterChild;

  gtk_tree_store_append(
    pTreeStore,
    &IterChild,
    pIter);
}

static
void
file_callback(
  void *pUserContext,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype Filetype,
  mediadb_uint nSize,
  mediadb_uint nTime)
{
  GtkTreeIter IterChild;
  gchar *pszFullPath;
  char strTime[26];
  time_t time = nTime;
  char strSize[MAX_SIZE_DESCRIPTION];
  struct tm tm;
  GdkPixbuf * pIcon;
  GError * error = NULL;
  char * pszIcon;
  gchar * pszIconFullname;

  localtime_r(&time, &tm);
  sprintf(strTime,
          "%u-%02u-%02u %02u:%02u:%02u",
          1900+tm.tm_year,
          1+tm.tm_mon,
          tm.tm_mday,
          tm.tm_hour,
          tm.tm_min,
          tm.tm_sec);

  get_size_description(nSize, strSize);

  pIcon = NULL;

  if (Filetype == MEDIADB_FILETYPE_DIR)
  {
    pszIcon = "icon_folder.png";
  }
  else
  {
    pszIcon = "icon_unknown.png";
  }

  pszIconFullname = path_get_data_filename(pszIcon);
  if (pszIconFullname != NULL)
  {
    pIcon = gdk_pixbuf_new_from_file(pszIconFullname, &error);
    if (error)
    {
      g_warning("Could not load icon: %s\n", error->message);
      g_error_free(error);
      error = NULL;
    }

    g_free(pszIconFullname);
  }
  else
  {
    g_warning("Could not find icon %s\n", pszIcon);
  }

  gtk_tree_store_append(
    ((struct InsertChildsContext *)pUserContext)->pTreeStore,
    &IterChild,
    ((struct InsertChildsContext *)pUserContext)->pIter);

  gtk_tree_store_set(
    ((struct InsertChildsContext *)pUserContext)->pTreeStore,
    &IterChild,
    COL_ICON, pIcon,
    COL_NAME, pszName,
    COL_SIZE, (Filetype == MEDIADB_FILETYPE_DIR)?NULL:strSize,
    COL_TIME, strTime,
    -1);

  if (Filetype == MEDIADB_FILETYPE_DIR)
  {
    InsertPseudoChild(
      ((struct InsertChildsContext *)pUserContext)->pTreeStore,
      &IterChild);

    pszFullPath = g_malloc(strlen(pszPath)+strlen(pszName)+1);
    sprintf(pszFullPath, "%s%s", pszPath, pszName);
    gtk_tree_store_set(
      ((struct InsertChildsContext *)pUserContext)->pTreeStore,
      &IterChild,
      COL_PATH, pszFullPath,
      -1);
    g_free(pszFullPath);
  }
}

static void
InsertChilds(
  mediadb_uint nMediaID,
  GtkTreeStore *pTreeStore,
  const char *pszPath,
  GtkTreeIter *pIter
  )
{
  struct InsertChildsContext context;
  mediadb_result r;

  context.pTreeStore = pTreeStore;
  context.pIter = pIter;

  r = mediadb_files_get(
    g_hDB,
    nMediaID,
    pszPath,
    file_callback,
    &context);

  if (MEDIADB_IS_ERROR(r))
  {
    mediadb_error_printf(
      MEDIADB_ERROR_NONCRITICAL,
      "Cannot retrieve contents of \"%s/\" directory from media with ID %u (%s)",
      pszPath,
      (unsigned int)nMediaID,
      mediadb_get_error_message(g_hDB));
  }
}

void
OnRowExpanded(
  GtkTreeView *pTreeView,
  GtkTreeIter *pIter,
  GtkTreePath *pPath,
  gpointer pUserData)
{
  GtkTreeModel *pModel;
  gchar *pszPath;
  GtkTreeIter IterChild;

  pModel = gtk_tree_view_get_model(pTreeView);

  if (!gtk_tree_model_iter_children(pModel, &IterChild, pIter))
    return;

  gtk_tree_model_get(pModel, pIter, COL_PATH, &pszPath, -1);

  InsertChilds(
    ((struct MediaWindowContext *)pUserData)->nMediaID,
    GTK_TREE_STORE(pModel),
    pszPath,
    pIter);

  g_free(pszPath);

  gtk_tree_store_remove(GTK_TREE_STORE(pModel), &IterChild);
}

void
OnRowCollapsed(
  GtkTreeView *pTreeView,
  GtkTreeIter *pIter,
  GtkTreePath *pPath,
  gpointer pUserData)
{
  GtkTreeModel *pModel;
  GtkTreeIter IterChild;

  pModel = gtk_tree_view_get_model(pTreeView);

  if (!gtk_tree_model_iter_children(pModel, &IterChild, pIter))
    return;

  do
  {
    gtk_tree_store_remove(GTK_TREE_STORE(pModel), &IterChild);
  }
  while (gtk_tree_model_iter_children(pModel, &IterChild, pIter));

  InsertPseudoChild(GTK_TREE_STORE(pModel), pIter);
}

void
OnMediaWindowDestroy(
  GtkObject *object,
  gpointer user_data)
{
  g_free(user_data);
}

void
CreateMediaWindow(mediadb_uint nMediaID)
{
  mediadb_result r;
  mediadb_uint nTimeAdded;
  mediadb_uint nTotalFiles;
  mediadb_uint nTotalSize;
  char *pszTitle;
  GtkWidget *pDialog;
  GtkWidget *pMediaWindow = NULL;
  GtkWidget *pChild;
  GtkCellRenderer * pRendererText;
	GtkCellRenderer * pRendererPixbuf;
  GtkTreeViewColumn * pColumn;
  GtkTreeStore *pTreeStore;
  struct MediaWindowContext * pContext;
  GString * str;
  char strTime[26];
  char strTotalSize[MAX_SIZE_DESCRIPTION];
  mediadb_mediatype nType;
  time_t time;
  struct tm tm;
  mediadb_uint nLocationID;
  mediadb_uint nLocationTypeID;
  char *pszLocation;
  char *pszLocationType;

  r = mediadb_media_get_properties(
    g_hDB,
    nMediaID,
    &nTimeAdded,
    &nType,
    &nLocationID,
    &pszTitle);
  if (MEDIADB_IS_ERROR(r))
  {
    g_warning("%s\n", mediadb_get_error_message(g_hDB));

    pDialog = gtk_message_dialog_new(
      GTK_WINDOW(pMainWindow),
      GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_MESSAGE_ERROR,
      GTK_BUTTONS_CLOSE,
      "%s",
      mediadb_get_error_message(g_hDB));

    gtk_dialog_run(GTK_DIALOG(pDialog));
    gtk_widget_destroy(pDialog);

    return;
  }

  if (nType != MEDIADB_MT_DATA)
  {
    g_warning("Only data cd type is supported.\n");

    pDialog = gtk_message_dialog_new(
      GTK_WINDOW(pMainWindow),
      GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_MESSAGE_ERROR,
      GTK_BUTTONS_CLOSE,
      "Only data cd type is supported.");

    gtk_dialog_run(GTK_DIALOG(pDialog));
    gtk_widget_destroy(pDialog);
    free(pszTitle);
    return;
  }

  r = mediadb_location_get_properties(
    g_hDB,
    nLocationID,
    &nLocationTypeID,
    &pszLocation);
  if (MEDIADB_IS_ERROR(r))
  {
    g_warning("%s\n", mediadb_get_error_message(g_hDB));

    pDialog = gtk_message_dialog_new(
      GTK_WINDOW(pMainWindow),
      GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_MESSAGE_ERROR,
      GTK_BUTTONS_CLOSE,
      "%s",
      mediadb_get_error_message(g_hDB));

    gtk_dialog_run(GTK_DIALOG(pDialog));
    gtk_widget_destroy(pDialog);

    free(pszTitle);
    return;
  }

  r = mediadb_location_type_get_properties(
    g_hDB,
    nLocationTypeID,
    &pszLocationType);
  if (MEDIADB_IS_ERROR(r))
  {
    g_warning("%s\n", mediadb_get_error_message(g_hDB));

    pDialog = gtk_message_dialog_new(
      GTK_WINDOW(pMainWindow),
      GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_MESSAGE_ERROR,
      GTK_BUTTONS_CLOSE,
      "%s",
      mediadb_get_error_message(g_hDB));

    gtk_dialog_run(GTK_DIALOG(pDialog));
    gtk_widget_destroy(pDialog);

    free(pszTitle);
    free(pszLocation);
    return;
  }

  r = mediadb_media_get_properties_data(
    g_hDB,
    nMediaID,
    &nTotalFiles,
    &nTotalSize);
  if (MEDIADB_IS_ERROR(r))
  {
    g_warning("%s\n", mediadb_get_error_message(g_hDB));

    pDialog = gtk_message_dialog_new(
      GTK_WINDOW(pMainWindow),
      GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_MESSAGE_ERROR,
      GTK_BUTTONS_CLOSE,
      "%s",
      mediadb_get_error_message(g_hDB));

    gtk_dialog_run(GTK_DIALOG(pDialog));
    gtk_widget_destroy(pDialog);

    free(pszTitle);
    free(pszLocation);
    free(pszLocationType);
    return;
  }

  time = nTimeAdded;
  localtime_r(&time, &tm);
  sprintf(strTime,
          "%u-%02u-%02u %02u:%02u:%02u",
          1900+tm.tm_year,
          1+tm.tm_mon,
          tm.tm_mday,
          tm.tm_hour,
          tm.tm_min,
          tm.tm_sec);

  get_size_description(nTotalSize, strTotalSize);

  pContext = g_malloc(sizeof(struct MediaWindowContext));
  pContext->nMediaID = nMediaID;

  pMediaWindow = construct_glade_widget("media_window");

  gtk_window_set_title(GTK_WINDOW(pMediaWindow),
                       pszTitle);

  g_signal_connect(
    G_OBJECT(pMediaWindow),
    "destroy",
    G_CALLBACK(OnMediaWindowDestroy),
    pContext);

  /* -- name -- */
  gtk_label_set_text(GTK_LABEL(get_glade_widget_child(pMediaWindow, "media_window_label_name_data")), pszTitle);
  free(pszTitle);

  str = g_string_sized_new(100);

  /* -- id -- */
  g_string_printf(str, "%u", (unsigned int)nMediaID);
  gtk_label_set_text(GTK_LABEL(get_glade_widget_child(pMediaWindow, "media_window_label_id_data")), str->str);

  /* -- time -- */
  gtk_label_set_text(GTK_LABEL(get_glade_widget_child(pMediaWindow, "media_window_label_time_data")), strTime);

  /* -- location -- */
  pChild = get_glade_widget_child(pMediaWindow, "media_window_label_location_data");
  if (*pszLocation == 0)
  {
    gtk_label_set_text(GTK_LABEL(pChild), "");
  }
  else if (*pszLocationType == 0)
  {
    g_string_printf(str, "%s", pszLocation);
    gtk_label_set_text(GTK_LABEL(pChild), str->str);
  }
  else
  {
    g_string_printf(str, "%s (%s)", pszLocation, pszLocationType);
    gtk_label_set_text(GTK_LABEL(pChild), str->str);
  }

  /* -- type -- */
  /* "Data CD" os hardcoded until we have support for something else */
  gtk_label_set_text(GTK_LABEL(get_glade_widget_child(pMediaWindow, "media_window_label_type_data")), "Data CD");

  /* -- files_count -- */
  g_string_printf(str, "%u", (unsigned int)nTotalFiles);
  gtk_label_set_text(GTK_LABEL(get_glade_widget_child(pMediaWindow, "media_window_label_files_count_data")), str->str);

  /* -- size -- */
  gtk_label_set_text(GTK_LABEL(get_glade_widget_child(pMediaWindow, "media_window_label_size_data")), strTotalSize);

  /* Tree view */
  pChild = get_glade_widget_child(pMediaWindow, "treeview");

  pRendererText = gtk_cell_renderer_text_new();
  pRendererPixbuf = gtk_cell_renderer_pixbuf_new();

  /* --- Column #1 --- */

  pColumn = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(pColumn, "Name");

	gtk_tree_view_column_pack_start(pColumn, pRendererPixbuf, FALSE);
	gtk_tree_view_column_set_attributes(pColumn, pRendererPixbuf,
	                                    "pixbuf", COL_ICON,
	                                    NULL);

	gtk_tree_view_column_pack_start(pColumn, pRendererText, TRUE);
	gtk_tree_view_column_set_attributes(pColumn,
                                      pRendererText,
	                                    "text", COL_NAME,
	                                    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* --- Column #2 --- */

  pColumn = gtk_tree_view_column_new_with_attributes(
    "Size",
    pRendererText,
    "text", COL_SIZE,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* --- Column #3 --- */

  pColumn = gtk_tree_view_column_new_with_attributes(
    "Time",
    pRendererText,
    "text", COL_TIME,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* Create the store */
  pTreeStore = gtk_tree_store_new(
    NUM_COLS,
    GDK_TYPE_PIXBUF,
    G_TYPE_STRING,
    G_TYPE_STRING,
    G_TYPE_STRING,
    G_TYPE_STRING);

  /* Fill the top level items in store */

  InsertChilds(nMediaID, pTreeStore, NULL, NULL);

  /* connect the row-expanded and row-collapsed signals */
  g_signal_connect(
    pChild,
    "row-expanded",
    G_CALLBACK(OnRowExpanded),
    pContext);

  g_signal_connect(
    pChild,
    "row-collapsed",
    G_CALLBACK(OnRowCollapsed),
    pContext);

  /* Set model */
  gtk_tree_view_set_model(GTK_TREE_VIEW(pChild), GTK_TREE_MODEL(pTreeStore));

  gtk_widget_show_all(pMediaWindow);
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: media.c,v $
 *   Revision 1.9  2005/02/20 15:32:23  nedko
 *   Align labels in media dialog in glade
 *
 *   Revision 1.8  2005/02/19 13:38:56  nedko
 *   Remove disabled pre-glade code.
 *
 *   Revision 1.7  2005/02/19 00:26:42  nedko
 *   Add prefix to media window label names
 *
 *   Revision 1.6  2005/02/19 00:21:32  nedko
 *   Switch media window to glade
 *
 *   Revision 1.5  2005/02/18 02:12:08  nedko
 *   Use path module to access project specific data files.
 *
 *   Revision 1.4  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *   Revision 1.3  2004/08/08 00:47:42  nedko
 *   Get more info for media from database.
 *
 *   Revision 1.2  2004/05/22 00:36:24  nedko
 *   set WM_WINDOWS_ROLE
 *
 *   Revision 1.1  2004/05/21 23:43:38  nedko
 *   Implement media window.
 *
 *****************************************************************************/
