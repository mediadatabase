#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libfrontend/db.h"
#include "../libfrontend/error.h"
#include "common.h"
#include "media.h"
#include "helper.h" 
#include "path.h"
#include "glade.h"
#include "search.h"


void ShowAddMediaDialog() {

	GtkWidget *pAddMediaDialog;

	pAddMediaDialog = construct_glade_widget( "Add_Media_Dialog" );

	gtk_widget_show_all( pAddMediaDialog );

};

void ShowAddPlaceDialog() {

	GtkWidget *pAddPlaceDialog;

	pAddPlaceDialog = construct_glade_widget( "Add_Place_Dialog" );

	gtk_widget_show_all( pAddPlaceDialog );
};

void ShowAddCategoryDialog() {

	GtkWidget *pAddCategoryDialog;

	pAddCategoryDialog = construct_glade_widget( "Add_Category_Dialog" );

	gtk_widget_show_all( pAddCategoryDialog );
};
