/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: search.h,v 1.1 2004/08/31 22:40:15 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#ifndef SEARCH_H__1BDAB477_2332_4142_97E5_91D832820FA2__INCLUDED
#define SEARCH_H__1BDAB477_2332_4142_97E5_91D832820FA2__INCLUDED

void
ShowSearchWindow();

#endif /* #ifndef SEARCH_H__1BDAB477_2332_4142_97E5_91D832820FA2__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: search.h,v $
 *   Revision 1.1  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *****************************************************************************/
