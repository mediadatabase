/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: preferences.c,v 1.1 2005/02/19 21:03:03 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#include <gtk/gtk.h>

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libfrontend/db.h"
#include "../libfrontend/disk.h"
#include "../libfrontend/conf.h"
#include "glade.h"

void
on_preferences_window_button_cancel_activate(
  GtkAction *action, 
  GtkWidget *window)
{
  gtk_widget_destroy(window);
}

void
on_preferences_window_button_ok_activate(
  GtkAction *action, 
  GtkWidget *window)
{
  mediadb_result r;
  GtkWidget * dialog;

  r = db_set_mysql_host(
    gtk_entry_get_text(
      GTK_ENTRY(get_glade_widget_child(window, "preferences_window_notebook_mysql_host_entry"))));

  r = db_set_mysql_user(
    gtk_entry_get_text(
      GTK_ENTRY(get_glade_widget_child(window, "preferences_window_notebook_mysql_user_entry"))));

  r = db_set_mysql_pass(
    gtk_entry_get_text(
      GTK_ENTRY(get_glade_widget_child(window, "preferences_window_notebook_mysql_pass_entry"))));

  r = db_set_mysql_database(
    gtk_entry_get_text(
      GTK_ENTRY(get_glade_widget_child(window, "preferences_window_notebook_mysql_db_entry"))));

  r = db_set_sqlite_database(
    gtk_entry_get_text(
      GTK_ENTRY(get_glade_widget_child(window, "preferences_window_notebook_sqlite_dbfile_entry"))));

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_glade_widget_child(window, "radiobutton_mysql"))))
  {
    db_use_mysql();
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_glade_widget_child(window, "radiobutton_sqlite"))))
  {
    db_use_sqlite();
  }

  disk_set_mount_command(
    gtk_entry_get_text(
      GTK_ENTRY(get_glade_widget_child(window, "mount_cmd_entry"))));

  disk_set_unmount_command(
    gtk_entry_get_text(
      GTK_ENTRY(get_glade_widget_child(window, "unmount_cmd_entry"))));

  disk_set_path(
    gtk_entry_get_text(
      GTK_ENTRY(get_glade_widget_child(window, "mountdir_entry"))));

  r = conf_write();

  dialog = gtk_message_dialog_new(
    GTK_WINDOW(window),
    GTK_DIALOG_MODAL,
    GTK_MESSAGE_INFO,
    GTK_BUTTONS_CLOSE,
    "Backend related settings changes will take effect when program is restarted.");

  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);

  gtk_widget_destroy(window);
}

void
on_preferences_activate(
  GtkAction *action, 
  GtkWidget *window)
{
  GtkWidget * pref;
  unsigned int nDBType;

  pref = construct_glade_widget("preferences_window");

  g_signal_connect(
    get_glade_widget_child(pref, "preferences_window_button_cancel"),
    "activate",
    G_CALLBACK(on_preferences_window_button_cancel_activate), pref);

  g_signal_connect(
    get_glade_widget_child(pref, "preferences_window_button_cancel"),
    "released",
    G_CALLBACK(on_preferences_window_button_cancel_activate), pref);

  g_signal_connect(
    get_glade_widget_child(pref, "preferences_window_button_ok"),
    "activate",
    G_CALLBACK(on_preferences_window_button_ok_activate), pref);

  g_signal_connect(
    get_glade_widget_child(pref, "preferences_window_button_ok"),
    "released",
    G_CALLBACK(on_preferences_window_button_ok_activate), pref);

  nDBType = db_get_type();
  if (nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    gtk_toggle_button_set_active(
      GTK_TOGGLE_BUTTON(get_glade_widget_child(pref, "radiobutton_mysql")),
      TRUE);
  }
  else if (nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    gtk_toggle_button_set_active(
      GTK_TOGGLE_BUTTON(get_glade_widget_child(pref, "radiobutton_sqlite")),
      TRUE);
  }

  gtk_entry_set_text(
    GTK_ENTRY(get_glade_widget_child(pref, "mount_cmd_entry")),
    disk_get_mount_command());

  gtk_entry_set_text(
    GTK_ENTRY(get_glade_widget_child(pref, "unmount_cmd_entry")),
    disk_get_unmount_command());

  gtk_entry_set_text(
    GTK_ENTRY(get_glade_widget_child(pref, "mountdir_entry")),
    disk_get_path());

  gtk_entry_set_text(
    GTK_ENTRY(get_glade_widget_child(pref, "preferences_window_notebook_mysql_host_entry")),
    db_get_mysql_host());

  gtk_entry_set_text(
    GTK_ENTRY(get_glade_widget_child(pref, "preferences_window_notebook_mysql_user_entry")),
    db_get_mysql_user());

  gtk_entry_set_text(
    GTK_ENTRY(get_glade_widget_child(pref, "preferences_window_notebook_mysql_pass_entry")),
    db_get_mysql_pass());

  gtk_entry_set_text(
    GTK_ENTRY(get_glade_widget_child(pref, "preferences_window_notebook_mysql_db_entry")),
    db_get_mysql_database());

  gtk_entry_set_text(
    GTK_ENTRY(get_glade_widget_child(pref, "preferences_window_notebook_sqlite_dbfile_entry")),
    db_get_sqlite_database());

  if (window != NULL)
  {
    gtk_window_set_transient_for(
      GTK_WINDOW(pref),
      GTK_WINDOW(window));
  }
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: preferences.c,v $
 *   Revision 1.1  2005/02/19 21:03:03  nedko
 *   Implement preferences window.
 *
 *****************************************************************************/
