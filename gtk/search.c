/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: search.c,v 1.6 2005/02/19 13:39:20 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#include <limits.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>

/* ULLONG_MAX should come from limits.h but doesn't */
#if !defined(ULLONG_MAX) && defined(ULONG_LONG_MAX)
# define ULLONG_MAX	ULONG_LONG_MAX
#endif

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libfrontend/db.h"
#include "common.h"
#include "search.h"
#include "helper.h"
#include "path.h"
#include "glade.h"

enum
{
  COL_ICON,
  COL_NAME,
  COL_SIZE,
  COL_TIME,
  COL_MEDIAID,
  COL_MEDIAICON,
  COL_MEDIANAME,
  COL_MEDIALOCATION,
  NUM_COLS
};

#define KEY_PREFIX                "mediadatabase_file_search_"
#define KEY_FILENAME              KEY_PREFIX "filename"
#define KEY_FILENAME_PMM          KEY_PREFIX "filename_pmm"
#define KEY_PATH                  KEY_PREFIX "path"
#define KEY_PATH_PMM              KEY_PREFIX "path_pmm"
#define KEY_MIN_SIZE              KEY_PREFIX "min_size"
#define KEY_MIN_SIZE_UNITS        KEY_PREFIX "min_size_units"
#define KEY_MAX_SIZE              KEY_PREFIX "max_size"
#define KEY_MAX_SIZE_UNITS        KEY_PREFIX "max_size_units"
#define KEY_PMM                   KEY_PREFIX "pmm"
#define KEY_STORE                 KEY_PREFIX "store"

void
SearchCallback(
  void *pUserContext,
  mediadb_uint nMediaID,
  mediadb_mediatype nMediaType,
  const char *pszMediaName,
  const char *pszMediaLocation,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype Filetype,
  mediadb_uint nSize,
  mediadb_uint nTime)
{
  struct tm tm;
  GdkPixbuf * pIcon;
  GdkPixbuf * pMediaIcon;
  time_t time = nTime;
  GtkTreeIter Iter;
  char strTime[26];
  GError * error = NULL;
  char * pszIcon;
  char strSize[MAX_SIZE_DESCRIPTION];
  char *pszFullPath;
  gchar * pszIconFullname;

  pszFullPath = g_malloc(strlen(pszPath)+strlen(pszName)+1);
  sprintf(pszFullPath, "%s%s", pszPath, pszName);

  localtime_r(&time, &tm);
  sprintf(strTime,
          "%u-%02u-%02u %02u:%02u:%02u",
          1900+tm.tm_year,
          1+tm.tm_mon,
          tm.tm_mday,
          tm.tm_hour,
          tm.tm_min,
          tm.tm_sec);

  get_size_description(nSize, strSize);

  pIcon = NULL;

  if (Filetype == MEDIADB_FILETYPE_DIR)
  {
    pszIcon = "icon_folder.png";
  }
  else
  {
    pszIcon = "icon_unknown.png";
  }

  pszIconFullname = path_get_data_filename(pszIcon);
  if (pszIconFullname != NULL)
  {
    pIcon = gdk_pixbuf_new_from_file(pszIconFullname, &error);
    if (error)
    {
      g_warning("Could not load icon: %s\n", error->message);
      g_error_free(error);
      error = NULL;
    }

    g_free(pszIconFullname);
  }
  else
  {
    g_warning("Could not find icon %s\n", pszIcon);
  }

  pszIcon = "icon_cd.png";

  pszIconFullname = path_get_data_filename(pszIcon);
  if (pszIconFullname != NULL)
  {
    pMediaIcon = gdk_pixbuf_new_from_file(pszIconFullname, &error);
    if (error)
    {
      g_warning("Could not load icon: %s\n", error->message);
      g_error_free(error);
      error = NULL;
    }

    g_free(pszIconFullname);
  }
  else
  {
    g_warning("Could not find icon %s\n", pszIcon);
  }

  gtk_list_store_append(
    GTK_LIST_STORE(
      g_object_get_data(
        G_OBJECT(pUserContext),
        KEY_STORE)),
    &Iter);

  gtk_list_store_set(
    GTK_LIST_STORE(
      g_object_get_data(
        G_OBJECT(pUserContext),
        KEY_STORE)),
    &Iter,
    COL_ICON, pIcon,
    COL_NAME, pszFullPath,
    COL_SIZE, (Filetype == MEDIADB_FILETYPE_DIR)?NULL:strSize,
    COL_TIME, strTime,
    COL_MEDIAID, (unsigned int)nMediaID,
    COL_MEDIAICON, pMediaIcon,
    COL_MEDIANAME, pszMediaName,
    COL_MEDIALOCATION, pszMediaLocation,
    -1);

  g_free(pszFullPath);
}

void
DoSearch(
  GtkEntry *pEntry,
  gpointer pUserData)
{
  GtkWidget *pDialog;
  const char *pszFilename;
  gint nFilenamePMMIndex;
  const char *pszPath;
  gint nPathPMMIndex;
  const char *pszMinSize;
  gint nMinSizeUnits;
  const char *pszMaxSize;
  gint nMaxSizeUnits;
  const struct mediadb_pattern_match_method *pPMM;
  mediadb_uint nMinSize;
  mediadb_uint nMaxSize;
  char *pchEnd;
  mediadb_result r;

  pPMM = (const struct mediadb_pattern_match_method *)g_object_get_data(
    G_OBJECT(pUserData),
    KEY_PMM);

  pszFilename = gtk_entry_get_text(
    GTK_ENTRY(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_FILENAME)));

  nFilenamePMMIndex = gtk_combo_box_get_active(
    GTK_COMBO_BOX(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_FILENAME_PMM)));

  pszPath = gtk_entry_get_text(
    GTK_ENTRY(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_PATH)));

  nPathPMMIndex = gtk_combo_box_get_active(
    GTK_COMBO_BOX(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_PATH_PMM)));

  pszMinSize = gtk_entry_get_text(
    GTK_ENTRY(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_MIN_SIZE)));

  nMinSizeUnits = gtk_combo_box_get_active(
    GTK_COMBO_BOX(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_MIN_SIZE_UNITS)));

  pszMaxSize = gtk_entry_get_text(
    GTK_ENTRY(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_MAX_SIZE)));

  nMaxSizeUnits = gtk_combo_box_get_active(
    GTK_COMBO_BOX(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_MAX_SIZE_UNITS)));

  /* We better handle such thing in backend
  if (*pszFilename == 0 &&
      *pszPath == 0 &&
      *pszMinSize == 0 &&
      *pszMaxSize == 0)
  {
    pDialog = gtk_message_dialog_new(
      GTK_WINDOW(pUserData),
      GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_MESSAGE_ERROR,
      GTK_BUTTONS_CLOSE,
      "No search criteria ...\n");

    gtk_dialog_run(GTK_DIALOG(pDialog));
    gtk_widget_destroy(pDialog);

    return;
  }
  */

  if (*pszMinSize &&
      *pszMaxSize &&
      nMinSizeUnits > nMaxSizeUnits)
  {
    pDialog = gtk_message_dialog_new(
      GTK_WINDOW(pUserData),
      GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_MESSAGE_ERROR,
      GTK_BUTTONS_CLOSE,
      "Min size units (%s) are bigger than max size units (%s).",
      g_arrSizeUnits[nMinSizeUnits].pszUnitsName,
      g_arrSizeUnits[nMaxSizeUnits].pszUnitsName);

    gtk_dialog_run(GTK_DIALOG(pDialog));
    gtk_widget_destroy(pDialog);

    return;
  }

  if (*pszMinSize)
  {
    nMinSize = strtoull(pszMinSize, &pchEnd, 10);
    if (nMinSize == 0 || nMinSize == ULLONG_MAX || *pchEnd)
    {
      pDialog = gtk_message_dialog_new(
        GTK_WINDOW(pUserData),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_CLOSE,
        "Cannot convert \"%s\" to decimal integer number.",
        pszMinSize);

      gtk_dialog_run(GTK_DIALOG(pDialog));
      gtk_widget_destroy(pDialog);

      return;
    }

    if (ULLONG_MAX / g_arrSizeUnits[nMinSizeUnits].nUnitSize < nMinSize)
    {
      pDialog = gtk_message_dialog_new(
        GTK_WINDOW(pUserData),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_CLOSE,
        "%llu %s is too large number.",
        nMinSize,
        g_arrSizeUnits[nMinSizeUnits].pszUnitsName);

      gtk_dialog_run(GTK_DIALOG(pDialog));
      gtk_widget_destroy(pDialog);

      return;
    }

    nMinSize = nMinSize * g_arrSizeUnits[nMinSizeUnits].nUnitSize;
  }

  if (*pszMaxSize)
  {
    nMaxSize = strtoull(pszMaxSize, &pchEnd, 10);
    if (nMaxSize == 0 || nMaxSize == ULLONG_MAX || *pchEnd)
    {
      pDialog = gtk_message_dialog_new(
        GTK_WINDOW(pUserData),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_CLOSE,
        "Cannot convert \"%s\" to decimal integer number.",
        pszMaxSize);

      gtk_dialog_run(GTK_DIALOG(pDialog));
      gtk_widget_destroy(pDialog);

      return;
    }

    if (ULLONG_MAX / g_arrSizeUnits[nMaxSizeUnits].nUnitSize < nMaxSize)
    {
      pDialog = gtk_message_dialog_new(
        GTK_WINDOW(pUserData),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_CLOSE,
        "%llu %s is too large number.",
        nMaxSize,
        g_arrSizeUnits[nMaxSizeUnits].pszUnitsName);

      gtk_dialog_run(GTK_DIALOG(pDialog));
      gtk_widget_destroy(pDialog);

      return;
    }

    nMaxSize = nMaxSize * g_arrSizeUnits[nMaxSizeUnits].nUnitSize;
  }

  pDialog = gtk_message_dialog_new(
    GTK_WINDOW(pUserData),
    GTK_DIALOG_DESTROY_WITH_PARENT,
    GTK_MESSAGE_INFO,
    GTK_BUTTONS_CLOSE,
    "Searching ...\n"
    "%s%s%s%s%s"
    "%s%s%s%s%s"
    "%s%s%s%s%s"
    "%s%s%s%s%s",
    *pszFilename?"Filename \"":"",
    *pszFilename?pszFilename:"",
    *pszFilename?"\" - ":"",
    *pszFilename?pPMM[nFilenamePMMIndex].pszDescription:"",
    *pszFilename?"\n":"",
    *pszPath?"Path \"":"",
    *pszPath?pszPath:"",
    *pszPath?"\" - ":"",
    *pszPath?pPMM[nPathPMMIndex].pszDescription:"",
    *pszPath?"\n":"",
    *pszMinSize?"Min size ":"",
    *pszMinSize?pszMinSize:"",
    *pszMinSize?" ":"",
    *pszMinSize?g_arrSizeUnits[nMinSizeUnits].pszUnitsName:"",
    *pszMinSize?"\n":"",
    *pszMaxSize?"Max size ":"",
    *pszMaxSize?pszMaxSize:"",
    *pszMaxSize?" ":"",
    *pszMaxSize?g_arrSizeUnits[nMaxSizeUnits].pszUnitsName:"",
    *pszMaxSize?"\n":"");

  gtk_dialog_run(GTK_DIALOG(pDialog));
  gtk_widget_destroy(pDialog);

  gtk_list_store_clear(
    GTK_LIST_STORE(
      g_object_get_data(
        G_OBJECT(pUserData),
        KEY_STORE)));

  r = mediadb_files_search(
    g_hDB,
    *pszFilename?pPMM[nFilenamePMMIndex].nID:MEDIADB_PMM_NULL,
    *pszFilename?pszFilename:NULL,
    *pszPath?pPMM[nPathPMMIndex].nID:MEDIADB_PMM_NULL,
    *pszPath?pszPath:NULL,
    *pszMinSize?&nMinSize:NULL,
    *pszMaxSize?&nMaxSize:NULL,
    SearchCallback,
    pUserData);
  if (MEDIADB_IS_ERROR(r))
  {
    display_error_message_mediadb(GTK_WINDOW(pUserData));
    return;
  }
}

void
ShowSearchWindow()
{
  GtkWidget *pSearchWindow;
  GtkWidget *pChild;
  GtkCellRenderer * pRendererText;
	GtkCellRenderer * pRendererPixbuf;
  GtkTreeViewColumn * pColumn;
  GtkListStore *pListStore;
  const struct mediadb_pattern_match_method *pPMM;
  mediadb_result r;
  unsigned int i, j;

  r = mediadb_get_pattern_match_methods(
    g_hDB,
    &pPMM);
  if (MEDIADB_IS_ERROR(r))
  {
    display_error_message_mediadb(GTK_WINDOW(pMainWindow));
    return;
  }

  pSearchWindow = construct_glade_widget("search_window");

  g_object_set_data(G_OBJECT(pSearchWindow), KEY_PMM, (gpointer)pPMM);

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_filename_entry");

    g_signal_connect(
      pChild,
      "activate",
      (GCallback)DoSearch,
      pSearchWindow);

    g_object_set_data(G_OBJECT(pSearchWindow), KEY_FILENAME, pChild);
  }

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_filename_pmm");
    setup_text_combo(pChild);

    i = 0;

    while (pPMM[i].nID != MEDIADB_PMM_NULL)
    {
      gtk_combo_box_append_text(GTK_COMBO_BOX(pChild), pPMM[i].pszName);
      i++;
    }

    gtk_combo_box_set_active(GTK_COMBO_BOX(pChild), 0);

    g_object_set_data(G_OBJECT(pSearchWindow), KEY_FILENAME_PMM, pChild);
  }

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_path_entry");

    g_signal_connect(
      pChild,
      "activate",
      (GCallback)DoSearch,
      pSearchWindow);

    g_object_set_data(G_OBJECT(pSearchWindow), KEY_PATH, pChild);
  }

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_path_pmm");
    setup_text_combo(pChild);

    i = 0;

    while (pPMM[i].nID != MEDIADB_PMM_NULL)
    {
      gtk_combo_box_append_text(GTK_COMBO_BOX(pChild), pPMM[i].pszName);
      i++;
    }

    gtk_combo_box_set_active(GTK_COMBO_BOX(pChild), 0);

    g_object_set_data(G_OBJECT(pSearchWindow), KEY_PATH_PMM, pChild);
  }

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_min_size_entry");

    g_signal_connect(
      pChild,
      "activate",
      (GCallback)DoSearch,
      pSearchWindow);

    g_object_set_data(G_OBJECT(pSearchWindow), KEY_MIN_SIZE, pChild);
  }

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_min_size_units");
    setup_text_combo(pChild);

    i = j = 0;

    while (g_arrSizeUnits[i].nUnitSize != 0)
    {
      if (i == 1)
      {
        /* Second entry should be kilobytes */
        j = i;
      }

      gtk_combo_box_append_text(GTK_COMBO_BOX(pChild), g_arrSizeUnits[i].pszUnitsName);

      i++;
    }

    gtk_combo_box_set_active(GTK_COMBO_BOX(pChild), j);

    g_object_set_data(G_OBJECT(pSearchWindow), KEY_MIN_SIZE_UNITS, pChild);
  }

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_max_size_entry");

    g_signal_connect(
      pChild,
      "activate",
      (GCallback)DoSearch,
      pSearchWindow);

    g_object_set_data(G_OBJECT(pSearchWindow), KEY_MAX_SIZE, pChild);
  }

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_max_size_units");
    setup_text_combo(pChild);

    i = j = 0;

    while (g_arrSizeUnits[i].nUnitSize != 0)
    {
      if (i == 1)
      {
        /* Second entry should be kilobytes */
        j = i;
      }

      gtk_combo_box_append_text(GTK_COMBO_BOX(pChild), g_arrSizeUnits[i].pszUnitsName);

      i++;
    }

    gtk_combo_box_set_active(GTK_COMBO_BOX(pChild), j);

    g_object_set_data(G_OBJECT(pSearchWindow), KEY_MAX_SIZE_UNITS, pChild);
  }

  {
    pChild = get_glade_widget_child(pSearchWindow, "search_window_button_find");

    g_signal_connect(
      pChild,
      "clicked",
      (GCallback)DoSearch,
      pSearchWindow);
  }

  /* Tree view setup */
  pChild = get_glade_widget_child(pSearchWindow, "treeview");

  pRendererText = gtk_cell_renderer_text_new();
  pRendererPixbuf = gtk_cell_renderer_pixbuf_new();

  /* --- Column #1 --- */

  pColumn = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(pColumn, "Name");

	gtk_tree_view_column_pack_start(pColumn, pRendererPixbuf, FALSE);
	gtk_tree_view_column_set_attributes(pColumn, pRendererPixbuf,
	                                    "pixbuf", COL_ICON,
	                                    NULL);

	gtk_tree_view_column_pack_start(pColumn, pRendererText, TRUE);
	gtk_tree_view_column_set_attributes(pColumn,
                                      pRendererText,
	                                    "text", COL_NAME,
	                                    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* --- Column #2 --- */

  pColumn = gtk_tree_view_column_new_with_attributes(
    "Size",
    pRendererText,
    "text", COL_SIZE,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* --- Column #3 --- */

  pColumn = gtk_tree_view_column_new_with_attributes(
    "Time",
    pRendererText,
    "text", COL_TIME,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* --- Column #4 --- */

  pColumn = gtk_tree_view_column_new_with_attributes(
    "MediaID",
    pRendererText,
    "text", COL_MEDIAID,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* --- Column #5 --- */

  pColumn = gtk_tree_view_column_new();

	gtk_tree_view_column_set_title(
    pColumn,
    "MediaName");

	gtk_tree_view_column_pack_start(
    pColumn,
    pRendererPixbuf,
    FALSE);

	gtk_tree_view_column_set_attributes(
    pColumn,
    pRendererPixbuf,
    "pixbuf",
    COL_MEDIAICON,
    NULL);

	gtk_tree_view_column_pack_end(
    pColumn,
    pRendererText,
    TRUE);

	gtk_tree_view_column_set_attributes(
    pColumn,
    pRendererText,
    "text", COL_MEDIANAME,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* --- Column #6 --- */

  pColumn = gtk_tree_view_column_new_with_attributes(
    "Location",
    pRendererText,
    "text", COL_MEDIALOCATION,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pChild),
    pColumn);

  /* Create the store */
  pListStore = gtk_list_store_new(
    NUM_COLS,
    GDK_TYPE_PIXBUF,            /* COL_ICON */
    G_TYPE_STRING,              /* COL_NAME */
    G_TYPE_STRING,              /* COL_SIZE */
    G_TYPE_STRING,              /* COL_TIME */
    G_TYPE_UINT,                /* COL_MEDIAID */
    GDK_TYPE_PIXBUF,            /* COL_MEDIAICON */
    G_TYPE_STRING,              /* COL_MEDIANAME */
    G_TYPE_STRING);             /* COL_MEDIALOCATION */

  /* Set model */
  gtk_tree_view_set_model(GTK_TREE_VIEW(pChild), GTK_TREE_MODEL(pListStore));

  g_object_set_data(G_OBJECT(pSearchWindow), KEY_STORE, pListStore);

  gtk_widget_show_all(pSearchWindow);
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: search.c,v $
 *   Revision 1.6  2005/02/19 13:39:20  nedko
 *   Switch search window to glade.
 *
 *   Revision 1.5  2005/02/18 02:12:08  nedko
 *   Use path module to access project specific data files.
 *
 *   Revision 1.4  2004/09/18 21:34:59  nedko
 *   Return media name, media type and media location when searching files.
 *
 *   Revision 1.3  2004/09/01 05:05:43  nedko
 *   Search partitially works.
 *
 *   Revision 1.2  2004/09/01 00:56:43  nedko
 *   Refine look.
 *   Attach callback for search action.
 *
 *   Revision 1.1  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *****************************************************************************/
