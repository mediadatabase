/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: medias.c,v 1.6 2005/02/19 21:02:34 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libfrontend/db.h"
#include "common.h"
#include "helper.h"
#include "media.h"
#include "menu.h"
#include "path.h"
#include "glade.h"

enum
{
  COL_MEDIAID = 0,
  COL_MEDIAICON,
  COL_MEDIANAME,
  COL_MEDIAFILES,
  COL_MEDIASIZE,
  COL_MEDIADATE,
  COL_MEDIALOCATION,
  COL_MEDIACOMMENT,
  NUM_COLS
};

void
media_callback(
  void *pUserContext,
  mediadb_uint nMediaID,
  mediadb_mediatype nType,
  const char *pszName,
  const char *pszComment,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize,
  mediadb_uint nLocationID,
  const char *pszLocation
  )
{
  GtkTreeIter iter;
  char strTime[26];
  time_t time = nTimeAdded;
  char strTotalSize[MAX_SIZE_DESCRIPTION];
  struct tm tm;
  GdkPixbuf * pIcon;
  GError * error = NULL;
  char * pszIcon;
  gchar * pszIconFullname;

/*   ctime_r(&time, strTime); */
/*   strTime[strlen(strTime) - 1] = 0; */

  localtime_r(&time, &tm);
  sprintf(strTime,
          "%u-%02u-%02u %02u:%02u:%02u",
          1900+tm.tm_year,
          1+tm.tm_mon,
          tm.tm_mday,
          tm.tm_hour,
          tm.tm_min,
          tm.tm_sec);

  get_size_description(nTotalSize, strTotalSize);

  pIcon = NULL;

  if (nType == MEDIADB_MT_AUDIO)
  {
    pszIcon = "icon_acd.png";
  }
  else if (nType == MEDIADB_MT_DATA)
  {
    pszIcon = "icon_cd.png";
  }
  else
  {
    pszIcon = NULL;
  }

  if (pszIcon != NULL)
  {
    pszIconFullname = path_get_data_filename(pszIcon);
    if (pszIconFullname != NULL)
    {
      pIcon = gdk_pixbuf_new_from_file(pszIconFullname, &error);
      if (error)
      {
        g_warning("Could not load icon: %s\n", error->message);
        g_error_free(error);
        error = NULL;
      }

      g_free(pszIconFullname);
    }
    else
    {
      g_warning("Could not find icon %s\n", pszIcon);
    }
  }

  gtk_list_store_prepend(
    (GtkListStore *)pUserContext,
    &iter);
  gtk_list_store_set(
    (GtkListStore *)pUserContext,
    &iter,
    COL_MEDIAID, (guint)nMediaID,
    COL_MEDIAICON, pIcon,
    COL_MEDIANAME, pszName,
    COL_MEDIAFILES, (guint)nTotalFiles,
    COL_MEDIASIZE, strTotalSize,
    COL_MEDIADATE, strTime,
    COL_MEDIALOCATION, pszLocation,
    COL_MEDIACOMMENT, pszComment,
    -1);
}

static GtkTreeModel *
create_and_fill_model(void)
{
  mediadb_result r;
  GtkListStore *pStore;
  GtkWidget *pDialog;

  pStore = gtk_list_store_new(
    NUM_COLS,
    G_TYPE_UINT,                /* COL_MEDIAID */
    GDK_TYPE_PIXBUF,            /* COL_MEDIAICON */
    G_TYPE_STRING,              /* COL_MEDIANAME */
    G_TYPE_UINT,                /* COL_MEDIAFILES */
    G_TYPE_STRING,              /* COL_MEDIASIZE */
    G_TYPE_STRING,              /* COL_MEDIADATE */
    G_TYPE_STRING,              /* COL_MEDIALOCATION */
    G_TYPE_STRING);             /* COL_MEDIACOMMENT */

  if (g_hDB != NULL)
  {
    r = mediadb_media_get_all(
      g_hDB,
      media_callback,
      pStore);
    if (MEDIADB_IS_ERROR(r))
    {
      g_warning("%s\n", mediadb_get_error_message(g_hDB));

      pDialog = gtk_message_dialog_new(
        GTK_WINDOW(pMainWindow),
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_CLOSE,
        "%s",
        mediadb_get_error_message(g_hDB));

      gtk_dialog_run(GTK_DIALOG(pDialog));
      gtk_widget_destroy(pDialog);
    }
  }

  return GTK_TREE_MODEL(pStore);
}

void
view_onRowActivated(
   GtkTreeView *pTreeView,
   GtkTreePath  *pPath,
   GtkTreeViewColumn  *pColumn,
   gpointer userdata)
{
  GtkTreeModel *pModel;
  GtkTreeIter iter;
  unsigned int nMediaID;

  pModel = gtk_tree_view_get_model(pTreeView);

  if (gtk_tree_model_get_iter(pModel, &iter, pPath))
  {
    gtk_tree_model_get(pModel, &iter, COL_MEDIAID, &nMediaID, -1);

    CreateMediaWindow(nMediaID);
  }
}

static void
setup_view_and_model(GtkWidget *pTreeView)
{
  GtkTreeViewColumn   *col;
  GtkCellRenderer     *renderer;
  GtkTreeModel        *model;
	GtkCellRenderer * pRendererPixbuf;

  renderer = gtk_cell_renderer_text_new();
  pRendererPixbuf = gtk_cell_renderer_pixbuf_new();

  /* --- Column #1 --- */

  col = gtk_tree_view_column_new_with_attributes(
    "ID",
    renderer,
    "text", COL_MEDIAID,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pTreeView),
    col);

  /* --- Column #2 --- */

  col = gtk_tree_view_column_new();

	gtk_tree_view_column_set_title(col, "Name");

	gtk_tree_view_column_pack_start(col, pRendererPixbuf, FALSE);
	gtk_tree_view_column_set_attributes(col, pRendererPixbuf,
	                                    "pixbuf", COL_MEDIAICON,
	                                    NULL);

	gtk_tree_view_column_pack_end(col, renderer, TRUE);
	gtk_tree_view_column_set_attributes(col,
                                      renderer,
	                                    "text", COL_MEDIANAME,
	                                    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pTreeView),
    col);

  gtk_tree_view_column_set_resizable(col, TRUE);
//  gtk_tree_view_column_set_max_width(col, 200);
//  gtk_tree_view_column_set_min_width(col, 200);

  /* --- Column #3 --- */

  col = gtk_tree_view_column_new_with_attributes(
    "Location",
    renderer,
    "text", COL_MEDIALOCATION,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pTreeView),
    col);

  /* --- Column #4 --- */

  col = gtk_tree_view_column_new_with_attributes(
    "Files Size",
    renderer,
    "text", COL_MEDIASIZE,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pTreeView),
    col);

  /* --- Column #5 --- */

  col = gtk_tree_view_column_new_with_attributes(
    "Files Count",
    renderer,
    "text", COL_MEDIAFILES,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pTreeView),
    col);

  /* --- Column #6 --- */

  col = gtk_tree_view_column_new_with_attributes(
    "Date added",
    renderer,
    "text", COL_MEDIADATE,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pTreeView),
    col);

  /* --- Column #7 --- */

  col = gtk_tree_view_column_new_with_attributes(
    "Comment",
    renderer,
    "text", COL_MEDIACOMMENT,
    NULL);

  gtk_tree_view_append_column(
    GTK_TREE_VIEW(pTreeView),
    col);

  model = create_and_fill_model();

  gtk_tree_view_set_model(GTK_TREE_VIEW(pTreeView), model);

  g_object_unref(model); /* destroy model automatically with view */

  g_signal_connect(
    pTreeView,
    "row-activated",
    (GCallback)view_onRowActivated,
    NULL);
}

GtkWidget *
CreateMediasWindow()
{
  GtkWidget *pMediasWindow;

  pMediasWindow = construct_glade_widget("medias_window");

  setup_view_and_model(get_glade_widget_child(pMediasWindow, "medias_treeview"));

  gtk_widget_show_all(pMediasWindow);

  return pMediasWindow;
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: medias.c,v $
 *   Revision 1.6  2005/02/19 21:02:34  nedko
 *   Don't try to access database when it is not opened.
 *
 *   Revision 1.5  2005/02/18 22:20:05  nedko
 *   Cleanup things and make them better way.
 *
 *   Revision 1.4  2005/02/18 02:12:47  nedko
 *   Use path module to access project specific data files.
 *   Use libglade.
 *
 *   Revision 1.3  2004/08/31 22:40:15  nedko
 *   Partitally implemented search feature.
 *
 *   Revision 1.2  2004/05/22 00:36:24  nedko
 *   set WM_WINDOWS_ROLE
 *
 *   Revision 1.1  2004/05/21 23:43:38  nedko
 *   Implement media window.
 *
 *****************************************************************************/
