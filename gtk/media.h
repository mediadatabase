/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: media.h,v 1.1 2004/05/21 23:43:38 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#ifndef MEDIA_H__D81685DF_242E_4897_9040_12D46BD29413__INCLUDED
#define MEDIA_H__D81685DF_242E_4897_9040_12D46BD29413__INCLUDED

void
CreateMediaWindow(mediadb_uint nMediaID);

#endif /* #ifndef MEDIA_H__D81685DF_242E_4897_9040_12D46BD29413__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: media.h,v $
 *   Revision 1.1  2004/05/21 23:43:38  nedko
 *   Implement media window.
 *
 *****************************************************************************/
