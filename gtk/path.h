/* -*- Mode: C ; c-basic-offset: 3 -*- */
/*****************************************************************************
 *
 * $Id: path.h,v 1.1 2005/02/18 02:10:10 nedko Exp $
 *
 * DESCRIPTION:
 *  Access to project specific data files.
 *
 *****************************************************************************/

#ifndef PATH_H__6A0C8189_7048_457D_9081_B0F76AD4B93C__INCLUDED
#define PATH_H__6A0C8189_7048_457D_9081_B0F76AD4B93C__INCLUDED

void
path_init(const char * argv0);

/* g_free the return value if it is not NULL */
gchar *
path_get_data_filename(const gchar * filename);

void
path_uninit();

#endif /* #ifndef PATH_H__6A0C8189_7048_457D_9081_B0F76AD4B93C__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: path.h,v $
 *   Revision 1.1  2005/02/18 02:10:10  nedko
 *   Access to project specific data files - initial revision.
 *
 *****************************************************************************/
