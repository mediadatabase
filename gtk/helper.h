/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: helper.h,v 1.4 2004/10/03 23:33:30 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#ifndef HELPER_H__EE98126E_9844_493D_9BB0_1BD50E70379C__INCLUDED
#define HELPER_H__EE98126E_9844_493D_9BB0_1BD50E70379C__INCLUDED

#include "../libfrontend/helper.h"

void
display_error_message_mediadb(GtkWindow *pWindow);

void
display_error_message_string(GtkWindow *pWindow, const char *pszMessage);

#endif /* #ifndef HELPER_H__EE98126E_9844_493D_9BB0_1BD50E70379C__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: helper.h,v $
 *   Revision 1.4  2004/10/03 23:33:30  nedko
 *   Move size helpers to libfrontend
 *
 *   Revision 1.3  2004/09/01 05:03:51  nedko
 *   Supply window to display_error_message_xxx()
 *   Global "units" array.
 *
 *   Revision 1.2  2004/09/01 00:52:50  nedko
 *   Add helper functions for displaying messages.
 *
 *   Revision 1.1  2004/05/21 23:43:38  nedko
 *   Implement media window.
 *
 *****************************************************************************/
