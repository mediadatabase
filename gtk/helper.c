/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: helper.c,v 1.4 2004/10/03 23:33:30 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#include <gtk/gtk.h>

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libfrontend/db.h"
#include "common.h"
#include "helper.h"

void
display_error_message_string(GtkWindow *pWindow, const char *pszMessage)
{
  GtkWidget *pDialog;

  g_warning("%s\n", pszMessage);

  pDialog = gtk_message_dialog_new(
    pWindow,
    GTK_DIALOG_DESTROY_WITH_PARENT,
    GTK_MESSAGE_ERROR,
    GTK_BUTTONS_CLOSE,
    "%s",
    pszMessage);

  gtk_dialog_run(GTK_DIALOG(pDialog));
  gtk_widget_destroy(pDialog);
}

void
display_error_message_mediadb(GtkWindow *pWindow)
{
  display_error_message_string(
    pWindow,
    mediadb_get_error_message(g_hDB));
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: helper.c,v $
 *   Revision 1.4  2004/10/03 23:33:30  nedko
 *   Move size helpers to libfrontend
 *
 *   Revision 1.3  2004/09/01 05:03:51  nedko
 *   Supply window to display_error_message_xxx()
 *   Global "units" array.
 *
 *   Revision 1.2  2004/09/01 00:52:50  nedko
 *   Add helper functions for displaying messages.
 *
 *   Revision 1.1  2004/05/21 23:43:38  nedko
 *   Implement media window.
 *
 *****************************************************************************/
