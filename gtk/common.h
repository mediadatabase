/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: common.h,v 1.2 2005/02/18 02:12:08 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#ifndef COMMON_H__24B06458_F5A2_4315_997C_8BB9EC02609C__INCLUDED
#define COMMON_H__24B06458_F5A2_4315_997C_8BB9EC02609C__INCLUDED

extern GtkWidget *pMainWindow;

#endif /* #ifndef COMMON_H__24B06458_F5A2_4315_997C_8BB9EC02609C__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: common.h,v $
 *   Revision 1.2  2005/02/18 02:12:08  nedko
 *   Use path module to access project specific data files.
 *
 *   Revision 1.1  2004/05/21 23:43:38  nedko
 *   Implement media window.
 *
 *****************************************************************************/
