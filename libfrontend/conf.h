/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: conf.h,v 1.2 2005/02/19 20:58:32 nedko Exp $
 *
 * DESCRIPTION:
 *  Mediadatabase configuration file public declarations.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef CONF_H__676B81C4_D84B_4F44_9989_E885F11AC710__INCLUDED
#define CONF_H__676B81C4_D84B_4F44_9989_E885F11AC710__INCLUDED

mediadb_result
conf_parse();

mediadb_result
conf_write();

void
conf_cleanup();

#endif /* #ifndef CONF_H__676B81C4_D84B_4F44_9989_E885F11AC710__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: conf.h,v $
 *   Revision 1.2  2005/02/19 20:58:32  nedko
 *   Implement conf file writing.
 *
 *   Revision 1.1  2004/05/16 19:01:17  nedko
 *   libfrontend holds code common to frontends but not in libdb.
 *
 *****************************************************************************/
