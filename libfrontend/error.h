/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: error.h,v 1.2 2004/05/21 23:42:49 nedko Exp $
 *
 * DESCRIPTION:
 *  Mediadatabase error handling public declarations.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef ERROR_H__A25D2F88_F133_41C2_9D56_5617108A7D26__INCLUDED
#define ERROR_H__A25D2F88_F133_41C2_9D56_5617108A7D26__INCLUDED

#define MEDIADB_ERROR_CRITICAL    1
#define MEDIADB_ERROR_NONCRITICAL 2

void
mediadb_error_callback(
  unsigned int nCritical,
  const char *pszErrorDescription
  );

void
mediadb_error_printf(
  unsigned int nCritical,
  const char *pszFormat,
  ...);

#endif /* #ifndef ERROR_H__A25D2F88_F133_41C2_9D56_5617108A7D26__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: error.h,v $
 *   Revision 1.2  2004/05/21 23:42:49  nedko
 *   mediadb_error_callback() now tells if error is critical.
 *
 *   Revision 1.1  2004/05/16 19:01:17  nedko
 *   libfrontend holds code common to frontends but not in libdb.
 *
 *****************************************************************************/
