/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: db.h,v 1.1 2004/05/16 19:01:17 nedko Exp $
 *
 * DESCRIPTION:
 *  Mediadatabase database handling public declarations.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef DB_H__5CEB0A7A_9B33_4DA0_9BBC_65FB4EF17C33__INCLUDED
#define DB_H__5CEB0A7A_9B33_4DA0_9BBC_65FB4EF17C33__INCLUDED

#include "../libdb/libdb.h"

mediadb_result
db_set_mysql_host(const char *pszHost);

const char *
db_get_mysql_host();

mediadb_result
db_set_mysql_user(const char *pszUser);

const char *
db_get_mysql_user();

mediadb_result
db_set_mysql_pass(const char *pszPass);

const char *
db_get_mysql_pass();

mediadb_result
db_set_mysql_database(const char *pszDatabase);

const char *
db_get_mysql_database();

mediadb_result
db_set_sqlite_database(const char *pszDatabaseFullFilename);

const char *
db_get_sqlite_database();

void
db_use_mysql();

void
db_use_sqlite();

unsigned int
db_get_type();

mediadb_result
db_set_defaults();

mediadb_result
db_open();

extern mediadb g_hDB;

void
db_close();

void
db_uninit();

#endif /* #ifndef DB_H__5CEB0A7A_9B33_4DA0_9BBC_65FB4EF17C33__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: db.h,v $
 *   Revision 1.1  2004/05/16 19:01:17  nedko
 *   libfrontend holds code common to frontends but not in libdb.
 *
 *****************************************************************************/
