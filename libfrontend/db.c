/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: db.c,v 1.3 2005/02/19 20:57:46 nedko Exp $
 *
 * DESCRIPTION:
 *  Mediadatabase database handling.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../result.h"
#include "db.h"
#include "error.h"

#define DEFAULT_MYSQL_USER      "mediadatabase"
#define DEFAULT_MYSQL_PASS      ""
#define DEFAULT_MYSQL_HOST      "localhost"
#define DEFAULT_MYSQL_DB        "mediadatabase"
#define DEFAULT_SQLITE_DB        "mediadatabase.sqlitedb"
#define DEFAULT_BACKEND      MEDIADB_DBTYPE_SQLITE

mediadb g_hDB = MEDIADB_INVALID_VALUE;
static unsigned int g_nDBType = MEDIADB_DBTYPE_UNKNOWN;
static char *g_pszMySQLHost = NULL;
static char *g_pszMySQLUser = NULL;
static char *g_pszMySQLPass = NULL;
static char *g_pszMySQLDB = NULL;
static char *g_pszSQLiteDB = NULL;

mediadb_result
db_set_mysql_host(const char *pszHost)
{
  if (g_pszMySQLHost != NULL)
  {
    free(g_pszMySQLHost);
  }

  g_pszMySQLHost = strdup(pszHost);

  if (g_pszMySQLHost == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  return MEDIADB_OK;
}

const char *
db_get_mysql_host()
{
  return g_pszMySQLHost;
}

mediadb_result
db_set_mysql_user(const char *pszUser)
{
  if (g_pszMySQLUser != NULL)
  {
    free(g_pszMySQLUser);
  }

  g_pszMySQLUser = strdup(pszUser);

  if (g_pszMySQLUser == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  return MEDIADB_OK;
}

const char *
db_get_mysql_user()
{
  return g_pszMySQLUser;
}

mediadb_result
db_set_mysql_pass(const char *pszPass)
{
  if (g_pszMySQLPass != NULL)
  {
    free(g_pszMySQLPass);
  }

  g_pszMySQLPass = strdup(pszPass);

  if (g_pszMySQLPass == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  return MEDIADB_OK;
}

const char *
db_get_mysql_pass()
{
  return g_pszMySQLPass;
}

mediadb_result
db_set_mysql_database(const char *pszDatabase)
{
  if (g_pszMySQLDB != NULL)
  {
    free(g_pszMySQLDB);
  }

  g_pszMySQLDB = strdup(pszDatabase);

  if (g_pszMySQLDB == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  return MEDIADB_OK;
}

const char *
db_get_mysql_database()
{
  return g_pszMySQLDB;
}

mediadb_result
db_set_sqlite_database(const char *pszDatabaseFullFilename)
{
  if (g_pszSQLiteDB != NULL)
  {
    free(g_pszSQLiteDB);
  }

  g_pszSQLiteDB = strdup(pszDatabaseFullFilename);

  if (g_pszSQLiteDB == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  return MEDIADB_OK;
}

const char *
db_get_sqlite_database()
{
  return g_pszSQLiteDB;
}

void
db_use_mysql()
{
  g_nDBType = MEDIADB_DBTYPE_MYSQL;
}

void
db_use_sqlite()
{
  g_nDBType = MEDIADB_DBTYPE_SQLITE;
}

unsigned int
db_get_type()
{
  return g_nDBType;
}

mediadb_result
db_set_defaults()
{
  size_t s;
  const char *pszHOME;

  if (g_pszMySQLHost == NULL)
  {
    g_pszMySQLHost = strdup(DEFAULT_MYSQL_HOST);
    if (g_pszMySQLHost == NULL)
    {
      mediadb_error_callback(
        MEDIADB_ERROR_CRITICAL,
        "Out of memory.");
      return MEDIADB_MEM;
    }
  }

  if (g_pszMySQLUser == NULL)
  {
    g_pszMySQLUser = strdup(DEFAULT_MYSQL_USER);
    if (g_pszMySQLUser == NULL)
    {
      mediadb_error_callback(
        MEDIADB_ERROR_CRITICAL,
        "Out of memory.");
      return MEDIADB_MEM;
    }
  }

  if (g_pszMySQLPass == NULL)
  {
    g_pszMySQLPass = strdup(DEFAULT_MYSQL_PASS);
    if (g_pszMySQLPass == NULL)
    {
      mediadb_error_callback(
        MEDIADB_ERROR_CRITICAL,
        "Out of memory.");
      return MEDIADB_MEM;
    }
  }

  if (g_pszMySQLDB == NULL)
  {
    g_pszMySQLDB = strdup(DEFAULT_MYSQL_DB);
    if (g_pszMySQLDB == NULL)
    {
      mediadb_error_callback(
        MEDIADB_ERROR_CRITICAL,
        "Out of memory.");
      return MEDIADB_MEM;
    }
  }

  if (g_pszSQLiteDB == NULL)
  {
    pszHOME = getenv("HOME");

    if (pszHOME == NULL)
    {
      mediadb_error_callback(
        MEDIADB_ERROR_CRITICAL,
        "HOME environment variable not set.");
      return MEDIADB_NO_HOME;
    }

    s = strlen(pszHOME) + strlen(DEFAULT_SQLITE_DB)+2;
    g_pszSQLiteDB = malloc(s);
    if (g_pszSQLiteDB == NULL)
    {
      mediadb_error_callback(
        MEDIADB_ERROR_CRITICAL,
        "Out of memory.");
      return MEDIADB_MEM;
    }
    sprintf(g_pszSQLiteDB,
            "%s/%s",
            pszHOME,
            DEFAULT_SQLITE_DB);
  }

  if (g_nDBType == MEDIADB_DBTYPE_UNKNOWN)
  {
    g_nDBType = DEFAULT_BACKEND;
  }

  return MEDIADB_OK;
}

mediadb_result
db_open()
{
  mediadb_result r;

  r = db_set_defaults();
  if (MEDIADB_IS_ERROR(r))
  {
    return r;
  }

  if (g_nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    r = mediadb_open(
      MEDIADB_DBTYPE_MYSQL,
      g_pszMySQLHost,
      g_pszMySQLUser,
      g_pszMySQLPass,
      g_pszMySQLDB,
      &g_hDB);
  }
  else if (g_nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    r = mediadb_open(
      MEDIADB_DBTYPE_SQLITE,
      NULL,
      NULL,
      NULL,
      g_pszSQLiteDB,
      &g_hDB);
  }
  else
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Unknown backend type (internal error)");
    return MEDIADB_NOT_IMPL;
  }

  if (MEDIADB_IS_ERROR(r))
  {
    mediadb_error_printf(
      MEDIADB_ERROR_NONCRITICAL,
      "Failed to open database. Error is %d (%s)\n",
      (int)r,
      mediadb_get_error_message(NULL));
    return r;
  }

  return MEDIADB_OK;
}

void
db_close()
{
  mediadb_close(g_hDB);
  g_hDB = MEDIADB_INVALID_VALUE;
}

void
db_uninit()
{
/*   if (g_hDB != MEDIADB_INVALID_VALUE) */
/*   { */
/*     mediadb_close(g_hDB); */
/*     g_hDB = MEDIADB_INVALID_VALUE; */
/*   } */

  if (g_pszMySQLHost != NULL)
  {
    free(g_pszMySQLHost);
    g_pszMySQLHost = NULL;
  }

  if (g_pszMySQLUser != NULL)
  {
    free(g_pszMySQLUser);
    g_pszMySQLUser = NULL;
  }

  if (g_pszMySQLPass != NULL)
  {
    free(g_pszMySQLPass);
    g_pszMySQLPass = NULL;
  }

  if (g_pszMySQLDB != NULL)
  {
    free(g_pszMySQLDB);
    g_pszMySQLDB = NULL;
  }

  if (g_pszSQLiteDB != NULL)
  {
    free(g_pszSQLiteDB);
    g_pszSQLiteDB = NULL;
  }
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: db.c,v $
 *   Revision 1.3  2005/02/19 20:57:46  nedko
 *   Treat fail to open database as non-fatal to make possible changing preferences in gtk frontend.
 *
 *   Revision 1.2  2004/05/21 23:42:49  nedko
 *   mediadb_error_callback() now tells if error is critical.
 *
 *   Revision 1.1  2004/05/16 19:01:17  nedko
 *   libfrontend holds code common to frontends but not in libdb.
 *
 *****************************************************************************/
