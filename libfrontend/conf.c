/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: conf.c,v 1.3 2005/02/19 20:58:32 nedko Exp $
 *
 * DESCRIPTION:
 *  Mediadatabase configuration file handling.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <cfl.h>

#include "../result.h"
#include "conf.h"
#include "error.h"
#include "db.h"
#include "disk.h"

#define CONF_FILE            ".mediadatabase"
#define CFL_SECTION_GLOBAL   NULL
#define CFL_SECTION_CUI      "cui"
#define CFL_SECTION_MYSQL    "MySQL"
#define CFL_SECTION_SQLITE   "SQLite"
#define CFL_VALUE_MYSQL_HOST "host"
#define CFL_VALUE_MYSQL_USER "user"
#define CFL_VALUE_MYSQL_PASS "pass"
#define CFL_VALUE_MYSQL_DB   "db"
#define CFL_VALUE_MOUNT      "mount"
#define CFL_VALUE_UNMOUNT    "unmount"
#define CFL_VALUE_MOUNTDIR   "mountdir"
#define CFL_VALUE_SQLITE_DB  "dbfile"
#define CFL_VALUE_BACKEND    "backend"

static cflh_t g_hConf = NULL;

mediadb_result
conf_get_full_name(char ** ppszConf)
{
  const char *pszHOME;
  char *pszConf;
  size_t s;

  pszHOME = getenv("HOME");

  if (pszHOME == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "HOME environment variable not set.");
    return MEDIADB_FAIL;
  }

  s = strlen(pszHOME) + 1 + strlen(CONF_FILE) + 1;

  pszConf = (char *)malloc(s);
  if (pszConf == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  sprintf(pszConf, "%s/%s", pszHOME, CONF_FILE);

  *ppszConf = pszConf;

  return MEDIADB_OK;
}

mediadb_result
conf_parse()
{
  mediadb_result r;
  char *pszConf;
  int nRet;
  FILE *hFile;
  char *pszCFLValue;
  cfl_value_type_t CFLValueType;

  hFile = NULL;
  pszConf = NULL;

  conf_cleanup();

  r = conf_get_full_name(&pszConf);
  if (MEDIADB_IS_ERROR(r))
  {
    goto Exit;
  }

  hFile = fopen(pszConf, "r");
  if (hFile == NULL)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "Cannot open configuration file \"%s\".",
      pszConf);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  g_hConf = cfl_alloc();
  if (g_hConf == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    r = MEDIADB_MEM;
    goto Exit;
  }

  nRet = cfl_init_from_file(
    g_hConf,
    hFile,
    CFL_FILE_TYPE_DEFAULT);
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_init_from_file() failed. (%s)",
      cfl_errcode_to_message(nRet));
    goto Exit;
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_GLOBAL,
    CFL_VALUE_MOUNT,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    r = disk_set_mount_command(pszCFLValue);
    if (MEDIADB_IS_ERROR(r))
    {
      goto Exit;
    }
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_GLOBAL,
    CFL_VALUE_UNMOUNT,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    r = disk_set_unmount_command(pszCFLValue);
    if (MEDIADB_IS_ERROR(r))
    {
      goto Exit;
    }
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_GLOBAL,
    CFL_VALUE_MOUNTDIR,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    r = disk_set_path(pszCFLValue);
    if (MEDIADB_IS_ERROR(r))
    {
      goto Exit;
    }
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_MYSQL,
    CFL_VALUE_MYSQL_HOST,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    r = db_set_mysql_host(pszCFLValue);
    if (MEDIADB_IS_ERROR(r))
    {
      goto Exit;
    }
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_MYSQL,
    CFL_VALUE_MYSQL_DB,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    r = db_set_mysql_database(pszCFLValue);
    if (MEDIADB_IS_ERROR(r))
    {
      goto Exit;
    }
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_MYSQL,
    CFL_VALUE_MYSQL_USER,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    r = db_set_mysql_user(pszCFLValue);
    if (MEDIADB_IS_ERROR(r))
    {
      goto Exit;
    }
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_MYSQL,
    CFL_VALUE_MYSQL_PASS,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    r = db_set_mysql_pass(pszCFLValue);
    if (MEDIADB_IS_ERROR(r))
    {
      goto Exit;
    }
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_SQLITE,
    CFL_VALUE_SQLITE_DB,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    r = db_set_sqlite_database(pszCFLValue);
    if (MEDIADB_IS_ERROR(r))
    {
      goto Exit;
    }
  }

  pszCFLValue = cfl_value_get_by_name(
    g_hConf,
    CFL_SECTION_GLOBAL,
    CFL_VALUE_BACKEND,
    &CFLValueType);

  if (pszCFLValue != NULL &&
      CFLValueType == CFL_TYPE_STRING)
  {
    if (strcmp(pszCFLValue, CFL_SECTION_SQLITE) == 0)
    {
      db_use_sqlite();
    }
    else if (strcmp(pszCFLValue, CFL_SECTION_MYSQL) == 0)
    {
      db_use_mysql();
    }
  }

  r = MEDIADB_OK;

Exit:
  if (hFile != NULL)
  {
    fclose(hFile);
  }

  if (pszConf != NULL)
  {
    free(pszConf);
  }

  return r;
}

mediadb_result
conf_write()
{
  mediadb_result r;
  char *pszConf;
  FILE *hFile;
  int nRet;
  unsigned int nDBType;

  hFile = NULL;
  pszConf = NULL;

  if (g_hConf == NULL)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "Cannot write configuration file without first parsing it.");
    return MEDIADB_UNEXP;
  }

  r = conf_get_full_name(&pszConf);
  if (MEDIADB_IS_ERROR(r))
  {
    goto Exit;
  }

  hFile = fopen(pszConf, "w");
  if (hFile == NULL)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "Cannot open configuration file \"%s\".",
      pszConf);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nRet = cfl_value_set_by_name(
    g_hConf,
    CFL_SECTION_GLOBAL,
    CFL_VALUE_MOUNT,
    CFL_TYPE_STRING,
    (void *)disk_get_mount_command());
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_value_set_by_name() failed with %d",
      nRet);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nRet = cfl_value_set_by_name(
    g_hConf,
    CFL_SECTION_GLOBAL,
    CFL_VALUE_UNMOUNT,
    CFL_TYPE_STRING,
    (void *)disk_get_unmount_command());
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_value_set_by_name() failed with %d",
      nRet);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nRet = cfl_value_set_by_name(
    g_hConf,
    CFL_SECTION_GLOBAL,
    CFL_VALUE_MOUNTDIR,
    CFL_TYPE_STRING,
    (void *)disk_get_path());
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_value_set_by_name() failed with %d",
      nRet);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nRet = cfl_value_set_by_name(
    g_hConf,
    CFL_SECTION_MYSQL,
    CFL_VALUE_MYSQL_HOST,
    CFL_TYPE_STRING,
    (void *)db_get_mysql_host());
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_value_set_by_name() failed with %d",
      nRet);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nRet = cfl_value_set_by_name(
    g_hConf,
    CFL_SECTION_MYSQL,
    CFL_VALUE_MYSQL_USER,
    CFL_TYPE_STRING,
    (void *)db_get_mysql_user());
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_value_set_by_name() failed with %d",
      nRet);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nRet = cfl_value_set_by_name(
    g_hConf,
    CFL_SECTION_MYSQL,
    CFL_VALUE_MYSQL_PASS,
    CFL_TYPE_STRING,
    (void *)db_get_mysql_pass());
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_value_set_by_name() failed with %d",
      nRet);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nRet = cfl_value_set_by_name(
    g_hConf,
    CFL_SECTION_MYSQL,
    CFL_VALUE_MYSQL_DB,
    CFL_TYPE_STRING,
    (void *)db_get_mysql_database());
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_value_set_by_name() failed with %d",
      nRet);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nRet = cfl_value_set_by_name(
    g_hConf,
    CFL_SECTION_SQLITE,
    CFL_VALUE_SQLITE_DB,
    CFL_TYPE_STRING,
    (void *)db_get_sqlite_database());
  if (nRet != 0)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_CRITICAL,
      "cfl_value_set_by_name() failed with %d",
      nRet);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nDBType = db_get_type();
  if (nDBType == MEDIADB_DBTYPE_MYSQL)
  {
    nRet = cfl_value_set_by_name(
      g_hConf,
      CFL_SECTION_GLOBAL,
      CFL_VALUE_BACKEND,
      CFL_TYPE_STRING,
      (void *)CFL_SECTION_MYSQL);
    if (nRet != 0)
    {
      mediadb_error_printf(
        MEDIADB_ERROR_CRITICAL,
        "cfl_value_set_by_name() failed with %d",
        nRet);
      r = MEDIADB_FAIL;
      goto Exit;
    }
  }
  else if (nDBType == MEDIADB_DBTYPE_SQLITE)
  {
    nRet = cfl_value_set_by_name(
      g_hConf,
      CFL_SECTION_GLOBAL,
      CFL_VALUE_BACKEND,
      CFL_TYPE_STRING,
      (void *)CFL_SECTION_SQLITE);
    if (nRet != 0)
    {
      mediadb_error_printf(
        MEDIADB_ERROR_CRITICAL,
        "cfl_value_set_by_name() failed with %d",
        nRet);
      r = MEDIADB_FAIL;
      goto Exit;
    }
  }
  else
  {
      mediadb_error_printf(
        MEDIADB_ERROR_CRITICAL,
        "unknown backend type %u",
        nRet);
      r = MEDIADB_UNEXP;
      goto Exit;
  }

  cfl_write(g_hConf, hFile);

  r = MEDIADB_OK;

Exit:
  if (hFile != NULL)
  {
    fclose(hFile);
  }

  if (pszConf != NULL)
  {
    free(pszConf);
  }

  return r;
}

void
conf_cleanup()
{
  if (g_hConf != NULL)
  {
    cfl_free(g_hConf);
    g_hConf = NULL;
  }
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: conf.c,v $
 *   Revision 1.3  2005/02/19 20:58:32  nedko
 *   Implement conf file writing.
 *
 *   Revision 1.2  2004/05/21 23:42:49  nedko
 *   mediadb_error_callback() now tells if error is critical.
 *
 *   Revision 1.1  2004/05/16 19:01:17  nedko
 *   libfrontend holds code common to frontends but not in libdb.
 *
 *****************************************************************************/
