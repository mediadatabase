/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: disk.h,v 1.1 2004/05/16 19:01:17 nedko Exp $
 *
 * DESCRIPTION:
 *  Mediadatabase disk handling public declarations.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#ifndef DISK_H__1D8BD16B_C0BC_4761_8B57_1C25719E866B__INCLUDED
#define DISK_H__1D8BD16B_C0BC_4761_8B57_1C25719E866B__INCLUDED

//#if defined(LINUX) || defined(OPENBSD)
#define DISK_UNIX
//#endif

mediadb_result
disk_set_path(const char *pszDiskPath);

const char *
disk_get_path();

mediadb_result
disk_open();

typedef mediadb_result (* disk_scan_callback)(
  void *pUserContext,
  const char *pszFilename,
  const mediadb_filetype nType,
  const char *pszPath,
  mediadb_uint nFileSize,
  mediadb_uint nFileTime);

mediadb_result
disk_scan(disk_scan_callback pCallback,
          void *pUserContext,
          mediadb_uint *pnTotalFiles,
          mediadb_uint *pnTotalSize);

mediadb_result
disk_close();

void
disk_uninit();

#if defined(DISK_UNIX)

mediadb_result
disk_set_mount_command(const char *pszMountCommand);

const char *
disk_get_mount_command();

mediadb_result
disk_set_unmount_command(const char *pszUnmountCommand);

const char *
disk_get_unmount_command();

#endif

mediadb_result
disk_set_defaults();

#endif /* #ifndef DISK_H__1D8BD16B_C0BC_4761_8B57_1C25719E866B__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: disk.h,v $
 *   Revision 1.1  2004/05/16 19:01:17  nedko
 *   libfrontend holds code common to frontends but not in libdb.
 *
 *****************************************************************************/
