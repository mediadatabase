/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: disk.c,v 1.2 2004/05/21 23:42:49 nedko Exp $
 *
 * DESCRIPTION:
 *  Mediadatabase disk handling.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/wait.h>

#include "../result.h"
#include "../libdb/libdb.h"
#include "disk.h"
#include "error.h"
#include "../libdb/memory.h"

#if defined(OPENBSD)
#define DEFAULT_MOUNT_CMD       "sudo /sbin/mount /cdrom"
#define DEFAULT_UNMOUNT_CMD     "sudo /sbin/umount /cdrom"
#else
#define DEFAULT_MOUNT_CMD       "/bin/mount /cdrom"
#define DEFAULT_UNMOUNT_CMD     "/bin/umount /cdrom"
#endif
#define DEFAULT_MOUNTDIR        "/cdrom"
#define MAX_PATH_DEPTH          65535

char *g_pszDiskPath = NULL;
int g_blnMediaMounted = 0;
char *g_pszMountCommand = NULL;
char *g_pszUnmountCommand = NULL;

struct scan_context
{
  disk_scan_callback pCallback;
  void *pUserContext;
  char *pszPathBuffer;
  size_t nPathBufferSize;
  size_t nDiskPathPrefixLength;
  unsigned long nTotalFiles;
  unsigned long long nTotalSize;
};

mediadb_result
disk_set_path(const char *pszDiskPath)
{
  if (g_pszDiskPath != NULL)
  {
    free(g_pszDiskPath);
  }

  g_pszDiskPath = strdup(pszDiskPath);

  if (g_pszDiskPath == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  return MEDIADB_OK;
}

const char *
disk_get_path()
{
  return g_pszDiskPath;
}

mediadb_result
disk_set_mount_command(const char *pszMountCommand)
{
  if (g_pszMountCommand != NULL)
  {
    free(g_pszMountCommand);
  }

  g_pszMountCommand = strdup(pszMountCommand);

  if (g_pszMountCommand == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  return MEDIADB_OK;
}

const char *
disk_get_mount_command()
{
  return g_pszMountCommand;
}

mediadb_result
disk_set_unmount_command(const char *pszUnmountCommand)
{
  if (g_pszUnmountCommand != NULL)
  {
    free(g_pszUnmountCommand);
  }

  g_pszUnmountCommand = strdup(pszUnmountCommand);

  if (g_pszUnmountCommand == NULL)
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
    return MEDIADB_MEM;
  }

  return MEDIADB_OK;
}

const char *
disk_get_unmount_command()
{
  return g_pszUnmountCommand;
}

mediadb_result
disk_set_defaults()
{
  if (g_pszMountCommand == NULL)
  {
    g_pszMountCommand = strdup(DEFAULT_MOUNT_CMD);
    if (g_pszMountCommand == NULL)
    {
      mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
      return MEDIADB_MEM;
    }
  }

  if (g_pszUnmountCommand == NULL)
  {
    g_pszUnmountCommand = strdup(DEFAULT_UNMOUNT_CMD);
    if (g_pszUnmountCommand == NULL)
    {
      mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
      return MEDIADB_MEM;
    }
  }

  if (g_pszDiskPath == NULL)
  {
    g_pszDiskPath = strdup(DEFAULT_MOUNTDIR);
    if (g_pszDiskPath == NULL)
    {
      mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory.");
      return MEDIADB_MEM;
    }
  }

  return MEDIADB_OK;
}

static
mediadb_result
ScanDir(struct scan_context *pContext, unsigned long nDepth)
{
  DIR *pDir;
  struct dirent *pDE;
  size_t nCurrentPathLength;
  struct stat st;
  size_t sizeName;
  mediadb_result r;

  if (nDepth >= MAX_PATH_DEPTH)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_NONCRITICAL,
      "Max path depth of \"%u\" reached.\n",
      (unsigned int)MAX_PATH_DEPTH);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  nCurrentPathLength = strlen(pContext->pszPathBuffer);

  pDir = opendir(pContext->pszPathBuffer);
  if (pDir == NULL)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_NONCRITICAL,
      "Cannot open \"%s\"",
      pContext->pszPathBuffer);
    r = MEDIADB_FAIL;
    goto Exit;
  }

  while ((pDE = readdir(pDir)) != NULL)
  {
    if (strcmp(pDE->d_name, ".") == 0)
      continue;

    if (strcmp(pDE->d_name, "..") == 0)
      continue;

#if defined(OPENBSD)
    sizeName = pDE->d_namlen;
#else
    sizeName = strlen(pDE->d_name);
#endif

    r = maybe_enlarge_buffer(
      &pContext->pszPathBuffer,
      &pContext->nPathBufferSize,
      nCurrentPathLength + sizeName + 1);
    if (MEDIADB_IS_ERROR(r))
    {
      mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory");
      goto ExitCloseDir;
    }

    memcpy(pContext->pszPathBuffer + nCurrentPathLength, pDE->d_name, sizeName);
    pContext->pszPathBuffer[nCurrentPathLength + sizeName] = 0;

    if (lstat(pContext->pszPathBuffer, &st) != 0)
    {
      mediadb_error_printf(
        MEDIADB_ERROR_NONCRITICAL,
        "Cannot stat() %s. Error is %d (%s)",
        pContext->pszPathBuffer,
        errno,
        strerror(errno));
      r = MEDIADB_FAIL;
      goto ExitCloseDir;
    }

    pContext->pszPathBuffer[nCurrentPathLength] = 0;

    r = pContext->pCallback(
      pContext->pUserContext,
      pDE->d_name,
      (S_ISDIR(st.st_mode))?MEDIADB_FILETYPE_DIR:MEDIADB_FILETYPE_FILE,
      pContext->pszPathBuffer + pContext->nDiskPathPrefixLength,
      ((S_ISDIR(st.st_mode))?0:st.st_size),
      st.st_ctime);
    if (MEDIADB_IS_ERROR(r))
    {
      goto ExitCloseDir;
    }

    pContext->nTotalFiles++;

    if (S_ISDIR(st.st_mode))
    {
      memcpy(pContext->pszPathBuffer + nCurrentPathLength, pDE->d_name, sizeName);
      memcpy(pContext->pszPathBuffer + nCurrentPathLength + sizeName, "/", 2);
      r = ScanDir(pContext, nDepth + 1);
      if (MEDIADB_IS_ERROR(r))
      {
        goto ExitCloseDir;
      }
    }
    else
    {
      pContext->nTotalSize += st.st_size;
    }
  }

ExitCloseDir:
  closedir(pDir);

Exit:
  return r;
}

mediadb_result
disk_scan(disk_scan_callback pCallback,
          void *pUserContext,
          mediadb_uint *pnTotalFiles,
          mediadb_uint *pnTotalSize)
{
  mediadb_result r;
  struct scan_context context;

  context.pCallback = pCallback;
  context.pUserContext = pUserContext;
  context.pszPathBuffer = NULL;
  context.nPathBufferSize = 0;
  context.nTotalFiles = 0;
  context.nTotalSize = 0;

  context.nDiskPathPrefixLength = strlen(g_pszDiskPath);

  r = maybe_enlarge_buffer(
    &context.pszPathBuffer,
    &context.nPathBufferSize,
    context.nDiskPathPrefixLength + 2);
  if (MEDIADB_IS_ERROR(r))
  {
    mediadb_error_callback(
      MEDIADB_ERROR_CRITICAL,
      "Out of memory");
    return r;
  }

  memcpy(context.pszPathBuffer, g_pszDiskPath, context.nDiskPathPrefixLength);
  memcpy(context.pszPathBuffer + context.nDiskPathPrefixLength, "/", 2);

  r = ScanDir(&context, 0);

  if (MEDIADB_IS_SUCCESS(r))
  {
    *pnTotalFiles = context.nTotalFiles;
    *pnTotalSize = context.nTotalSize;
  }

  return r;
}

static mediadb_result
ExecuteCommand(const char *pszCommand)
{
  int nRet;

  nRet = system(pszCommand);
  if (nRet == -1)
  {
    mediadb_error_printf(
      MEDIADB_ERROR_NONCRITICAL,
      "Cannot execute command \"%s\". "
      "system() returned -1, error is %d (%s)\n",
      pszCommand,
      errno,
      strerror(errno));
    return MEDIADB_FAIL;
  }

  if (WIFEXITED(nRet))
  {
    if (WEXITSTATUS(nRet) != 0)
    {
      mediadb_error_printf(
      MEDIADB_ERROR_NONCRITICAL,
        "Cannot execute command \"%s\". "
        "Command returned %d\n",
        pszCommand,
        (int)WEXITSTATUS(nRet));
      return MEDIADB_FAIL;
    }
  }
  else if (WIFSIGNALED(nRet))
  {
    mediadb_error_printf(
      MEDIADB_ERROR_NONCRITICAL,
      "Cannot execute command \"%s\". "
      "Cammand was terminated because of signal %d\n",
      pszCommand,
      (int)WTERMSIG(nRet));
    return MEDIADB_FAIL;
  }
  else
  {
    mediadb_error_printf(
      MEDIADB_ERROR_NONCRITICAL,
      "Cannot execute command \"%s\". "
      "system() returned %d\n",
      pszCommand,
      nRet);
    return MEDIADB_FAIL;
  }

  return MEDIADB_OK;
}

mediadb_result
disk_open()
{
  mediadb_result r;

  r = ExecuteCommand(g_pszMountCommand);
  if (MEDIADB_IS_SUCCESS(r))
  {
    g_blnMediaMounted = 1;
  }

  return r;
}

mediadb_result
disk_close()
{
  mediadb_result r;

  r = ExecuteCommand(g_pszUnmountCommand);
  if (MEDIADB_IS_SUCCESS(r))
  {
    g_blnMediaMounted = 0;
  }

  return r;
}

void
disk_uninit()
{
  if (g_blnMediaMounted)
  {
    disk_close();
  }

  if (g_pszMountCommand != NULL)
    free(g_pszMountCommand);

  if (g_pszUnmountCommand != NULL)
    free(g_pszUnmountCommand);

  if (g_pszDiskPath != NULL)
    free(g_pszDiskPath);
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: disk.c,v $
 *   Revision 1.2  2004/05/21 23:42:49  nedko
 *   mediadb_error_callback() now tells if error is critical.
 *
 *   Revision 1.1  2004/05/16 19:01:17  nedko
 *   libfrontend holds code common to frontends but not in libdb.
 *
 *****************************************************************************/
