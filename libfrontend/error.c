/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * $Id: error.c,v 1.2 2004/05/21 23:42:49 nedko Exp $
 *
 * DESCRIPTION:
 *  Mediadatabase error handling.
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *
 *****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "../result.h"
#include "error.h"

#define FORMATTING_BUFFER_SIZE 8*1024

void
mediadb_error_printf(
  unsigned int nCritical,
  const char *pszFormat,
  ...)
{
  va_list argList;
  char Buffer[FORMATTING_BUFFER_SIZE];

  va_start(argList, pszFormat);
  vsnprintf(Buffer, FORMATTING_BUFFER_SIZE-1, pszFormat, argList);
  va_end(argList);

  Buffer[FORMATTING_BUFFER_SIZE-1] = 0;

  mediadb_error_callback(nCritical, Buffer);
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: error.c,v $
 *   Revision 1.2  2004/05/21 23:42:49  nedko
 *   mediadb_error_callback() now tells if error is critical.
 *
 *   Revision 1.1  2004/05/16 19:01:17  nedko
 *   libfrontend holds code common to frontends but not in libdb.
 *
 *****************************************************************************/
