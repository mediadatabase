/* -*- Mode: C ; c-basic-offset: 3 -*- */
/*****************************************************************************
 *
 * $Id: helper.h,v 1.1 2004/10/03 23:33:55 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#ifndef HELPER_H__34E38ED1_AE06_4D91_95B2_F6676BDCFFCA__INCLUDED
#define HELPER_H__34E38ED1_AE06_4D91_95B2_F6676BDCFFCA__INCLUDED

#include "../result.h"
#include "../libdb/libdb.h"

#define MAX_SIZE_DESCRIPTION 1024

struct size_units
{
  mediadb_uint nUnitSize;       /* Unit size, in bytes */
  const char *pszUnitName;
  const char *pszUnitsName;
};

extern struct size_units g_arrSizeUnits[];

void
get_size_description(
  mediadb_uint nSize,
  char *pSizeStringBuffer);

#endif /* #ifndef HELPER_H__34E38ED1_AE06_4D91_95B2_F6676BDCFFCA__INCLUDED */

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: helper.h,v $
 *   Revision 1.1  2004/10/03 23:33:55  nedko
 *   Move size helpers from gtk frontend
 *
 *****************************************************************************/
