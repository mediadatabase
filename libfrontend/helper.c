/* -*- Mode: C ; c-basic-offset: 3 -*- */
/*****************************************************************************
 *
 * $Id: helper.c,v 1.1 2004/10/03 23:33:55 nedko Exp $
 *
 * DESCRIPTION:
 *  
 *
 * NOTES:
 *  
 *
 *****************************************************************************/

#include <stdio.h>

#include "helper.h"

#define KILOBYTE ((unsigned long long)           1024)
#define MEGABYTE ((unsigned long long)  KILOBYTE*1024)
#define GIGABYTE ((unsigned long long)  MEGABYTE*1024)
#define TERABYTE ((unsigned long long)  GIGABYTE*1024)

struct size_units g_arrSizeUnits[] =
{
  {1,         "Byte",      "Bytes"     },
  {KILOBYTE,  "Kilobyte",  "Kilobytes" },
  {MEGABYTE,  "Megabyte",  "Megabytes" },
  {GIGABYTE,  "Gigabyte",  "Gigabytes" },
  {TERABYTE,  "Terabyte",  "Terabytes" },
  {0,         NULL,        NULL        }
};

void
get_size_description(
  mediadb_uint nSize,
  char *pSizeStringBuffer)
{
  unsigned int n, m;

  if (nSize < KILOBYTE)         /* In bytes */
  {
    n = nSize;
    sprintf(pSizeStringBuffer,
            "%u bytes",
            n);
  }
  else if (nSize < MEGABYTE) /* In kilobytes */
  {
    n = nSize / KILOBYTE;
    m = nSize % KILOBYTE;
    m *= 100;
    m /= KILOBYTE;

    if (m == 0)
    {
      sprintf(pSizeStringBuffer,
              "%u K",
              n);
    }
    else
    {
      sprintf(pSizeStringBuffer,
              "%u,%u K",
              n, m);
    }
  }
  else if (nSize < GIGABYTE) /* In megabytes */
  {
    n = nSize / MEGABYTE;
    m = nSize % MEGABYTE;
    m *= 100;
    m /= MEGABYTE;

    if (m == 0)
    {
      sprintf(pSizeStringBuffer,
              "%u M",
              n);
    }
    else
    {
      sprintf(pSizeStringBuffer,
              "%u.%u M",
              n, m);
    }
  }
  else if (nSize < TERABYTE) /* In gigabytes */
  {
    n = nSize / GIGABYTE;
    m = nSize % GIGABYTE;
    m *= 100;
    m /= GIGABYTE;

    if (m == 0)
    {
      sprintf(pSizeStringBuffer,
              "%u G",
              n);
    }
    else
    {
      sprintf(pSizeStringBuffer,
              "%u.%u G",
              n, m);
    }
  }
  else                          /* In terabytes */
  {
    n = nSize / TERABYTE;
    m = nSize % TERABYTE;
    m *= 100;
    m /= TERABYTE;

    if (m == 0)
    {
      sprintf(pSizeStringBuffer,
              "%u T",
              n);
    }
    else
    {
      sprintf(pSizeStringBuffer,
              "%u.%u T",
              n, m);
    }
  }
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: helper.c,v $
 *   Revision 1.1  2004/10/03 23:33:55  nedko
 *   Move size helpers from gtk frontend
 *
 *****************************************************************************/
