//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainDialog.h"
#include "AboutDialog.h"
#include "DbPreferencesDialog.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Exit1Click(TObject *Sender)
{
        this->Close();        
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::About1Click(TObject *Sender)
{
        AboutBox->ShowModal();        
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::DatabasePreferences1Click(TObject *Sender)
{
        DatabasePreferences->ShowModal();         
}
//---------------------------------------------------------------------------
