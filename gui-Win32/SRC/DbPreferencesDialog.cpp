//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DBAccess.hpp"

#include "DbPreferencesDialog.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBAccess"
#pragma link "MyAccess"
#pragma resource "*.dfm"
TDatabasePreferences *DatabasePreferences;
//---------------------------------------------------------------------------
__fastcall TDatabasePreferences::TDatabasePreferences(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TDatabasePreferences::cbDefaultPortClick(TObject *Sender)
{
        if ( cbDefaultPort->Checked == TRUE ) {
                ebPort->Text = "3306";
                ebPort->Enabled = FALSE;
                }
             else
                ebPort->Enabled = TRUE;
}
//---------------------------------------------------------------------------
void __fastcall TDatabasePreferences::cbDefaultUserClick(TObject *Sender)
{
        if ( cbDefaultUser->Checked == TRUE ) {
                ebUsername->Text = "mediabase";
                ebUsername->Enabled = FALSE;
                ebPassword->Text = "mediabase";
                ebPassword->Enabled = FALSE;
                }
                else {
                  ebUsername->Enabled = TRUE;
                  ebPassword->Enabled = TRUE;
                };
}
//---------------------------------------------------------------------------
void __fastcall TDatabasePreferences::Button2Click(TObject *Sender)
{
        this->Close();        
}
//---------------------------------------------------------------------------



void __fastcall TDatabasePreferences::TestConnectionClick(TObject *Sender)
{
bool debug;

        MySQLTest->Username = this->ebUsername->Text;
        MySQLTest->Password = this->ebPassword->Text;
        MySQLTest->Port = this->ebPort->Text.ToInt();

        debug = MySQLTest->Connected;

        MySQLTest->Connect();


}
//---------------------------------------------------------------------------

