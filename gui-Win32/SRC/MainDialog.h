//---------------------------------------------------------------------------

#ifndef MainDialogH
#define MainDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainDialogMenu;
        TMenuItem *File1;
        TMenuItem *View1;
        TMenuItem *Help1;
        TMenuItem *BrowseHelp1;
        TMenuItem *About1;
        TMenuItem *N1;
        TMenuItem *Exit1;
        TMenuItem *Databases1;
        TMenuItem *ConectDatabase1;
        TMenuItem *DisconectDatabse1;
        TMenuItem *Media1;
        TMenuItem *AddMedia1;
        TMenuItem *N2;
        TMenuItem *DatabasePreferences1;
        TStatusBar *MainStatusBar;
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall About1Click(TObject *Sender);
        void __fastcall DatabasePreferences1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
