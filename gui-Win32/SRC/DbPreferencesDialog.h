//---------------------------------------------------------------------------

#ifndef DbPreferencesDialogH
#define DbPreferencesDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <DB.hpp>
#include <DBTables.hpp>
#include "DBAccess.hpp"
#include "MyAccess.hpp"
//---------------------------------------------------------------------------
class TDatabasePreferences : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TEdit *ebRemoteHost;
        TEdit *ebUsername;
        TEdit *ebPassword;
        TLabel *RemoteHostLabel;
        TLabel *UsernameLabel;
        TLabel *PasswordLabel;
        TCheckBox *cbDefaultPort;
        TEdit *ebPort;
        TCheckBox *cbDefaultUser;
        TButton *Button1;
        TButton *Button2;
        TButton *TestConnection;
        TMyConnection *MySQLTest;
        void __fastcall cbDefaultPortClick(TObject *Sender);
        void __fastcall cbDefaultUserClick(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall TestConnectionClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TDatabasePreferences(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDatabasePreferences *DatabasePreferences;
//---------------------------------------------------------------------------
#endif
