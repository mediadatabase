object MainForm: TMainForm
  Left = 286
  Top = 257
  Width = 696
  Height = 480
  Caption = 'mediadatabase gui for Win32'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainDialogMenu
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MainStatusBar: TStatusBar
    Left = 0
    Top = 415
    Width = 688
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 'Not Connected'
  end
  object MainDialogMenu: TMainMenu
    Left = 656
    Top = 384
    object File1: TMenuItem
      Caption = '&File'
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
    end
    object View1: TMenuItem
      Caption = 'Views'
    end
    object Databases1: TMenuItem
      Caption = 'Databases'
      object ConectDatabase1: TMenuItem
        Caption = 'Connect Database'
        ShortCut = 113
      end
      object DisconectDatabse1: TMenuItem
        Caption = 'Disconnect Database'
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object DatabasePreferences1: TMenuItem
        Caption = 'Database Preferences ...'
        OnClick = DatabasePreferences1Click
      end
    end
    object Media1: TMenuItem
      Caption = 'Local Media'
      object AddMedia1: TMenuItem
        Caption = 'Scan Media ...'
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object BrowseHelp1: TMenuItem
        Caption = 'Browse Help'
        ShortCut = 112
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object About1: TMenuItem
        Caption = 'About'
        OnClick = About1Click
      end
    end
  end
end
