object DatabasePreferences: TDatabasePreferences
  Left = 278
  Top = 182
  Width = 384
  Height = 335
  Caption = 'Database Preferences'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 313
    Height = 177
    Caption = 'Database'
    TabOrder = 0
    object RemoteHostLabel: TLabel
      Left = 8
      Top = 16
      Width = 71
      Height = 13
      Caption = 'Database Host'
    end
    object UsernameLabel: TLabel
      Left = 8
      Top = 128
      Width = 48
      Height = 13
      Caption = 'Username'
    end
    object PasswordLabel: TLabel
      Left = 120
      Top = 128
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object ebRemoteHost: TEdit
      Left = 8
      Top = 32
      Width = 225
      Height = 21
      TabOrder = 0
      Text = 'localhost'
    end
    object ebUsername: TEdit
      Left = 8
      Top = 144
      Width = 105
      Height = 21
      Enabled = False
      TabOrder = 1
      Text = 'mediabase'
    end
    object ebPassword: TEdit
      Left = 120
      Top = 144
      Width = 105
      Height = 21
      Enabled = False
      PasswordChar = '*'
      TabOrder = 2
      Text = 'mediabase'
    end
    object cbDefaultPort: TCheckBox
      Left = 8
      Top = 72
      Width = 89
      Height = 17
      Caption = 'Default Port'
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = cbDefaultPortClick
    end
    object ebPort: TEdit
      Left = 96
      Top = 68
      Width = 57
      Height = 21
      Enabled = False
      TabOrder = 4
      Text = '3306'
    end
    object cbDefaultUser: TCheckBox
      Left = 8
      Top = 104
      Width = 105
      Height = 17
      Caption = 'Default Username'
      Checked = True
      State = cbChecked
      TabOrder = 5
      OnClick = cbDefaultUserClick
    end
  end
  object Button1: TButton
    Left = 104
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 1
  end
  object Button2: TButton
    Left = 190
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = Button2Click
  end
  object TestConnection: TButton
    Left = 32
    Top = 240
    Width = 75
    Height = 25
    Caption = 'Test'
    TabOrder = 3
    OnClick = TestConnectionClick
  end
  object MySQLTest: TMyConnection
    Database = 'mediadb'
    Username = 'mediabase'
    Password = 'mediabase'
    Server = '10.0.0.5'
    Connected = True
    Left = 8
    Top = 184
  end
end
