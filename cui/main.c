/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * DESCRIPTION:
 *  Portable frontend to MediaDatabase
 *
 * COMPILATION:
 *  gcc main.c -lmysqlclient -o mdb_cui [-DOPENBSD]
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *  
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <sys/wait.h>
#include <pwd.h>
#include <unistd.h>
#include <cfl.h>
#include <assert.h>
extern char **environ;

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libdb/memory.h"
#include "../libfrontend/error.h"
#include "../libfrontend/conf.h"
#include "../libfrontend/db.h"
#include "../libfrontend/disk.h"
#include "../libfrontend/helper.h"

#define MAX_TITLE_SIZE 128      /* in db */
#define MAX_PASS_SIZE 1024
#define MAX_READ_STRING_SIZE 10240
//#define PRINT_FILENAMES

char strTitle[MAX_TITLE_SIZE+2]; /* + newline + null */
char strPass[MAX_PASS_SIZE];
mediadb_uint nMediaID = 0ULL;
int blnExiting = 0;
char *pszTitle = NULL;
const char *pszHOME;

#define MODE_LIST_MEDIA     0
#define MODE_ADD_MEDIA      1
#define MODE_UPDATE_MEDIA   2
#define MODE_SEARCH_FILES   3
unsigned int nMode = MODE_SEARCH_FILES;

void
ExecuteCommand(const char *pszCommand);

void
MountMedia()
{
  printf("--> Mounting media ...\n");
  disk_open();
  printf("--> Media mounted.\n");
}

void
UnmountMedia()
{
  printf("--> Unmounting media ...\n");
  disk_close();
  printf("--> Media unmounted.\n");
}
 
void
Exit(int nRet)
{
  if (blnExiting)
    return;

  blnExiting = 1;

  disk_uninit();

  conf_cleanup();

  db_uninit();

  if (pszTitle != NULL)
    free(pszTitle);

  exit(nRet);
}

void
mediadb_error_callback(
  unsigned int nCritical,
  const char *pszErrorDescription
  )
{
  fprintf(stderr,
          "%s\n",
          pszErrorDescription);

  if (nCritical == MEDIADB_ERROR_CRITICAL)
  {
    Exit(1);
  }
}

void
ExecuteCommand(const char *pszCommand)
{
  int nRet;

  nRet = system(pszCommand);
  if (nRet == -1)
  {
    fprintf(stderr,
            "Cannot execute command \"%s\". "
            "system() returned -1, error is %d (%s)\n",
            pszCommand,
            errno,
            strerror(errno));
    Exit(1);
  }

  if (WIFEXITED(nRet))
  {
    if (WEXITSTATUS(nRet) != 0)
    {
      fprintf(stderr,
              "Cannot execute command \"%s\". "
              "Command returned %d\n",
              pszCommand,
              (int)WEXITSTATUS(nRet));
      Exit(1);
    }
  }
  else if (WIFSIGNALED(nRet))
  {
    fprintf(stderr,
            "Cannot execute command \"%s\". "
            "Cammand was terminated because of signal %d\n",
            pszCommand,
            (int)WTERMSIG(nRet));
    Exit(1);
  }
  else
  {
    fprintf(stderr,
            "Cannot execute command \"%s\". "
            "system() returned %d\n",
            pszCommand,
            nRet);
    Exit(1);
  }
}

void
ReadTitleExitOnEmpty()
{
  char *pch;

  printf("Title: ");
  if (fgets(strTitle, sizeof(strTitle), stdin) == NULL)
  {
    printf("\n");

    if (feof(stdin))
    {
      Exit(0);
    }

    fprintf(stderr, "Error occured while reading standard input.\n");
    Exit(1);
  }
  else if (strTitle[0] == '\n')
  {
    Exit(0);
  }

  if ((pch = strchr(strTitle, '\n')) == NULL)
  {
    fprintf(stderr, "input line too long.\n");
    Exit(1);
  }

  *pch = '\0';
}

void
ReadMediaID()
{
  char *pch;
  char strMediaID[100];

  printf("Media ID: ");
  if (fgets(strMediaID, sizeof(strMediaID), stdin) == NULL)
  {
    printf("\n");

    if (feof(stdin))
    {
      Exit(0);
    }

    fprintf(stderr, "Error occured while reading standard input.\n");
    Exit(1);
  }
  else if (strMediaID[0] == '\n')
  {
    Exit(0);
  }

  if ((pch = strchr(strMediaID, '\n')) == NULL)
  {
    fprintf(stderr, "input line too long.\n");
    Exit(1);
  }

  *pch = '\0';

  nMediaID = strtoull(strMediaID, NULL, 10);
  if (nMediaID == 0)
  {
    Exit(1);
  }
}

mediadb_bool
ReadInteger(
  const char *pszQuestion,
  mediadb_bool blnExitOnEmpty,
  mediadb_uint *pn)
{
  char *pch;
  char str[100];

  printf("%s", pszQuestion);
  if (fgets(str, sizeof(str), stdin) == NULL)
  {
    printf("\n");

    if (feof(stdin))
    {
      Exit(0);
    }

    fprintf(stderr, "Error occured while reading standard input.\n");
    Exit(1);
  }
  else if (str[0] == '\n')
  {
    if (blnExitOnEmpty)
    {
      Exit(0);
    }
    else
    {
      return MEDIADB_FALSE;
    }
  }

  if ((pch = strchr(str, '\n')) == NULL)
  {
    fprintf(stderr, "input line too long.\n");
    Exit(1);
  }

  *pch = '\0';

  *pn = strtoull(str, NULL, 10);
  return MEDIADB_TRUE;
}

mediadb_bool
ReadString(
  const char *pszQuestion,
  mediadb_bool blnExitOnEmpty,
  char *pBuffer)
{
  char *pch;

  printf("%s", pszQuestion);
  if (fgets(pBuffer, MAX_READ_STRING_SIZE, stdin) == NULL)
  {
    printf("\n");

    if (feof(stdin))
    {
      Exit(0);
    }

    fprintf(stderr, "Error occured while reading standard input.\n");
    Exit(1);
  }
  else if (pBuffer[0] == '\n')
  {
    if (blnExitOnEmpty)
    {
      Exit(0);
    }
    else
    {
      return MEDIADB_FALSE;
    }
  }

  if ((pch = strchr(pBuffer, '\n')) == NULL)
  {
    fprintf(stderr, "input line too long.\n");
    Exit(1);
  }

  *pch = '\0';
  return MEDIADB_TRUE;
}

void
Menu(
  const char *pszQuestion,
  const char **ppszOptions,
  unsigned int nOptionsCount,
  unsigned int nDefaultIndex,
  unsigned int *pnIndex)
{
  char *pch;
  char str[100];
  unsigned int i;
  unsigned long choice;

  assert(nDefaultIndex < nOptionsCount);

Retry:
  printf("%s\n", pszQuestion);
  for (i = 0 ; i < nOptionsCount ; i++)
  {
    printf("  [%u] %s", i+1, ppszOptions[i]);
    if (i == nDefaultIndex)
    {
      printf(" (default)\n");
    }
    else
    {
      printf("\n");
    }
  }

  printf("What is your choice ? [default %u] ", nDefaultIndex+1);
  if (fgets(str, sizeof(str), stdin) == NULL)
  {
    printf(" \n");

    if (feof(stdin))
    {
      Exit(0);
    }

    fprintf(stderr, "Error occured while reading standard input.\n");
    Exit(1);
  }
  else if (str[0] == '\n')
  {
    *pnIndex = nDefaultIndex;
    return;
  }

  if ((pch = strchr(str, '\n')) == NULL)
  {
    fprintf(stderr, "input line too long.\n");
    Exit(1);
  }

  *pch = '\0';

  choice = strtoul(str, NULL, 10);

  if (choice < 1 || choice > nOptionsCount)
  {
    printf("Cannot interpret \"%s\" as valid menu selection.", str);
    goto Retry;
  }

  *pnIndex = choice - 1;
}

void
ReadTitleIgnoreOnEmpty()
{
  char *pch;

  printf("Title: ");
  if (fgets(strTitle, sizeof(strTitle), stdin) == NULL)
  {
    printf("\n");

    if (feof(stdin))
    {
      Exit(0);
    }

    fprintf(stderr, "Error occured while reading standard input.\n");
    Exit(1);
  }
  else if (strTitle[0] == '\n')
  {
    strcpy(strTitle, pszTitle);
    return;
  }

  if ((pch = strchr(strTitle, '\n')) == NULL)
  {
    fprintf(stderr, "input line too long.\n");
    Exit(1);
  }

  *pch = '\0';
}

void
AskForPassword()
{
  char *pszPass;

  pszPass = getpass("MySQL password: ");

  strcpy(strPass, pszPass);
}

void
MaybeEnlargeBuffer(char ** ppBuffer,
                   size_t *pnBufferSize,
                   size_t nSizeRequired)
{
  mediadb_result r;

  r = maybe_enlarge_buffer(
    ppBuffer,
    pnBufferSize,
    nSizeRequired);
  if (MEDIADB_IS_ERROR(r))
  {
      fprintf(stderr,
              "Cannot enlarge buffer.\n");
      Exit(0);
  }
}

mediadb_result
mediadb_scan_callback(
  void *pUserContext,
  const char *pszFilename,
  const mediadb_filetype nType,
  const char *pszPath,
  mediadb_uint nFileSize,
  mediadb_uint nFileTime)
{
  mediadb_result r;

  r = mediadb_file_add_new(
    g_hDB,
    nMediaID,
    nType,
    pszPath,
    pszFilename,
    nFileSize,
    nFileTime);
  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Failed to add new file to database. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
  }

#ifdef PRINT_FILENAMES
  printf("%s/%s", pszPath, pszFilename);
#endif

  if (nType == MEDIADB_FILETYPE_DIR)
  {
#ifdef PRINT_FILENAMES
    printf("/\n");
#else
    printf("/");
#endif
  }
  else
  {
#ifdef PRINT_FILENAMES
    printf("\n");
#else
    printf(".");
#endif
  }

  fflush(stdout);

  return r;
}

void
SetDefaults()
{
  db_set_defaults();

  disk_set_defaults();
}

void
Help(char *arg0)
{
  char *pszExecutable;

  pszExecutable = strrchr(arg0, '/');

  if (pszExecutable == NULL)
  {
    pszExecutable = arg0;
  }
  else
  {
    pszExecutable++;
  }

  SetDefaults();

  printf("==============================\n");
  printf("Offline media content database\n");
  printf("==============================\n");
  printf("Usage:\n");
  printf("[<path_to>]%s [options]\n", pszExecutable);
  printf("  Options can be:\n");
  printf("  -? - This help\n");
/*   printf("  -L - List media mode\n"); */
  printf("  -A - Add media mode\n");
  printf("  -U - Update media mode\n");
  printf("  -F - Search files mode\n");
  printf("  -m <mount_cmd> - command to mount media, default is \"%s\"\n", disk_get_mount_command());
  printf("  -n <unmount_cmd> - command to unmount, default is \"%s\"\n", disk_get_unmount_command());
  printf("  -M - Use MySQL backend%s\n", (db_get_type() == MEDIADB_DBTYPE_MYSQL)?" (default)":"");
  printf("  -d <mount_dir> - where media is mounted, default is \"%s\"\n", disk_get_path());
  printf("  -h <mysql_host> - MySQL server host, default is \"%s\"\n", db_get_mysql_host());
  printf("  -u <mysql_user> - MySQL user, default is \"%s\"\n", db_get_mysql_user());
  printf("  -p [mysql_pass] - MySQL password, default is \"%s\"\n", db_get_mysql_pass());
  printf("  -b <mysql_database> - MySQL database, default is \"%s\"\n", db_get_mysql_database());
  printf("  -S - Use SQLite backend%s\n", (db_get_type() == MEDIADB_DBTYPE_SQLITE)?" (default)":"");
  printf("  -f <sqlite_database_file> - SQLite database file, default is \"%s\"\n", db_get_sqlite_database());
  Exit(0);
}

void
AddMedia()
{
  mediadb_result r;
  time_t timeAdded;
  mediadb_uint nTotalFiles;
  mediadb_uint nTotalSize;

  printf("NOTE: Currently audio media is not supported.\n");

Loop:
  printf("--> Please insert media and enter title.\n");

  ReadTitleExitOnEmpty();

  MountMedia();

  printf("--> Processing media ...\n");

  r = mediadb_media_add_new(
    g_hDB,
    strTitle,
    "",
    MEDIADB_MT_DATA,
    &nMediaID);

  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Failed to add new media to database. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
    Exit(1);
  }

  disk_scan(mediadb_scan_callback,
            NULL,
            &nTotalFiles,
            &nTotalSize);

  printf("\n");

  timeAdded = time(NULL);

  r = mediadb_media_update_properties(
    g_hDB,
    nMediaID,
    timeAdded,
    nTotalFiles,
    nTotalSize);

  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Failed to update new media to database. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
    Exit(1);
  }

  UnmountMedia();

  printf("--> SUCCESS - Media added.\n");
  printf("Media title: %s\n", strTitle);
  printf("Media ID: %u\n", (unsigned int)nMediaID);
  printf("Added: %s", ctime(&timeAdded));
  printf("Total files: %u\n", (unsigned int)nTotalFiles);
  printf("Total size: %u\n", (unsigned int)nTotalSize);

  goto Loop;
}

void
UpdateMedia()
{
  mediadb_result r;
  time_t timeAdded;
  mediadb_uint nTimeAdded;
  mediadb_uint nTotalFiles;
  mediadb_uint nTotalSize;
  mediadb_mediatype nType;
  mediadb_uint nLocationID;

  printf("NOTE: Currently audio media is not supported.\n");

Loop:
  printf("--> Please enter Media ID.\n");

  ReadMediaID();

  r = mediadb_media_get_properties(
    g_hDB,
    nMediaID,
    &nTimeAdded,
    &nType,
    &nLocationID,
    &pszTitle);
  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Failed to get media properties. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
    Exit(1);
  }

  if (nType != MEDIADB_MT_DATA)
  {
    fprintf(stderr,
            "Only data cd type is supprted.\n");
    Exit(1);
  }

  r = mediadb_media_get_properties_data(
    g_hDB,
    nMediaID,
    &nTotalFiles,
    &nTotalSize);
  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Failed to get media properties. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
    Exit(1);
  }

  if (strlen(pszTitle) + 1 > MAX_TITLE_SIZE)
  {
    fprintf(stderr,
            "Title too big\n");
    Exit(1);
  }

  timeAdded = (time_t)nTimeAdded;

  printf("Media title: %s\n", pszTitle);
  printf("Added: %s", ctime(&timeAdded));
  printf("Total files: %u\n", (unsigned int)nTotalFiles);
  printf("Total size: %u\n", (unsigned int)nTotalSize);

  printf("--> Please insert media and enter title (Just press return to keep current title).\n");

  ReadTitleIgnoreOnEmpty();

  MountMedia();

  printf("--> Processing media ...\n");

  if (strcmp(pszTitle, strTitle) != 0)
  {
    r = mediadb_media_update_name(
      g_hDB,
      nMediaID,
      strTitle);
    if (MEDIADB_IS_ERROR(r))
    {
      fprintf(stderr,
              "Cannot update media title. Error is %d (%s)\n",
              (int)r,
              mediadb_get_error_message(g_hDB));
      Exit(1);
    }
  }

  r = mediadb_delete_media_files(
    g_hDB,
    nMediaID);

  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Failed to delete media files in database. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
    Exit(1);
  }

  disk_scan(mediadb_scan_callback,
            NULL,
            &nTotalFiles,
            &nTotalSize);

  printf("\n");

  //timeAdded = time(NULL);

  r = mediadb_media_update_properties(
    g_hDB,
    nMediaID,
    timeAdded,
    nTotalFiles,
    nTotalSize);

  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Failed to update new media to database. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
    Exit(1);
  }

  UnmountMedia();

  printf("--> SUCCESS - Media added.\n");
  printf("Media title: %s\n", strTitle);
  printf("Media ID: %u\n", (unsigned int)nMediaID);
  printf("Added: %s", ctime(&timeAdded));
  printf("Total files: %u\n", (unsigned int)nTotalFiles);
  printf("Total size: %u\n", (unsigned int)nTotalSize);

  goto Loop;
}

/* void */
/* ListMedia() */
/* { */
/*   printf("List media mode not implemented yet.\n"); */
/* } */

mediadb_uint g_SearchFilesCounter;

void
SearchFilesCallback(
  void *pUserContext,
  mediadb_uint nMediaID,
  mediadb_mediatype nMediaType,
  const char *pszMediaName,
  const char *pszMediaLocation,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype Filetype,
  mediadb_uint nSize,
  mediadb_uint nTime)
{
  struct tm tm;
  time_t time = nTime;
  char strTime[26];
  char strSize[MAX_SIZE_DESCRIPTION];

  localtime_r(&time, &tm);
  sprintf(strTime,
          "%u-%02u-%02u %02u:%02u:%02u",
          1900+tm.tm_year,
          1+tm.tm_mon,
          tm.tm_mday,
          tm.tm_hour,
          tm.tm_min,
          tm.tm_sec);

  get_size_description(nSize, strSize);

  printf("--------------------\n");
  printf("Name: %s%s\n", pszPath, pszName);
  if (Filetype == MEDIADB_FILETYPE_FILE)
  {
    printf("Size: %s\n", strSize);
  }
  printf("Time: %s\n", strTime);
  printf("MediaID: %u\n", (unsigned int)nMediaID);
  printf("Media name: %s\n", pszMediaName);
  printf("Media location: %s\n", pszMediaLocation);

  g_SearchFilesCounter++;
}

void
SearchFiles()
{
  mediadb_result r;
  mediadb_uint nMinSize;
  mediadb_uint nMaxSize;
  char strFilenamePattern[MAX_READ_STRING_SIZE];
  char strPathPattern[MAX_READ_STRING_SIZE];
  const char **ppszPMMS;
  const struct mediadb_pattern_match_method *pPMM;
  unsigned int nPPMCount;
  unsigned int nFilenamePMMIndex;
  unsigned int nPathPMMIndex;
  unsigned int i;
  struct timeval tv1, tv2;
  struct timezone tz;
  double t1, t2;

  r = mediadb_get_pattern_match_methods(
    g_hDB,
    &pPMM);
  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Cannot get pattern match methods from backend. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
    Exit(1);
  }

  nPPMCount = 0;
  while (pPMM[nPPMCount].nID != MEDIADB_PMM_NULL)
  {
    nPPMCount++;
  }

  if (nPPMCount == 0)
  {
    fprintf(stderr,
            "Backend returned zero pattern march methods.\n");
    Exit(1);
  }

  ppszPMMS = malloc(sizeof(const char *) * nPPMCount);
  if (ppszPMMS == NULL)
  {
    fprintf(stderr,
            "Out of memory.\n");
    Exit(1);
  }

  for (i = 0 ; i < nPPMCount ; i++)
  {
    ppszPMMS[i] = pPMM[i].pszName;
  }

  if (!ReadString("Filename pattern: ", MEDIADB_FALSE, strFilenamePattern))
  {
    strFilenamePattern[0] = 0;
  }
  else
  {
    Menu("Filename pattern match method",
         ppszPMMS,
         nPPMCount,
         0,
         &nFilenamePMMIndex);
  }

  if (!ReadString("Path pattern: ", MEDIADB_FALSE, strPathPattern))
  {
    strPathPattern[0] = 0;
  }
  else
  {
    Menu("Path pattern match method",
         ppszPMMS,
         nPPMCount,
         0,
         &nPathPMMIndex);
  }

  free(ppszPMMS);

  if (!ReadInteger("Min size: ", MEDIADB_FALSE, &nMinSize))
  {
    nMinSize = 0;
  }

  if (!ReadInteger("Max size: ", MEDIADB_FALSE, &nMaxSize))
  {
    nMaxSize = 0;
  }

  printf("--> Searching ...\n");
  g_SearchFilesCounter = 0;

  if (gettimeofday(&tv1, &tz) == -1)
  {
    fprintf(stderr,
            "gettimeofday() failed. Error is %d (%s)\n",
            errno,
            strerror(errno));
    Exit(1);
  }

  r = mediadb_files_search(
    g_hDB,
    strFilenamePattern[0]?pPMM[nFilenamePMMIndex].nID:MEDIADB_PMM_NULL,
    strFilenamePattern[0]?strFilenamePattern:NULL,
    strPathPattern[0]?pPMM[nPathPMMIndex].nID:MEDIADB_PMM_NULL,
    strPathPattern[0]?strPathPattern:NULL,
    nMinSize?&nMinSize:NULL,
    nMaxSize?&nMaxSize:NULL,
    SearchFilesCallback,
    NULL);

  if (gettimeofday(&tv2, &tz) == -1)
  {
    fprintf(stderr,
            "gettimeofday() failed. Error is %d (%s)\n",
            errno,
            strerror(errno));
    Exit(1);
  }

  printf("--------------------\n");

  if (MEDIADB_IS_ERROR(r))
  {
    fprintf(stderr,
            "Search failed. Error is %d (%s)\n",
            (int)r,
            mediadb_get_error_message(g_hDB));
    Exit(1);
  }

  printf("%u file(s) found\n", (unsigned int)g_SearchFilesCounter);

  t1 = tv1.tv_sec;
  t1 += (double)tv1.tv_usec / 1000000.0;
  t2 = tv2.tv_sec;
  t2 += (double)tv2.tv_usec / 1000000.0;
  printf("Search took %f seconds\n", t2-t1);
}

int
main(
  int argc,
  char ** argv
  )
{
  int i;
  char *pszExecutable;
  mediadb_result r;

  pszExecutable = strrchr(argv[0], '/');

  if (pszExecutable == NULL)
  {
    pszExecutable = argv[0];
  }
  else
  {
    pszExecutable++;
  }

  if (strcmp(pszExecutable, "mdb_add") == 0)
  {
    nMode = MODE_ADD_MEDIA;
  }
  else if (strcmp(pszExecutable, "mdb_update") == 0)
  {
    nMode = MODE_UPDATE_MEDIA;
  }
  else
  {
    nMode = MODE_SEARCH_FILES;
  }

  /* Find HOME environment variable */
  pszHOME = getenv("HOME");

  if (pszHOME == NULL)
  {
    fprintf(stderr,
            "Please set the HOME encironment variable.\n");
    Exit(1);
  }

  /* Look into configuration file */
  r = conf_parse();
  
  /* Process options from command line */

  for (i = 1; i < argc ; i++)
  {
/*     if (strcmp(argv[i],"-L") == 0) */
/*     { */
/*       nMode = MODE_LIST_MEDIA; */
/*     } */
/*     else  */
    if (strcmp(argv[i],"-A") == 0)
    {
      nMode = MODE_ADD_MEDIA;
    }
    else if (strcmp(argv[i],"-U") == 0)
    {
      nMode = MODE_UPDATE_MEDIA;
    }
    else if (strcmp(argv[i],"-F") == 0)
    {
      nMode = MODE_SEARCH_FILES;
    }
    else if (strcmp(argv[i],"-h") == 0)
    {
      i++;
      if (i < argc)
      {
        db_set_mysql_host(argv[i]);
      }
      else
      {
        fprintf(stderr,
                "Missing parameter of -h option\n");
        Help(argv[0]);
      }
    }
    else if (strcmp(argv[i],"-u") == 0)
    {
      i++;
      if (i < argc)
      {
        db_set_mysql_user(argv[i]);
      }
      else
      {
        fprintf(stderr,
                "Missing parameter of -u option\n");
        Help(argv[0]);
      }
    }
    else if (strcmp(argv[i],"-p") == 0)
    {
      if (i+1 < argc && argv[i+1][0] != '-')
      {
        i++;
        db_set_mysql_pass(argv[i]);
      }
      else
      {
        AskForPassword();
        db_set_mysql_pass(strPass);
      }
    }
    else if (strcmp(argv[i],"-b") == 0)
    {
      i++;
      if (i < argc)
      {
        db_set_mysql_database(argv[i]);
      }
      else
      {
        fprintf(stderr,
                "Missing parameter of -b option\n");
        Help(argv[0]);
      }
    }
    else if (strcmp(argv[i],"-f") == 0)
    {
      i++;
      if (i < argc)
      {
        db_set_sqlite_database(argv[i]);
      }
      else
      {
        fprintf(stderr,
                "Missing parameter of -f option\n");
        Help(argv[0]);
      }
    }
    else if (strcmp(argv[i],"-m") == 0)
    {
      i++;
      if (i < argc)
      {
        disk_set_mount_command(argv[i]);
      }
      else
      {
        fprintf(stderr,
                "Missing parameter of -m option\n");
        Help(argv[0]);
      }
    }
    else if (strcmp(argv[i],"-n") == 0)
    {
      i++;
      if (i < argc)
      {
        disk_set_unmount_command(argv[i]);
      }
      else
      {
        fprintf(stderr,
                "Missing parameter of -n option\n");
        Help(argv[0]);
      }
    }
    else if (strcmp(argv[i],"-d") == 0)
    {
      i++;
      if (i < argc)
      {
        disk_set_path(argv[i]);
      }
      else
      {
        fprintf(stderr,
                "Missing parameter of -d option\n");
        Help(argv[0]);
      }
    }
    else if (strcmp(argv[i],"-M") == 0)
    {
      db_use_mysql();
    }
    else if (strcmp(argv[i],"-S") == 0)
    {
      db_use_sqlite();
    }
    else if (strcmp(argv[i],"-?") == 0)
    {
      Help(argv[0]);
    }
    else
    {
      fprintf(stderr,
              "Unknown option \"%s\"\n",
              argv[i]);
      Help(argv[0]);
    }
  }

  SetDefaults();

  db_open();

  if (nMode == MODE_ADD_MEDIA)
  {
    printf("=========================\n");
    printf("Add media mode\n");
    printf("=========================\n");
    AddMedia();
  }
  else if (nMode == MODE_UPDATE_MEDIA)
  {
    printf("=========================\n");
    printf("Update media mode\n");
    printf("=========================\n");
    UpdateMedia();
  }
/*   else if (nMode == MODE_LIST_MEDIA) */
/*   { */
/*     printf("=========================\n"); */
/*     printf("List media mode\n"); */
/*     printf("=========================\n"); */
/*     ListMedia(); */
/*   } */
  else if (nMode == MODE_SEARCH_FILES)
  {
    printf("=========================\n");
    printf("Search files mode\n");
    printf("=========================\n");
    SearchFiles();
  }
  else
  {
    assert(0);
    return 1;
  }

  Exit(0);
  return 1;
}
