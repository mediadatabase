/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 * DESCRIPTION:
 *  fuse frontend to MediaDatabase
 *
 * AUTHOR:
 *  Nedko Arnaudov <nedko@users.sourceforge.net>
 *
 * LICENSE:
 *  GNU GENERAL PUBLIC LICENSE version 2
 *  
 *****************************************************************************/

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "../result.h"
#include "../libdb/libdb.h"
#include "../libfrontend/error.h"
#include "../libfrontend/conf.h"
#include "../libfrontend/db.h"

#define UNUSED_VARIABLE(x)  ((void) (x))

mediadb_uint
mediadb_get_mediaid_from_path(
  const char *path,
  const char **ppMediaRoot)
{
  const char * pch;
  char * end;
  mediadb_uint nMediaID;

  if (path[0] != '/')
    return 0;

  pch = path + 1;

  while (*pch != 0)
  {
    if (*pch == '/')
    {
      break;
    }

    pch++;
  }

  while (pch > path)
  {
    if (*pch == '#')
    {
      break;
    }

    pch--;
  }

  nMediaID = strtoull(pch+1, &end, 10);
  if (nMediaID == 18446744073709551615ULL)
  {
    return 0;
  }

  if (*end != 0 && *end != '/')
  {
    return 0;
  }

  *ppMediaRoot = end;
  return nMediaID;
}

static int mediadb_getattr(const char *path, struct stat *stbuf)
{
  const char * pszMediaRoot;
  mediadb_result r;
  mediadb_uint nMediaID;
  mediadb_uint nTotalFiles;
  mediadb_uint nTotalSize;
  mediadb_uint nTimeAdded;
  mediadb_mediatype nType;
  mediadb_uint nLocationID;
  char *pszTitle;
  mediadb_filetype Filetype;
  mediadb_uint nSize;
  mediadb_uint nTime;
  char *pszPath, *pszName;

  memset(stbuf, 0, sizeof(struct stat));

  if (path[0] == '/' && path[1] == 0)
  {
    stbuf->st_mode = __S_IFDIR|0555;
    return 0;
  }

  nMediaID = mediadb_get_mediaid_from_path(
    path,
    &pszMediaRoot);
  if (nMediaID == 0)
  {
    return -EINVAL;
  }

  if (*pszMediaRoot == 0)
  {
    stbuf->st_mode = __S_IFDIR|0555;
    r = mediadb_media_get_properties_data(
      g_hDB,
      nMediaID,
      &nTotalFiles,
      &nTotalSize);
    if (MEDIADB_IS_SUCCESS(r))
    {
      stbuf->st_size = nTotalSize;
      stbuf->st_nlink = nTotalFiles;
    }

    r = mediadb_media_get_properties(
      g_hDB,
      nMediaID,
      &nTimeAdded,
      &nType,
      &nLocationID,
      &pszTitle);
    if (MEDIADB_IS_SUCCESS(r))
    {
      stbuf->st_ctime = nTimeAdded;
      stbuf->st_atime = nTimeAdded;
      stbuf->st_mtime = nTimeAdded;
      free(pszTitle);
    }
  }
  else
  {
    stbuf->st_mode = __S_IFREG;

    pszPath = strdup(pszMediaRoot);
    pszName = strrchr(pszPath, '/');
    if (pszName == NULL)
    {
      return -EINVAL;
    }

    *pszName = 0;
    pszName++;

    r = mediadb_file_get_properties(
      g_hDB,
      nMediaID,
      pszPath,
      pszName,
      &Filetype,
      &nSize,
      &nTime);
    if (MEDIADB_IS_SUCCESS(r))
    {
      if (Filetype == MEDIADB_FILETYPE_FILE)
      {
        stbuf->st_mode = __S_IFREG|0444;
      }
      else if (Filetype == MEDIADB_FILETYPE_DIR)
      {
        stbuf->st_mode = __S_IFDIR|0444;
      }
      stbuf->st_size = nSize;
      stbuf->st_ctime = nTime;
      stbuf->st_atime = nTime;
      stbuf->st_mtime = nTime;
    }

    free(pszPath);
  }

  return 0;
}

struct context_filldir
{
  fuse_dirh_t h;
  fuse_dirfil_t filler;
};

#define ctx ((struct context_filldir *)pUserContext)

void
mediadb_fuse_media_callback(
  void *pUserContext,
  mediadb_uint nMediaID,
  mediadb_mediatype nType,
  const char *pszName,
  const char *pszComment,
  mediadb_uint nTimeAdded,
  mediadb_uint nTotalFiles,
  mediadb_uint nTotalSize,
  mediadb_uint nLocationID,
  const char *pszLocation
  )
{
  size_t s;
  char * entry, *pch;

  s = strlen(pszName);

  entry = (char *)malloc(s+100);

  sprintf(entry, "%s #%u", pszName, (unsigned int)nMediaID);

  for (pch = entry ; *pch != 0 ; pch++)
  {
    if (*pch == '/')
    {
      *pch = '|';
    }
  }

  ctx->filler(ctx->h, entry, __S_IFREG, 0);

  free(entry);
}

void
mediadb_fuse_files_callback(
  void *pUserContext,
  const char *pszPath,
  const char *pszName,
  mediadb_filetype Filetype,
  mediadb_uint nSize,
  mediadb_uint nTime)
{
  if (Filetype == MEDIADB_FILETYPE_FILE)
  {
    ctx->filler(ctx->h, pszName, __S_IFREG, 0);
  }
  else if (Filetype == MEDIADB_FILETYPE_DIR)
  {
    ctx->filler(ctx->h, pszName, __S_IFDIR, 0);
  }
}

#undef ctx

static int mediadb_getdir(const char *path, fuse_dirh_t h, fuse_dirfil_t filler)
{
  int res = 0;
  mediadb_result r;
  struct context_filldir ctx;
  mediadb_uint nMediaID;
  const char * pszMediaRoot;

  ctx.h = h;
  ctx.filler = filler;

  if (strcmp(path, "/") == 0)
  {
    r = mediadb_media_get_all(
      g_hDB,
      mediadb_fuse_media_callback,
      &ctx);
  }
  else
  {
    nMediaID = mediadb_get_mediaid_from_path(
      path,
      &pszMediaRoot);
    if (nMediaID == 0)
    {
      return -EINVAL;
    }

    r = mediadb_files_get(
      g_hDB,
      nMediaID,
      pszMediaRoot,
      mediadb_fuse_files_callback,
      &ctx);
  }

  return res;
}

static struct fuse_operations mediadb_oper = {
  .getattr  = mediadb_getattr,
  .getdir = mediadb_getdir,
};

int main(int argc, char *argv[])
{
  mediadb_result r;
  int ret;

  /* Look into configuration file */
  r = conf_parse();

  db_set_defaults();

  db_open();

  ret =  fuse_main(argc, argv, &mediadb_oper);

  conf_cleanup();

  db_uninit();

  return ret;
}

void
mediadb_error_callback(
  unsigned int nCritical,
  const char *pszErrorDescription
  )
{
  fprintf(stderr,
          "%s\n",
          pszErrorDescription);

  if (nCritical == MEDIADB_ERROR_CRITICAL)
  {
    exit(1);
  }
}

/*****************************************************************************
 *
 * Modifications log:
 *
 * !!! WARNING !!! Following lines are automatically updated by the CVS system.
 *
 *   $Log: main.c,v $
 *   Revision 1.2  2005/03/05 21:43:59  nedko
 *   Modification log added
 *
 *****************************************************************************/
